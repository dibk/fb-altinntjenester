﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class Kommune
    {
        [XmlElement("navn")]
        public string Navn { get; set; }

        [XmlElement("nummer")]
        public string Nummer { get; set; }
    }
}
