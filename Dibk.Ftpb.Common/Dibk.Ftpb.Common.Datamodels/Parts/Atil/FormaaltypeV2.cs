﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Atil
{
    public class FormaaltypeV2
    {
        [XmlElement("anleggstype")]
        public Kodeliste Anleggstype { get; set; }

        [XmlElement("naeringsgruppe")]
        public Kodeliste Naeringsgruppe { get; set; }

        [XmlElement("bygningstype")]
        public Kodeliste Bygningstype { get; set; }

        [XmlArray("tiltaksformaal")]
        [XmlArrayItem("kode")]
        public Kodeliste[] Tiltaksformaal { get; set; }

        [XmlElement("beskrivPlanlagtFormaal")]
        public string BeskrivPlanlagtFormaal { get; set; }
    }
}