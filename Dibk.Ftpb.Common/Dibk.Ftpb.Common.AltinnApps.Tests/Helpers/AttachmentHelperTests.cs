using Dibk.Ftpb.Common.AltinnApps.Helpers;
using Dibk.Ftpb.Common.Constants;
using Xunit;

namespace Dibk.Ftpb.Common.AltinnApps.Tests.Helpers
{
    public class AttachmentHelperTests
    {
        public static IEnumerable<object[]> GetFilenameTestData =>
            new List<object[]>
            {
                new object[] { DataTypes.OversendelseReguleringsplanforslag, null, "xml", null, null, null, "oversendelse-planforslag.xml" },
                new object[] { DataTypes.Planbestemmelser, null, "xml", null, null, null, "Planbestemmelser.xml" },
                new object[] { DataTypes.Plankart, "TestPlannavn", "xml", "v1", null, null, "Plankart-TestPlannavn-v1.xml" },
                new object[] { DataTypes.Valideringsrapport, null, "xml", null, null, null, "Valideringsrapport" },
            };

        [Theory]
        [InlineData(null, null, null, null, null, null, null)]
        [InlineData("", null, null, null, null, null, null)]
        [InlineData(" ", null, null, null, null, null, null)]
        public void GetFilename_InvalidDataType_ReturnsNull(string dataType, string plannavn, string fileExtension, string versjon, string vertikalnivaa, string epsgkode, string expected)
        {
            var result = AttachmentHelper.GetFilename(dataType, plannavn, fileExtension, versjon, vertikalnivaa, epsgkode);
            Assert.Null(result);
        }

        [Theory]
        [MemberData(nameof(GetFilenameTestData))]
        public void GetFilename_ValidDataType_ReturnsExpectedFilename(
            string dataType,
            string plannavn,
            string fileExtension,
            string versjon,
            string vertikalnivaa,
            string epsgkode,
            string expected)
        {
            var result = AttachmentHelper.GetFilename(dataType, plannavn, fileExtension, versjon, vertikalnivaa, epsgkode);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("name", null, null, "xml", null, null, "name.xml")]
        [InlineData("name", "TestPlan", null, "xml", null, null, "name-TestPlan.xml")]
        [InlineData("name", "TestPlan", "v1", "xml", null, null, "name-TestPlan-v1.xml")]
        [InlineData("name", "TestPlan", "v1", "xml", "top", "4326", "name-TestPlan-v1-top-4326.xml")]
        public void CreateFileName_ValidInput_ReturnsCorrectFilename(string name, string plannavn, string versjon, string contentType, string vertikalnivaa, string epsgkode, string expected)
        {
            var result = typeof(AttachmentHelper)
                .GetMethod("CreateFileName", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static)
                .Invoke(null, new object[] { name, plannavn, versjon, contentType, vertikalnivaa, epsgkode }) as string;
            Assert.Equal(expected, result);
        }

        [Fact]
        public void AttachmentOrder_ReturnsCorrectOrder()
        {
            var expectedOrder = new List<string>
            {
                DataTypes.OversendelseReguleringsplanforslag,
                DataTypes.RefDataAsPdf,
                DataTypes.Planbestemmelser,
                DataTypes.Plankart,
                DataTypes.Planbeskrivelse,
                DataTypes.Planvarsel,
                DataTypes.Varslingsliste,
                DataTypes.UttalelseSamling,
                DataTypes.UttalelseKommentarer,
                DataTypes.Illustrasjon,
                DataTypes.Konsekvensutredning,
                DataTypes.ROSAnalyse,
                DataTypes.Utredning,
                DataTypes.Rapport,
                DataTypes.Valideringsrapport,
                DataTypes.Annet
            };

            var result = AttachmentHelper.AttachmentOrder;
            Assert.Equal(expectedOrder, result);
        }
    }
}