﻿using Dibk.Ftpb.Common.Constants;
using System.Collections.Generic;

namespace Dibk.Ftpb.Common.Registry
{
    /// <summary>
    /// Lister over datatypes i ulike kategorier og sammenhenger
    /// </summary>
    public static class DataTypeRegistry
    {
        /// <summary>
        /// Liste over datatyper som inneholder personopplysninger
        /// </summary>
        public static List<string> PersonalSensitiveInformationDataTypes { get; } = new List<string>
        {
            DataTypes.DekryptertFoedselsnummerTiltakshaver,
            DataTypes.OpplysningerGittINabovarselDekrypteringsNoekkel
        };
    }
}