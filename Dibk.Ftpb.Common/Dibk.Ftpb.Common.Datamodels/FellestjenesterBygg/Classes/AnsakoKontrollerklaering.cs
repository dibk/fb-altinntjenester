﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    [XmlRoot("Kontrollerklaering")]
    public class AnsakoKontrollerklaering : DataTypeAttributesDatamodelBase
    {
        [XmlElement("prosjektnavn")]
        public string Prosjektnavn { get; set; }

        [XmlElement("prosjektnr")]
        public string Prosjektnr { get; set; }

        [XmlElement("fraSluttbrukersystem")]
        public string FraSluttbrukersystem { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] eiendomByggested { get; set; }

        [XmlElement("foretak")]
        public Aktoer Foretak { get; set; }

        [XmlElement("ansvarsrett")]
        public AnsvarsrettV2 Ansvarsrett { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 10002<br />
        /// <b>DataFormatVersion:</b> 1<br />
        /// <b>DataFormatProvider:</b> SERES
        /// </summary>
        public AnsakoKontrollerklaering()
        {
            DataFormatProvider = "SERES";
            DataFormatId = "10002";
            DataFormatVersion = "1";
        }
    }
}
