using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "http://skjema.kxml.no/dibk/planvarsel/2.0")]
    [XmlRoot("Planvarsel", Namespace = "http://skjema.kxml.no/dibk/planvarsel/2.0")]
    public class PlanvarselV2 : DataTypeAttributesDatamodelBase
    {

        [XmlElement("forslagsstiller")]
        public AktoerPlan Forslagsstiller { get; set; }

        [XmlArray("beroerteParter")]
        [XmlArrayItem("beroertpart")]
        public Beroertpart[] BeroerteParter { get; set; }

        [XmlElement("kommunenavn")]
        public string Kommunenavn { get; set; }

        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("signatur")]
        public Signatur Signatur { get; set; }

        [XmlArray("gjeldendePlan")]
        [XmlArrayItem("gjeldendeplan")]
        public Gjeldendeplan[] GjeldendePlan { get; set; }

        [XmlElement("plankonsulent")]
        public AktoerPlan Plankonsulent { get; set; }

        [XmlElement("metadata")]
        public MetadataPlan Metadata { get; set; }

        [XmlElement("planforslag")]
        public Planforslag Planforslag { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 11000<br />
        /// <b>DataFormatVersion:</b> 2.0<br />
        /// <b>DataFormatProvider:</b> DIBK
        /// </summary>
        public PlanvarselV2()
        {
            DataFormatProvider = "DIBK";
            DataFormatId = "11000";
            DataFormatVersion = "2.0";
        }
    }  
}