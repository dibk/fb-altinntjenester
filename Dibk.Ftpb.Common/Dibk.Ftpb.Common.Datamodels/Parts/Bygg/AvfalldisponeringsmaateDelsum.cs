﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class AvfalldisponeringsmaateDelsum
    {
        [XmlElement("disponeringsmaate")]
        public Kodeliste Disponeringsmaate { get; set; }

        [XmlElement("delsum")]
        public string Delsum { get; set; }
    }
}