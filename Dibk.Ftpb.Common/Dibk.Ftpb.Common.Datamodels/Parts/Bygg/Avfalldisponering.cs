﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Avfalldisponering
    {
        [XmlElement("leveringssted")]
        public Leveringssted Leveringssted { get; set; }

        [XmlElement("disponeringsmaate")]
        public Kodeliste Disponeringsmaate { get; set; }
    }
}