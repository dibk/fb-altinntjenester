﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Dibk.Ftpb.Common.Datamodels
{
    /// <summary>
    /// The XsdResolver class is used to retrieve XML Schema Definition (XSD) files from the Dibk.Ftpb.Common.Datamodels library or other loaded assemblies in the current AppDomain.
    /// It provides a method to get the stream of the specified XSD file.
    /// </summary>
    public static class XsdResolver
    {
        /// <summary>
        /// Retreives the xsd from the Dibk.Ftpb.Common.Datamodels library
        /// </summary>
        /// <param name="xsdName">Name of XSD to locate</param>
        /// <param name="scanAllAssemblies">Optional: will scan for xsd in all loaded assemblies in the current AppDomain</param>
        /// <returns></returns>
        public static Stream GetXsd(string xsdName, bool scanAllAssemblies = true)
        {
            if (string.IsNullOrEmpty(xsdName))
                throw new ArgumentNullException("xsdName must be provided");

            string xsdFileName;
            if (!xsdName.ToLower().EndsWith(".xsd"))
                xsdFileName = $"{xsdName}.xsd";
            else xsdFileName = xsdName;

            if(scanAllAssemblies)
                return AppDomain.CurrentDomain.GetAssemblies().Where(a => !a.IsDynamic).ToArray().GetResourceStream(xsdFileName);
            else
                return Assembly.GetExecutingAssembly().GetResourceStream(xsdFileName);
        }        

        private static Stream GetResourceStream(this Assembly assembly, string fileName)
        {
            var name = assembly.GetManifestResourceNames().SingleOrDefault(n => n.EndsWith(fileName));

            return name != null ? assembly.GetManifestResourceStream(name) : null;
        }

        private static Stream GetResourceStream(this Assembly[] assemblies, string fileName)
        {
            Stream resourceStream = null;
            foreach (var assembly in assemblies)
            {
                var name = assembly.GetManifestResourceNames().SingleOrDefault(n => n.EndsWith(fileName));
                if (name != null)
                {
                    resourceStream = assembly.GetManifestResourceStream(name);
                    break;
                }
            }

            return resourceStream;
        }
    }
}