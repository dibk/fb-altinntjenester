﻿using System;

namespace Dibk.Ftpb.Common.Constants
{
    /// <summary>
    /// DataTypes, tidligere AttachmentTypeNames.
    /// ** Er metadata tag i blob-containeren.
    /// ** Ved bruk av Altinn Event: Dokumenter som hentes fra appen, får AttachmentTypeName satt til dataTypes.id lagt inn i applicationMetadata.
    /// Søknadstyper skal bruke kodeverdien.
    /// </summary>
    public static class DataTypes
    {
        // Bygg - Hovedskjema
        public static readonly string GjennomføringsplanHovedskjema = "GP";
        public static readonly string Nabovarsel = "NV";
        public static readonly string SuppleringAvSøknad = "SU";
        public static readonly string SøknadOmEndringAvTillatelse = "ES";
        public static readonly string SøknadOmFerdigAttest = "FA";
        public static readonly string SøknadOmIgangsettingstillatelse = "IG";
        public static readonly string SøknadOmMidlertidigBrukstillatelse = "MB";
        public static readonly string SøknadOmRammetillatelse = "RS";
        public static readonly string SøknadOmTillatelseIEttrinn = "ET";
        public static readonly string SøknadOmTillatelseTilTiltakUtenAnsvarsrett = "TA";

        
        // Bygg - Underskjema        
        public static readonly string Vedleggsopplysninger = "Vedleggsopplysninger";
        public static readonly string Matrikkelopplysninger = "Matrikkelopplysninger";
        public static readonly string GjenpartNabovarsel = "GjenpartNabovarsel";


        //Arbeidstilsynet
        public static readonly string ArbeidstilsynetSamtykkeSoknad = "ArbeidstilsynetSamtykkeSoknad";

        
        // Vedlegg

        // -- Felles Plan og Bygg

        public static readonly string Annet = "Annet";

        // -- Bygg

        public static readonly string AndreRedegjoerelser = "AndreRedegjoerelser";
        public static readonly string AvkjoeringstillatelseVegmyndighet = "AvkjoeringstillatelseVegmyndighet";
        public static readonly string Avkjoerselsplan = "Avkjoerselsplan";
        public static readonly string AvklaringHoyspent = "AvklaringHoyspent";
        public static readonly string AvklaringVA = "AvklaringVA";
        public static readonly string BoligspesifikasjonMatrikkel = "BoligspesifikasjonMatrikkel";
        public static readonly string ByggesaksBIM = "ByggesaksBIM";
        public static readonly string Dispensasjonssoeknad = "Dispensasjonssoeknad";

        public static readonly string ErklaeringAnsvarsrett = "ErklaeringAnsvarsrett";
        public static readonly string Samsvarserklæring = "Samsvarserklæring";
        public static readonly string Kontrollerklæring = "KontrollerklæringMedSluttrapport";

        public static readonly string SvarNabovarselPlan = "SvarNabovarselPlan";

        public static readonly string Foto = "Foto";
        public static readonly string Folgebrev = "Folgebrev";
        public static readonly string Gjennomføringsplan = "Gjennomfoeringsplan";
        public static readonly string GjennomføringsplanPdf = "Gjennomfoeringsplan-pdf";
        public static readonly string Kart = "Kart";
        public static readonly string KommentarNabomerknader = "KommentarNabomerknader";
        public static readonly string KvitteringForLevertAvfall = "KvitteringForLevertAvfall";
        public static readonly string KvitteringNabovarsel = "KvitteringNabovarsel";
        public static readonly string Miljoesaneringsbeskrivelse = "Miljoesaneringsbeskrivelse";
        public static readonly string Nabomerknader = "Nabomerknader";
        public static readonly string NabovarselVedlegg = "Nabovarsel";
        public static readonly string OpphoerAnsvarsrett = "OpphoerAnsvarsrett";
        public static readonly string PlanForUavhengigKontroll = "PlanForUavhengigKontroll";
        public static readonly string RapportFraMiljoekartlegging = "RapportFraMiljoekartlegging";
        public static readonly string RedegjoerelseAndreNaturMiljoeforhold = "RedegjoerelseAndreNaturMiljoeforhold";
        public static readonly string RedegjoerelseEstetikk = "RedegjoerelseEstetikk";
        public static readonly string RedegjoerelseUnntakTEK = "RedegjoerelseUnntakTEK";
        public static readonly string RedegjoerelseForurensetGrunn = "RedegjoerelseForurensetGrunn";
        public static readonly string RedegjoerelseGrunnforhold = "RedegjoerelseGrunnforhold";
        public static readonly string RedegjoerelseSkredOgFlom = "RedegjoerelseSkredOgFlom";
        public static readonly string RekvisisjonAvOppmaalingsforretning = "RekvisisjonAvOppmaalingsforretning";
        public static readonly string SamtykkeArbeidstilsynet = "SamtykkeArbeidstilsynet";
        public static readonly string SamtykkePlassering = "SamtykkePlassering";
        public static readonly string Situasjonsplan = "Situasjonsplan";
        public static readonly string SluttrapportForBygningsavfall = "SluttrapportForBygningsavfall";
        public static readonly string AnsvarsrettSelvbygger = "AnsvarsrettSelvbygger";
        public static readonly string ErklaeringSelvbygger = "ErklaeringSelvbygger";
        public static readonly string SoknadOmAnsvarsrettTiltaksklasse1 = "SoknadOmAnsvarsrettTiltaksklasse1";
        public static readonly string TegningEksisterendeFasade = "TegningEksisterendeFasade";
        public static readonly string TegningEksisterendePlan = "TegningEksisterendePlan";
        public static readonly string TegningEksisterendeSnitt = "TegningEksisterendeSnitt";
        public static readonly string TegningNyFasade = "TegningNyFasade";
        public static readonly string TegningNyPlan = "TegningNyPlan";
        public static readonly string TegningNyttSnitt = "TegningNyttSnitt";
        public static readonly string TiltakshaverSignatur = "TiltakshaverSignatur";
        public static readonly string UnderlagUtnytting = "UnderlagUtnytting";
        public static readonly string Utomhusplan = "Utomhusplan";
        public static readonly string UttalelseKulturminnemyndighet = "UttalelseKulturminnemyndighet";
        public static readonly string UttalelseVedtakAnnenOffentligMyndighet = "UttalelseVedtakAnnenOffentligMyndighet";
        public static readonly string DekryptertFoedselsnummerTiltakshaver = "FoedselsnummerTiltakshaver";
        public static readonly string OpplysningerGittINabovarselDekrypteringsNoekkel = "FoedselsnummerNabovarsel";


        //Arbeidstilsynet
        public static readonly string Soknad = "Soknad";
        public static readonly string Arbeidsmiljoutvalg = "Arbeidsmiljoutvalg";
        public static readonly string ArbeidstakersRepresentant = "ArbeidstakersRepresentant";
        public static readonly string Bedriftshelsetjeneste = "Bedriftshelsetjeneste";
        public static readonly string Verneombud = "Verneombud";
        public static readonly string LeietakerArbeidsgiver = "LeietakerArbeidsgiver";
        public static readonly string BeskrivelseTypeArbeidProsess = "BeskrivelseTypeArbeidProsess";
        public static readonly string Arbeidsmiljofaktorer = "Arbeidsmiljofaktorer";
        public static readonly string ArbeidsgiversMedvirkning = "ArbeidsgiversMedvirkning";
        public static readonly string ForUtleiebygg = "ForUtleiebygg";
        public static readonly string BistandFraBHT = "BistandFraBHT";
        public static readonly string Ventilasjon = "Ventilasjon";
        public static readonly string AnsattesMedvirkning = "AnsattesMedvirkning";
        public static readonly string SoknadUnntasOffentlighet = "SoknadUnntasOffentlighet";


        // Felles datatyper på systemfiler
        public static readonly string Valideringsrapport = "Valideringsrapport";
        
        //Altinn standard signatur data type
        public static readonly string Signatur = "Signatur";
        
        // PLAN
        
        // - Hovedskjema

        /// <summary>
        /// Hovedskjema i varsel om oppstart av planarbeid.
        /// Også brukt som vedlegg til innsending av reguleringsplanforslag.
        /// </summary>
        public static readonly string Planvarsel = "Planvarsel";
        
        /// <summary>
        /// Hovedskjema for innsending av reguleringsplanforslag
        /// </summary>
        public static readonly string OversendelseReguleringsplanforslag = "OversendelseReguleringsplanforslag";

        // - Underskjema

        /// <summary>
        /// Vedlegg i innsending av reguleringsplanforslag.
        /// </summary>
        public static readonly string Planbestemmelser = "Planbestemmelser";

        /// <summary>
        /// Vedlegg i innsending av reguleringsplanforslag.
        /// Brukes for både pdf, xml, gml og sosi.
        /// </summary>
        public static readonly string Plankart = "Plankart";

        // - Vedlegg
        public static readonly string Varslingsliste = "Varslingsliste";
        public static readonly string Planbeskrivelse = "Planbeskrivelse";
        public static readonly string SamlefilMottatteUttalelser = "SamlefilUttalelser";
        public static readonly string JuridiskIllustrasjon = "JuridiskIllustrasjon";
        public static readonly string Illustrasjon = "Illustrasjon";
        public static readonly string ROSAnalyse = "RosAnalyse";
        public static readonly string Konsekvensutredning = "Konsekvensutredning";
        public static readonly string KommentarTilUttalelser = "KommentarerUttalelser";
        public static readonly string Utredning = "Utredning";
        public static readonly string Rapport = "Rapport";

        


        
        public static readonly string KartDetaljert = "KartDetaljert";
        public static readonly string Planinitiativ = "Planinitiativ";
        public static readonly string ReferatOppstartsmoete = "ReferatOppstartsmoete";
        public static readonly string Planprogram = "Planprogram";
        
        public static readonly string Planomraade = "Planomraade";
        public static readonly string PlanomraadePdf = "PlanomraadePdf";
        public static readonly string PlanomraadeSosi = "PlanomraadeSosi";
        public static readonly string Varselbrev = "Varselbrev";
        public static readonly string Hoeringsbrev = "Hoeringsbrev";
        public static readonly string Kunngjoring = "Kunngjoring";
        public static readonly string Uttalelse = "Uttalelse";
        public static readonly string ValideringsrapportSluttrapportBygningsavfall = "Valideringsrapport_SluttrapportForBygningsavfall";
        public static readonly string Saksframlegg = "Saksframlegg";
        public static readonly string Tegnforklaringer = "Tegnforklaringer";
        public static readonly string GrunnforholdGeoteknikk = "GrunnforholdGeoteknikk";
        public static readonly string GrunnforholdForurensetGrunn = "GrunnforholdForurensetGrunn";
        public static readonly string Trafikkutredning = "Trafikkutredning";
        public static readonly string Stoyutredning = "Stoyutredning";
        public static readonly string ArkeologiskeUndersokelser = "ArkeologiskeUndersokelser";
        public static readonly string RammeplanVannAvlopOvervann = "RammeplanVannAvlopOvervann";
        public static readonly string BiologiskMangfold = "BiologiskMangfold";
        public static readonly string OffentligEttersyn = "OffentligEttersyn";
        public static readonly string UttalelseOffentligEttersyn = "UttalelseOffentligEttersyn";
        public static readonly string Uttalelsebrev = "Uttalelsebrev";
        public static readonly string UttalelseSamling = "UttalelseSamling";
        public static readonly string UttalelseKommentarer = "UttalelseKommentarer";
        public static readonly string Planuttalelse = "Planuttalelse";
        public static readonly string UttalelseVedlegg = "UttalelseVedlegg";
        public static readonly string RefDataAsPdf = "ref-data-as-pdf";
        

        // TODO - Gjennomgang av disse. Usortert - er de i bruk? - https://arkitektum.atlassian.net/wiki/spaces/DF2/pages/3311861769/Vedlegg+-+Spesifikasjon

        public static readonly string Situasjonskart = "Situasjonskart";
        public static readonly string Organisasjonsplan = "Organisasjonsplan";
        public static readonly string Revisjonserklaering = "Revisjonserklaering";


        [Obsolete("Bruk den nye datatypen 'Plankart' i stedet for 'PlankartPdf'. Denne vil bli fjernet i fremtiden.")]
        public static readonly string PlankartPdf = "PlankartPDF";

        [Obsolete("Bruk den nye datatypen 'Plankart' i stedet for 'PlankartGml'. Denne vil bli fjernet i fremtiden.")]
        public static readonly string PlankartGml = "PlankartGml";

        [Obsolete("Bruk den nye datatypen 'Plankart' i stedet for 'PlankartSosi'. Denne vil bli fjernet i fremtiden.")]
        public static readonly string PlankartSosi = "PlankartSosi";

        [Obsolete("Bruk den nye datatypen 'Plankart' i stedet for 'PlankartGml2d'. Denne vil bli fjernet i fremtiden.")]
        public static readonly string PlankartGml2d = "PlankartGml2d";

        [Obsolete("Bruk den nye datatypen 'Plankart' i stedet for 'PlankartGml3d'. Denne vil bli fjernet i fremtiden.")]
        public static readonly string PlankartGml3d = "PlankartGml3d";

        [Obsolete("Bruk datatypen 'Planvarsel' i stedet for 'Varslingsbrev'.")]
        public static readonly string Varslingsbrev = "Varslingsbrev";

        [Obsolete("Bruk datatypen 'Illustrasjon' i stedet for 'Illustrasjoner'.")]
        public static readonly string Illustrasjoner = "Illustrasjoner";

        [Obsolete("Bruk datatypen 'Planbestemmelser' i stedet for 'PlanbestemmelsePdf'.")]
        public static readonly string PlanbestemmelsePdf = "Planbestemmelse";
        
        [Obsolete("Bruk datatypen 'Planbestemmelser' i stedet for 'PlanbestemmelseXml'.")]
        public static readonly string PlanbestemmelseXml = "PlanbestemmelseXml";

        [Obsolete("Bruk datatypen 'Folgebrev' i stedet for 'Foelgebrev'.")]
        public static readonly string Foelgebrev = "Foelgebrev";

        [Obsolete("Bruk datatypen 'Planomraade' i stedet for 'KartPlanavgrensing'.")]
        public static readonly string KartPlanavgrensing = "KartPlanavgrensing";

        [Obsolete("Bruk datatypen 'Planomraade' i stedet for 'KartPlanavgrensning'.")]
        public static readonly string KartPlanavgrensning = "KartPlanavgrensning";
    }
}