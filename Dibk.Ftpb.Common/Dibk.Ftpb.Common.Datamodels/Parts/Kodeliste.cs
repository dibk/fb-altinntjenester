﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class Kodeliste
    {
        [XmlElement("kodeverdi")]
        public string Kodeverdi { get; set; }

        [XmlElement("kodebeskrivelse")]
        public string Kodebeskrivelse { get; set; }
    }
}