using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Dibk.Ftpb.Common.Datamodels;

namespace Dibk.Ftpb.Common.Tests;

[TestClass]
public class MidlertidigbrukstillatelseTests
{
    [TestMethod]
    public void CurrentVersionIs4()
    {
        var mb = new Midlertidigbrukstillatelse();
        Assert.IsInstanceOfType<MidlertidigbrukstillatelseV4>(mb);
        Assert.IsNotInstanceOfType<MidlertidigbrukstillatelseV3>(mb);
    }

    [TestMethod]
    public void DeserializeCurrentVersionToClassWithUnspecifiedVersion_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/midlertidigbrukstillatelseV4.xml");

        var fa = XmlSerializerUtil.DeserializeFromString<Midlertidigbrukstillatelse>(xmlContent, false);

        Assert.IsNotNull(fa);
    }

    [TestMethod]
    public void V3Data_DeserializeTo_V4Model_throws_Exception()
    {
        var xmlContent = File.ReadAllText("TestData/midlertidigbrukstillatelseV3.xml");
        
        Assert.ThrowsException<InvalidOperationException>(new Action(() => 
        XmlSerializerUtil.DeserializeFromString<MidlertidigbrukstillatelseV4>(xmlContent, false)));
    }

    [TestMethod]
    public void MidlertidigbrukstillatelseV3_DeserializeXml_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/midlertidigbrukstillatelseV3.xml");

        var mbv3 = XmlSerializerUtil.DeserializeFromString<MidlertidigbrukstillatelseV3>(xmlContent, false);

        Assert.IsNotNull(mbv3);

        //DataFormat-attributter
        Assert.AreEqual("DIBK", mbv3.DataFormatProvider);
        Assert.AreEqual("10004", mbv3.DataFormatId);
        Assert.AreEqual("3.0", mbv3.DataFormatVersion);

        //EiendomByggested
        Assert.AreEqual(2, mbv3.EiendomByggested.Length);
        var eiendom = mbv3.EiendomByggested[0];
        Assert.IsNotNull(eiendom?.Eiendomsidentifikasjon);
        Assert.AreEqual("8", eiendom.Eiendomsidentifikasjon.Bruksnummer);
        Assert.AreEqual("1", eiendom.Eiendomsidentifikasjon.Festenummer);
        Assert.AreEqual("567", eiendom.Eiendomsidentifikasjon.Gaardsnummer);
        Assert.AreEqual("1234", eiendom.Eiendomsidentifikasjon.Kommunenummer);
        Assert.AreEqual("2", eiendom.Eiendomsidentifikasjon.Seksjonsnummer);
        Assert.IsNotNull(eiendom.Adresse);
        Assert.AreEqual("Gateadresse 123", eiendom.Adresse.Adresselinje1);
        Assert.AreEqual("1234", eiendom.Adresse.Postnr);
        Assert.AreEqual("Poststed", eiendom.Adresse.Poststed);
        Assert.AreEqual("NO", eiendom.Adresse.Landkode);
        Assert.AreEqual("123", eiendom.Bygningsnummer);
        Assert.AreEqual("1", eiendom.Bolignummer);
        Assert.AreEqual("Kommune", eiendom.Kommunenavn);

        //KommunensSaksnummer
        Assert.IsNotNull(mbv3.KommunensSaksnummer);
        Assert.AreEqual(2024, mbv3.KommunensSaksnummer.Saksaar);
        Assert.AreEqual(12345, mbv3.KommunensSaksnummer.Sakssekvensnummer);

        //Metadata
        Assert.IsNotNull(mbv3.Metadata);
        Assert.AreEqual("Systemnavn", mbv3.Metadata.FraSluttbrukersystem);
        Assert.AreEqual("98765", mbv3.Metadata.FtbId);
        Assert.AreEqual("Prosjektnavn", mbv3.Metadata.Prosjektnavn);
        Assert.AreEqual("54321", mbv3.Metadata.Prosjektnr);
        Assert.IsNotNull(mbv3.Metadata.ForetrukketSpraak);
        Assert.AreEqual("NO", mbv3.Metadata.ForetrukketSpraak.Kodeverdi);
        Assert.AreEqual("Norsk", mbv3.Metadata.ForetrukketSpraak.Kodebeskrivelse);

        //GenerelleVilkaar
        Assert.IsNotNull(mbv3.GenerelleVilkaar);
        Assert.AreEqual(true, mbv3.GenerelleVilkaar.NorskSvenskDansk);

        //SoeknadGjelder
        Assert.IsNotNull(mbv3.SoeknadGjelder);
        Assert.AreEqual(true, mbv3.SoeknadGjelder.GjelderHeleTiltaket);
        Assert.AreEqual("Bygning av bolig", mbv3.SoeknadGjelder.DelAvTiltaket);
        Assert.IsNotNull(mbv3.SoeknadGjelder.Type);
        Assert.AreEqual(2, mbv3.SoeknadGjelder.Type.Length);
        var soeknadGjelderType = mbv3.SoeknadGjelder.Type[0];
        Assert.AreEqual("Code1", soeknadGjelderType.Kodeverdi);
        Assert.AreEqual("Description1", soeknadGjelderType.Kodebeskrivelse);
        Assert.AreEqual("123", mbv3.SoeknadGjelder.Delsoeknadsnummer);
        Assert.AreEqual("Her er et følgebrev", mbv3.SoeknadGjelder.Foelgebrev);

        //Delsøknad
        Assert.IsNotNull(mbv3.Delsoeknader);
        Assert.AreEqual(2, mbv3.Delsoeknader.Length);
        var delsoeknad = mbv3.Delsoeknader[0];
        Assert.IsNotNull(delsoeknad);
        Assert.AreEqual("Bygging av garasje", delsoeknad.DelAvTiltaket);
        Assert.AreEqual(new DateTime(2024, 03, 12), delsoeknad.Tillatelsedato);
        Assert.AreEqual("Comment1", delsoeknad.Kommentar);
        Assert.AreEqual("123", delsoeknad.Delsoeknadsnummer);
        Assert.IsNotNull(delsoeknad.Type);
        Assert.AreEqual(2, delsoeknad.Type.Length);
        var delsoeknadType = delsoeknad.Type[0];
        Assert.AreEqual("Code1", delsoeknadType.Kodeverdi);
        Assert.AreEqual("Description1", delsoeknadType.Kodebeskrivelse);

        //UtfallBesvarelse
        Assert.IsNotNull(mbv3.UtfallBesvarelse);
        Assert.AreEqual(2, mbv3.UtfallBesvarelse.Length);
        var utfallSvar = mbv3.UtfallBesvarelse[0];
        Assert.AreEqual("123", utfallSvar.UtfallId);
        Assert.IsNotNull(utfallSvar.Utfalltype);
        Assert.AreEqual("UTFALL1", utfallSvar.Utfalltype.Kodeverdi);
        Assert.AreEqual("Utfall 1", utfallSvar.Utfalltype.Kodebeskrivelse);
        Assert.IsNotNull(utfallSvar.UtloestFraSjekkpunkt);
        Assert.AreEqual("Sjekkpunkt_1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktId);
        Assert.AreEqual("Sjekkpunkteier 1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktEier);
        Assert.IsNotNull(utfallSvar.Tema);
        Assert.AreEqual("Theme1", utfallSvar.Tema.Kodeverdi);
        Assert.AreEqual("ThemeDescription1", utfallSvar.Tema.Kodebeskrivelse);
        Assert.AreEqual("Utfall 1 tittel", utfallSvar.Tittel);
        Assert.AreEqual("Dette er utfall 1", utfallSvar.Beskrivelse);
        Assert.AreEqual(false, utfallSvar.ErUtfallBesvaresSenere);
        Assert.AreEqual(true, utfallSvar.ErUtfallBesvart);
        Assert.AreEqual("Utfall 1 kommentar", utfallSvar.Kommentar);
        //Vedlegg
        Assert.IsNotNull(utfallSvar.Vedleggsliste);
        Assert.AreEqual(2, utfallSvar.Vedleggsliste.Length);
        var vedlegg = utfallSvar.Vedleggsliste[0];
        Assert.AreEqual("1", vedlegg.Versjonsnummer);
        Assert.IsNotNull(vedlegg.Vedleggstype);
        Assert.AreEqual("Type1", vedlegg.Vedleggstype.Kodeverdi);
        Assert.AreEqual("TypeDescription1", vedlegg.Vedleggstype.Kodebeskrivelse);
        Assert.AreEqual(new DateTime(2024, 03, 12), vedlegg.Versjonsdato);
        Assert.AreEqual("File1.pdf", vedlegg.Filnavn);
        //Underskjema
        Assert.IsNotNull(utfallSvar.Underskjema);
        Assert.AreEqual(2, utfallSvar.Underskjema.Length);
        var underskjema = utfallSvar.Underskjema[0];
        Assert.AreEqual("UNDERSKJEMA1", underskjema.Kodeverdi);
        Assert.AreEqual("Underskjema 1", underskjema.Kodebeskrivelse);

        //Tiltakshaver
        var tiltakshaver = mbv3.Tiltakshaver;
        Assert.IsNotNull(tiltakshaver);
        Assert.IsNotNull(tiltakshaver.Partstype);
        Assert.AreEqual("TYPE3", tiltakshaver.Partstype.Kodeverdi);
        Assert.AreEqual("Type 3", tiltakshaver.Partstype.Kodebeskrivelse);
        Assert.AreEqual("12345678910", tiltakshaver.Foedselsnummer);
        Assert.AreEqual("987654321", tiltakshaver.Organisasjonsnummer);
        Assert.AreEqual("Tiltakshaver 1", tiltakshaver.Navn);
        Assert.IsNotNull(tiltakshaver.Adresse);
        Assert.AreEqual("Gateadresse 789", tiltakshaver.Adresse.Adresselinje1);
        Assert.AreEqual("9876", tiltakshaver.Adresse.Postnr);
        Assert.AreEqual("Poststed 3", tiltakshaver.Adresse.Poststed);
        Assert.AreEqual("NO", tiltakshaver.Adresse.Landkode);
        Assert.AreEqual("12345678", tiltakshaver.Telefonnummer);
        Assert.AreEqual("98765432", tiltakshaver.Mobilnummer);
        Assert.AreEqual("tiltakshaver1@example.com", tiltakshaver.Epost);
        var kontaktpersonTth = tiltakshaver.Kontaktperson;
        Assert.IsNotNull(kontaktpersonTth);
        Assert.AreEqual("Kontaktperson 1", kontaktpersonTth.Navn);
        Assert.AreEqual("11111111", kontaktpersonTth.Telefonnummer);
        Assert.AreEqual("22222222", kontaktpersonTth.Mobilnummer);
        Assert.AreEqual("kontaktperson1@example.com", kontaktpersonTth.Epost);

        //AnsvarligSoeker
        Assert.IsNotNull(mbv3.AnsvarligSoeker);
        Assert.IsNotNull(mbv3.AnsvarligSoeker.Partstype);
        Assert.AreEqual("Code2", mbv3.AnsvarligSoeker.Partstype.Kodeverdi);
        Assert.AreEqual("Description2", mbv3.AnsvarligSoeker.Partstype.Kodebeskrivelse);
        Assert.AreEqual("98765432109", mbv3.AnsvarligSoeker.Foedselsnummer);
        Assert.AreEqual("123456789", mbv3.AnsvarligSoeker.Organisasjonsnummer);
        Assert.AreEqual("Ansvarlig Søker 1", mbv3.AnsvarligSoeker.Navn);
        Assert.IsNotNull(mbv3.AnsvarligSoeker.Adresse);
        Assert.AreEqual("Gateadresse 101", mbv3.AnsvarligSoeker.Adresse.Adresselinje1);
        Assert.AreEqual("1111", mbv3.AnsvarligSoeker.Adresse.Postnr);
        Assert.AreEqual("Poststed 4", mbv3.AnsvarligSoeker.Adresse.Poststed);
        Assert.AreEqual("NO", mbv3.AnsvarligSoeker.Adresse.Landkode);
        Assert.AreEqual("99999999", mbv3.AnsvarligSoeker.Telefonnummer);
        Assert.AreEqual("88888888", mbv3.AnsvarligSoeker.Mobilnummer);
        Assert.AreEqual("ansvarligsoeker1@example.com", mbv3.AnsvarligSoeker.Epost);
        var kontaktperson = mbv3.AnsvarligSoeker.Kontaktperson;
        Assert.IsNotNull(kontaktperson);
        Assert.AreEqual("Kontaktperson 2", kontaktperson.Navn);
        Assert.AreEqual("33333333", kontaktperson.Telefonnummer);
        Assert.AreEqual("44444444", kontaktperson.Mobilnummer);
        Assert.AreEqual("kontaktperson2@example.com", kontaktperson.Epost);

        //DatoFerdigattest
        Assert.AreEqual(new DateTime(2024, 03, 16), mbv3.DatoFerdigattest);

        //GjenstaaendeArbeider
        Assert.IsNotNull(mbv3.GjenstaaendeArbeider);
        Assert.AreEqual("Work1", mbv3.GjenstaaendeArbeider.GjenstaaendeInnenfor);
        Assert.AreEqual("Work2", mbv3.GjenstaaendeArbeider.GjenstaaendeUtenfor);

        //Sikkerhetsnivaa
        Assert.IsNotNull(mbv3.Sikkerhetsnivaa);
        Assert.AreEqual(true, mbv3.Sikkerhetsnivaa.HarTilstrekkeligSikkerhet);
        Assert.AreEqual("WorkType", mbv3.Sikkerhetsnivaa.TypeArbeider);
        Assert.AreEqual(new DateTime(2024, 03, 17), mbv3.Sikkerhetsnivaa.UtfoertInnen);
        Assert.AreEqual(new DateTime(2024, 03, 18), mbv3.Sikkerhetsnivaa.BekreftelseInnen);

        //AnsvarForByggesaken
        Assert.IsNotNull(mbv3.AnsvarForByggesaken);
        Assert.AreEqual("Code3", mbv3.AnsvarForByggesaken.Kodeverdi);
        Assert.AreEqual("Description3", mbv3.AnsvarForByggesaken.Kodebeskrivelse);
    }

    [TestMethod]
    public void MidlertidigbrukstillatelseV4_DeserializeXml_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/midlertidigbrukstillatelseV4.xml");

        var mbv4 = XmlSerializerUtil.DeserializeFromString<MidlertidigbrukstillatelseV4>(xmlContent, false);

        Assert.IsNotNull(mbv4);

        //DataFormat-attributter
        Assert.AreEqual("DIBK", mbv4.DataFormatProvider);
        Assert.AreEqual("10004", mbv4.DataFormatId);
        Assert.AreEqual("4", mbv4.DataFormatVersion);

        //EiendomByggested
        Assert.AreEqual(2, mbv4.EiendomByggested.Length);
        var eiendom = mbv4.EiendomByggested[0];
        Assert.IsNotNull(eiendom?.Eiendomsidentifikasjon);
        Assert.AreEqual("8", eiendom.Eiendomsidentifikasjon.Bruksnummer);
        Assert.AreEqual("1", eiendom.Eiendomsidentifikasjon.Festenummer);
        Assert.AreEqual("567", eiendom.Eiendomsidentifikasjon.Gaardsnummer);
        Assert.AreEqual("1234", eiendom.Eiendomsidentifikasjon.Kommunenummer);
        Assert.AreEqual("2", eiendom.Eiendomsidentifikasjon.Seksjonsnummer);
        Assert.IsNotNull(eiendom.Adresse);
        Assert.AreEqual("Gateadresse 123", eiendom.Adresse.Adresselinje1);
        Assert.AreEqual("1234", eiendom.Adresse.Postnr);
        Assert.AreEqual("Poststed", eiendom.Adresse.Poststed);
        Assert.AreEqual("NO", eiendom.Adresse.Landkode);
        Assert.AreEqual("123", eiendom.Bygningsnummer);
        Assert.AreEqual("1", eiendom.Bolignummer);
        Assert.AreEqual("Kommune", eiendom.Kommunenavn);

        //KommunensSaksnummer
        Assert.IsNotNull(mbv4.KommunensSaksnummer);
        Assert.AreEqual(2024, mbv4.KommunensSaksnummer.Saksaar);
        Assert.AreEqual(12345, mbv4.KommunensSaksnummer.Sakssekvensnummer);

        //Metadata
        Assert.IsNotNull(mbv4.Metadata);
        Assert.AreEqual("Systemnavn", mbv4.Metadata.FraSluttbrukersystem);
        Assert.AreEqual("98765", mbv4.Metadata.FtbId);
        Assert.AreEqual("Prosjektnavn", mbv4.Metadata.Prosjektnavn);
        Assert.AreEqual("54321", mbv4.Metadata.Prosjektnr);
        Assert.IsNotNull(mbv4.Metadata.ForetrukketSpraak);
        Assert.AreEqual("NO", mbv4.Metadata.ForetrukketSpraak.Kodeverdi);
        Assert.AreEqual("Norsk", mbv4.Metadata.ForetrukketSpraak.Kodebeskrivelse);

        //GenerelleVilkaar
        Assert.IsNotNull(mbv4.GenerelleVilkaar);
        Assert.AreEqual(true, mbv4.GenerelleVilkaar.NorskSvenskDansk);

        //SoeknadGjelder
        Assert.IsNotNull(mbv4.SoeknadGjelder);
        Assert.AreEqual(true, mbv4.SoeknadGjelder.GjelderHeleTiltaket);
        Assert.AreEqual("Bygning av bolig", mbv4.SoeknadGjelder.DelAvTiltaket);
        Assert.IsNotNull(mbv4.SoeknadGjelder.Type);
        Assert.AreEqual(2, mbv4.SoeknadGjelder.Type.Length);
        var soeknadGjelderType = mbv4.SoeknadGjelder.Type[0];
        Assert.AreEqual("Code1", soeknadGjelderType.Kodeverdi);
        Assert.AreEqual("Description1", soeknadGjelderType.Kodebeskrivelse);
        Assert.AreEqual("123", mbv4.SoeknadGjelder.Delsoeknadsnummer);
        Assert.AreEqual("Her er et følgebrev", mbv4.SoeknadGjelder.Foelgebrev);

        //Delsøknad
        Assert.IsNotNull(mbv4.Delsoeknader);
        Assert.AreEqual(2, mbv4.Delsoeknader.Length);
        var delsoeknad = mbv4.Delsoeknader[0];
        Assert.IsNotNull(delsoeknad);
        Assert.AreEqual("Bygging av garasje", delsoeknad.DelAvTiltaket);
        Assert.AreEqual(new DateTime(2024, 03, 12), delsoeknad.Tillatelsedato);
        Assert.AreEqual("Comment1", delsoeknad.Kommentar);
        Assert.AreEqual("123", delsoeknad.Delsoeknadsnummer);
        Assert.IsNotNull(delsoeknad.Type);
        Assert.AreEqual(2, delsoeknad.Type.Length);
        var delsoeknadType = delsoeknad.Type[0];
        Assert.AreEqual("Code1", delsoeknadType.Kodeverdi);
        Assert.AreEqual("Description1", delsoeknadType.Kodebeskrivelse);

        //UtfallBesvarelse
        Assert.IsNotNull(mbv4.UtfallBesvarelse);
        Assert.AreEqual(2, mbv4.UtfallBesvarelse.Length);
        var utfallSvar = mbv4.UtfallBesvarelse[0];
        Assert.AreEqual("123", utfallSvar.UtfallId);
        Assert.IsNotNull(utfallSvar.Utfalltype);
        Assert.AreEqual("UTFALL1", utfallSvar.Utfalltype.Kodeverdi);
        Assert.AreEqual("Utfall 1", utfallSvar.Utfalltype.Kodebeskrivelse);
        Assert.IsNotNull(utfallSvar.UtloestFraSjekkpunkt);
        Assert.AreEqual("Sjekkpunkt_1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktId);
        Assert.AreEqual("Sjekkpunkteier 1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktEier);
        Assert.IsNotNull(utfallSvar.Tema);
        Assert.AreEqual("Theme1", utfallSvar.Tema.Kodeverdi);
        Assert.AreEqual("ThemeDescription1", utfallSvar.Tema.Kodebeskrivelse);
        Assert.AreEqual("Utfall 1 tittel", utfallSvar.Tittel);
        Assert.AreEqual("Dette er utfall 1", utfallSvar.Beskrivelse);
        Assert.AreEqual(false, utfallSvar.ErUtfallBesvaresSenere);
        Assert.AreEqual(true, utfallSvar.ErUtfallBesvart);
        Assert.AreEqual("Utfall 1 kommentar", utfallSvar.Kommentar);
        //Vedlegg
        Assert.IsNotNull(utfallSvar.Vedleggsliste);
        Assert.AreEqual(2, utfallSvar.Vedleggsliste.Length);
        var vedlegg = utfallSvar.Vedleggsliste[0];
        Assert.AreEqual("1", vedlegg.Versjonsnummer);
        Assert.IsNotNull(vedlegg.Vedleggstype);
        Assert.AreEqual("Type1", vedlegg.Vedleggstype.Kodeverdi);
        Assert.AreEqual("TypeDescription1", vedlegg.Vedleggstype.Kodebeskrivelse);
        Assert.AreEqual(new DateTime(2024, 03, 12), vedlegg.Versjonsdato);
        Assert.AreEqual("File1.pdf", vedlegg.Filnavn);

        //Tiltakshaver
        var tiltakshaver = mbv4.Tiltakshaver;
        Assert.IsNotNull(tiltakshaver);
        Assert.IsNotNull(tiltakshaver.Partstype);
        Assert.AreEqual("TYPE3", tiltakshaver.Partstype.Kodeverdi);
        Assert.AreEqual("Type 3", tiltakshaver.Partstype.Kodebeskrivelse);
        Assert.AreEqual("12345678910", tiltakshaver.Foedselsnummer);
        Assert.AreEqual("987654321", tiltakshaver.Organisasjonsnummer);
        Assert.AreEqual("Tiltakshaver 1", tiltakshaver.Navn);
        Assert.IsNotNull(tiltakshaver.Adresse);
        Assert.AreEqual("Gateadresse 789", tiltakshaver.Adresse.Adresselinje1);
        Assert.AreEqual("9876", tiltakshaver.Adresse.Postnr);
        Assert.AreEqual("Poststed 3", tiltakshaver.Adresse.Poststed);
        Assert.AreEqual("NO", tiltakshaver.Adresse.Landkode);
        Assert.AreEqual("12345678", tiltakshaver.Telefonnummer);
        Assert.AreEqual("98765432", tiltakshaver.Mobilnummer);
        Assert.AreEqual("tiltakshaver1@example.com", tiltakshaver.Epost);
        var kontaktpersonTth = tiltakshaver.Kontaktperson;
        Assert.IsNotNull(kontaktpersonTth);
        Assert.AreEqual("Kontaktperson 1", kontaktpersonTth.Navn);
        Assert.AreEqual("11111111", kontaktpersonTth.Telefonnummer);
        Assert.AreEqual("22222222", kontaktpersonTth.Mobilnummer);
        Assert.AreEqual("kontaktperson1@example.com", kontaktpersonTth.Epost);

        //AnsvarligSoeker
        Assert.IsNotNull(mbv4.AnsvarligSoeker);
        Assert.IsNotNull(mbv4.AnsvarligSoeker.Partstype);
        Assert.AreEqual("Code2", mbv4.AnsvarligSoeker.Partstype.Kodeverdi);
        Assert.AreEqual("Description2", mbv4.AnsvarligSoeker.Partstype.Kodebeskrivelse);
        Assert.AreEqual("98765432109", mbv4.AnsvarligSoeker.Foedselsnummer);
        Assert.AreEqual("123456789", mbv4.AnsvarligSoeker.Organisasjonsnummer);
        Assert.AreEqual("Ansvarlig Søker 1", mbv4.AnsvarligSoeker.Navn);
        Assert.IsNotNull(mbv4.AnsvarligSoeker.Adresse);
        Assert.AreEqual("Gateadresse 101", mbv4.AnsvarligSoeker.Adresse.Adresselinje1);
        Assert.AreEqual("1111", mbv4.AnsvarligSoeker.Adresse.Postnr);
        Assert.AreEqual("Poststed 4", mbv4.AnsvarligSoeker.Adresse.Poststed);
        Assert.AreEqual("NO", mbv4.AnsvarligSoeker.Adresse.Landkode);
        Assert.AreEqual("99999999", mbv4.AnsvarligSoeker.Telefonnummer);
        Assert.AreEqual("88888888", mbv4.AnsvarligSoeker.Mobilnummer);
        Assert.AreEqual("ansvarligsoeker1@example.com", mbv4.AnsvarligSoeker.Epost);
        var kontaktperson = mbv4.AnsvarligSoeker.Kontaktperson;
        Assert.IsNotNull(kontaktperson);
        Assert.AreEqual("Kontaktperson 2", kontaktperson.Navn);
        Assert.AreEqual("33333333", kontaktperson.Telefonnummer);
        Assert.AreEqual("44444444", kontaktperson.Mobilnummer);
        Assert.AreEqual("kontaktperson2@example.com", kontaktperson.Epost);

        //DatoFerdigattest
        Assert.AreEqual(new DateTime(2024, 03, 16), mbv4.DatoFerdigattest);

        //GjenstaaendeArbeider
        Assert.IsNotNull(mbv4.GjenstaaendeArbeider);
        Assert.AreEqual("Work1", mbv4.GjenstaaendeArbeider.GjenstaaendeInnenfor);
        Assert.AreEqual("Work2", mbv4.GjenstaaendeArbeider.GjenstaaendeUtenfor);

        //Sikkerhetsnivaa
        Assert.IsNotNull(mbv4.Sikkerhetsnivaa);
        Assert.AreEqual(true, mbv4.Sikkerhetsnivaa.HarTilstrekkeligSikkerhet);
        Assert.AreEqual("WorkType", mbv4.Sikkerhetsnivaa.TypeArbeider);
        Assert.AreEqual(new DateTime(2024, 03, 17), mbv4.Sikkerhetsnivaa.UtfoertInnen);
        Assert.AreEqual(new DateTime(2024, 03, 18), mbv4.Sikkerhetsnivaa.BekreftelseInnen);

        //AnsvarForByggesaken
        Assert.IsNotNull(mbv4.AnsvarForByggesaken);
        Assert.AreEqual("Code3", mbv4.AnsvarForByggesaken.Kodeverdi);
        Assert.AreEqual("Description3", mbv4.AnsvarForByggesaken.Kodebeskrivelse);
    }
}
