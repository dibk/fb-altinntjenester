using Altinn.App.Core.Features.FileAnalysis;

namespace Dibk.Ftpb.Common.AltinnApps.Validator.File;

public class ContentTypeAnalyser : IFileAnalyser
{
    /// <summary>
    /// Analyse for å sjekke om content-type og file extension matcher. Dette er en mangel hos Altinn, så brukeren kan laste opp en fil med feil content-type og file extension.
    /// Eksempel: Brukeren laster opp en xml-fil men angir content-type som application/pdf og filnavn med fil extension .pdf.
    /// Dette vil føre til at filen blir validert og sjekket seinere fordi dataelementet sin content-type og file extension er feil i forhold til innholdet. .
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="filename"></param>
    /// <returns></returns>
    public async Task<FileAnalysisResult> Analyse(Stream stream, string filename = null)
    {
        if (!stream.CanSeek) return new FileAnalysisResult(Id);


        var contentType = await ValidationHelper.GetContentType(stream);
        var fileExtension = Path.GetExtension(filename)?.ToLowerInvariant().TrimStart('.');


        //Det må være samsvar med content-type og file ekstensjon. Det sjekker ikke altinn
        if (!ValidationHelper.IsContentTypeAndExtensionMatching(contentType, fileExtension))
        {
            return new FileAnalysisResult(Id)
            {
                Filename = filename,
                Metadata = { new KeyValuePair<string, string>("ContentTypeAnalyser", "Content type and file extension do not match") }
            };
        }

        return new FileAnalysisResult(Id);
    }


    /// <summary>
    /// Id for filanalysatoren, settes på den aktuelle datatypen i applicationMetadata
    /// </summary>
    public string Id => "contentTypeAnalyser";
}