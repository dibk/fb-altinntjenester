﻿namespace Dibk.Ftpb.Common.Constants
{
    /// <summary>
    ///  https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/bokstav
    /// </summary>
    public enum Vedleggskategori
    {
        B,
        C,
        D,
        E,
        F, 
        G, 
        H, 
        I, 
        J, 
        K, 
        Q
    }
}
