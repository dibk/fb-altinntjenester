﻿using System;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "https://skjema.ft.dibk.no/midlertidigbrukstillatelse/v4")]
    [XmlRoot("midlertidigbrukstillatelse", Namespace = "https://skjema.ft.dibk.no/midlertidigbrukstillatelse/v4")]
    public class MidlertidigbrukstillatelseV4 : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("metadata")]
        public Metadata Metadata { get; set; }

        [XmlElement("generelleVilkaar")]
        public GenerelleVilkaar GenerelleVilkaar { get; set; }

        [XmlElement("soeknadGjelder")]
        public SoeknadGjelder SoeknadGjelder { get; set; }

        [XmlArray("delsoeknader")]
        [XmlArrayItem("delsoeknad")]
        public Delsoeknad[] Delsoeknader { get; set; }

        [XmlArray("utfallBesvarelse")]
        [XmlArrayItem("utfallSvar")]
        public UtfallSvar[] UtfallBesvarelse { get; set; }

        [XmlElement("tiltakshaver")]
        public Aktoer Tiltakshaver { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("datoFerdigattest")]
        public DateTime? DatoFerdigattest { get; set; }

        [XmlElement("gjenstaaendeArbeider")]
        public GjenstaaendeArbeider GjenstaaendeArbeider { get; set; }

        [XmlElement("sikkerhetsnivaa")]
        public Sikkerhetsnivaa Sikkerhetsnivaa { get; set; }

        [XmlElement("ansvarForByggesaken")]
        public Kodeliste AnsvarForByggesaken { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 10004<br />
        /// <b>DataFormatVersion:</b> 4<br />
        /// <b>DataFormatProvider:</b> DIBK
        /// </summary>
        public MidlertidigbrukstillatelseV4()
        {
            DataFormatProvider = "DIBK";
            DataFormatId = "10004";
            DataFormatVersion = "4";
        }
    }
}