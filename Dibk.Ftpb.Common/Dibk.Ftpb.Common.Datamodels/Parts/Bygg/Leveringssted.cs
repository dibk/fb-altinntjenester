﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Leveringssted
    {
        [XmlElement("mottaksId")]
        public string MottaksId { get; set; }

        [XmlElement("mottaksNavn")]
        public string Mottaksnavn { get; set; }

    }
}