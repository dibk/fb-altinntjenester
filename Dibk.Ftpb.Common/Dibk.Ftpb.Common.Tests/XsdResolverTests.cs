using Dibk.Ftpb.Common.Datamodels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Dibk.Ftpb.Common.Tests
{
    [TestClass]
    public class XsdResolverTests
    {
        [TestMethod]
        public void ResolveXsd_Ansako_returns_Ansvarserklæring()
        {
            var schemaName = "ansvarsrettAnsako.xsd";

            var schemaStream = XsdResolver.GetXsd(schemaName);

            Assert.IsNotNull(schemaStream);
        }

        [TestMethod]
        public void ResolveXsd_AnsakoWithoutExtension_returns_Ansvarserklæring()
        {
            var schemaName = "ansvarsrettAnsako";

            var schemaStream = XsdResolver.GetXsd(schemaName);

            Assert.IsNotNull(schemaStream);
        }

        [TestMethod]
        public void ResolveXsd_EmptyInput_throws_argumentNullException()
        {
            string? schemaName = null;

            Assert.ThrowsException<ArgumentNullException>(() => XsdResolver.GetXsd(schemaName));
        }

        [TestMethod]
        public void ResolveXsd_FellestjenesterPlan_returns_PlanvarselHøringsmyndigheter()
        {
            var schemaName = "planvarselHoeringsmyndigheter.xsd";

            var schemaStream = XsdResolver.GetXsd(schemaName);

            Assert.IsNotNull(schemaStream);
        }

        [TestMethod]
        public void ResolveXsd_FellestjenesterBygg_returns_PlanvarselHøringsmyndigheter()
        {
            var schemaName = "nabovarselV4.xsd";

            var schemaStream = XsdResolver.GetXsd(schemaName);

            Assert.IsNotNull(schemaStream);
        }

        [TestMethod]
        public void ResolveXsd_FellestjenesterBygg_returns_null()
        {
            var schemaName = "someunknown schema.xsd";

            var schemaStream = XsdResolver.GetXsd(schemaName);

            Assert.IsNull(schemaStream);
        }

        [TestMethod]
        public void ResolveXsd_notInCurrentAssembly_returns_Xsd()
        {
            var schemaName = "ansvarsrettV2.xsd";

            var schemaStream = XsdResolver.GetXsd(schemaName);

            Assert.IsNotNull(schemaStream);
        }

        [TestMethod]
        public void ResolveXsd_doNotScanAllAssemblies_returns_Null()
        {
            var schemaName = "ansvarsrettV2.xsd";

            var schemaStream = XsdResolver.GetXsd(xsdName: schemaName, scanAllAssemblies: false);

            Assert.IsNull(schemaStream);
        }

        [TestMethod]
        public void ResolveXsd_doNotScanAllAssemblies_returns_Xsd()
        {
            var schemaName = "nabovarselV4.xsd";

            var schemaStream = XsdResolver.GetXsd(xsdName: schemaName, scanAllAssemblies: false);

            Assert.IsNotNull(schemaStream);
        }
    }
}