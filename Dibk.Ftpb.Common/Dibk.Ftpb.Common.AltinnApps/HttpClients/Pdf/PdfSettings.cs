﻿namespace Dibk.Ftpb.Common.AltinnApps.HttpClients.Pdf
{
    public class PdfSettings
    {
        public static string SectionName => "PdfSettings";
        public string GeneratorUrl { get; set; }
        public PaperSettings Paper { get; set; } = new PaperSettings();

        public class PaperSettings
        {
            public string Format { get; set; }
            public string MarginTop { get; set; }
            public string MarginRight { get; set; }
            public string MarginBottom { get; set; }
            public string MarginLeft { get; set; }
        }
    }
}