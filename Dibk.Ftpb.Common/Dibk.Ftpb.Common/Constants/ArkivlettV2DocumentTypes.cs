﻿namespace Dibk.Ftpb.Common.Constants
{
    /// <summary>
    /// Arkivlett2 - https://www.ks.no/fagomrader/digitalisering/felleslosninger/verktoykasse-plan--og-byggesak/verktoy/sammenhengende-tjenester---integrasjoner/arkivlett/
    /// </summary>
    public static class ArkivlettV2DocumentTypes
    {
        public static readonly string Søknad = "SØKNAD";
        public static readonly string Melding = "MELDING";
        public static readonly string Kart = "KART";
        public static readonly string Foto = "FOTO";
        public static readonly string Tegning = "TEGNING";
        public static readonly string AnsvarOgKontroll = "ANSVKONT";
        public static readonly string Tilsyn = "TILSYN";
        public static readonly string Avtale = "AVTALE";
        public static readonly string Ferdigattest = "FERDIGATTEST";
        public static readonly string Planbeskrivelse = "PLANBESKR";
        public static readonly string Arealplankart = "PLANKART";
        public static readonly string Bestemmelser = "PLANBEST";
        public static readonly string Planprogram = "PLANPROG";
        public static readonly string JuridiskBindendeIllustrasjon = "JURILLUST";
        public static readonly string Illustrasjon = "ILLUST";
        public static readonly string Vedtak = "VEDTAK";
        public static readonly string Konsekvensutredning = "KONSUTR";
        public static readonly string Kunngjøring = "KUNNGJ";
        public static readonly string Rapport = "RAPPORT";
        public static readonly string RosAnalyse = "ROS-ANALYSE";
        public static readonly string Utredning = "UTREDNING";
        public static readonly string Uttalelse = "UTTALELSE";
        public static readonly string Innsigelse = "INNSIGELSE";
        public static readonly string Klage = "KLAGE";
        public static readonly string Saksframlegg = "SAKSFRAMLEGG";
        public static readonly string Saksfremlegg = "SAKSFREMLEGG";
        public static readonly string Tegnforklaring = "TEGNFKL";
        public static readonly string GeoreferertPlankart = "GEOREFPLAN";
        public static readonly string Korrespondanse = "KORR";

        public static readonly string System = "SYSTEM";
    }
}