# ---------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------

#!/usr/bin/env python

import logging
import traceback
import sys
import os
import time
import requests
import json
import datetime

from xml.dom.minidom import parse
import xml.dom.minidom






def createFolder(folderPath):
        """
        Creates a folder if it does not already exist        
    
        Args:
            folderPath (string), path to folder that needs to be created     
        """

        if not os.path.isdir(folderPath):
            os.makedirs(folderPath)


def logger_setup(cmd,loggingDir):
        """
        Sets up the logger function for the poject
    
        Args:
            cmd (string), name of the fuction that is being logged in this run of the tool
            loggingDir (string), file path where log is to be stored (no trailing \ )    
     

        Returns:
            logger object    
        """

        logger = logging.getLogger('DiBKparsing')
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler('{}\\DiBKparsing.{}.{}.log'.format(loggingDir,cmd, time.strftime("%Y%m%d_%H%M%S")))
        fh.setLevel(logging.DEBUG)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)

        return logger






### ------------------------ m a i n  c o d e -------------------------------- ###
def main(environment):
    """
    Main function: Python code entry.
    """

    try:
        # Setup logging
        loggingFolder = r'c:\temp'
        createFolder(loggingFolder)
        logger = logger_setup("Testlog", loggingFolder)
        logger.debug("Testlogg")

        runStartTime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

        inFil = r'C:\Temp\AltformangeXML.xml'

        # Read XML
        # Open XML document using minidom parser
        DOMTree = xml.dom.minidom.parse(inFil)
        collection = DOMTree.documentElement

        # Parse out <nabo-eier>, <nabogjenboer>       
        nabogjenboer = collection.getElementsByTagName("nabogjenboer")

        # Skrive ut liste
        idx = 1

        csvList = []
        csvList.append("Nummer;gaardsnummer;bruksnummer;festenummer;seksjonsnummer;nabo;partstype")

        for nabo in nabogjenboer:
            partstype = nabo.getElementsByTagName('partstype')[0]
            kodeverdi = partstype.getElementsByTagName('kodeverdi')[0]
            navn = nabo.getElementsByTagName('navn')[0]
            gjelderNaboeiendom = nabo.getElementsByTagName('gjelderNaboeiendom')[0]
            eiendomsidentifikasjon = gjelderNaboeiendom.getElementsByTagName('eiendomsidentifikasjon')[0]
            gaardsnummer = eiendomsidentifikasjon.getElementsByTagName('gaardsnummer')[0]
            bruksnummer = eiendomsidentifikasjon.getElementsByTagName('bruksnummer')[0]
            festenummer = eiendomsidentifikasjon.getElementsByTagName('festenummer')[0]
            seksjonsnummer = eiendomsidentifikasjon.getElementsByTagName('seksjonsnummer')[0]

            print(f"[{idx}]: {gaardsnummer.childNodes[0].data} {bruksnummer.childNodes[0].data} {festenummer.childNodes[0].data} {seksjonsnummer.childNodes[0].data} --  {navn.childNodes[0].data} ({kodeverdi.childNodes[0].data})")

            csvString = f"{idx};{gaardsnummer.childNodes[0].data};{bruksnummer.childNodes[0].data};{festenummer.childNodes[0].data};{seksjonsnummer.childNodes[0].data};{navn.childNodes[0].data};{kodeverdi.childNodes[0].data}"
            csvList.append(csvString)

            idx+=1

        
        # Write to fil;e
        outFile = open(f"{loggingFolder}\XMLdata.{runStartTime}.csv", 'w') 
        for line in csvList:
            outFile.write(line + "\n")    
        outFile.close()

        print("slutt")

    except:
        logger.debug("Unexpected error:", sys.exc_info()[0])
        logger.debug("{}".format(traceback.format_exc(limit=None, chain=True)))





## MAIN FUNCTION ... runs main() if invoked as a standalone script ##
if __name__ == '__main__':
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))    
    main(sys.argv[1])    
    


