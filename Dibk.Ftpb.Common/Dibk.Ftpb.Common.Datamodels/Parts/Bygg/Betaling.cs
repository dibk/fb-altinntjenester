﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Betaling
    {
        [XmlElement("beskrivelse")]
        public string Beskrivelse { get; set; }

        [XmlElement("ordreId")]
        public string OrdreId { get; set; }

        [XmlElement("sum")]
        public string Sum { get; set; }

        [XmlElement("transId")]
        public string TransId { get; set; }

        [XmlElement("gebyrkategori")]
        public string Gebyrkategori { get; set; }

        [XmlElement("skalFaktureres")]
        public bool? SkalFaktureres { get; set; }
    }
}