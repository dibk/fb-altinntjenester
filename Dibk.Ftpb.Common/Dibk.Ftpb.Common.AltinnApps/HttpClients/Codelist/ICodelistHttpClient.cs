﻿using DiBK.RuleValidator.Extensions;

namespace Dibk.Ftpb.Common.AltinnApps.HttpClients.Codelist
{
    public interface ICodelistHttpClient
    {
        Task<List<CodelistItem>> GetArealformålAsync();
        Task<List<CodelistItem>> GetFeltnavnAsync();
        Task<List<CodelistItem>> GetHensynskategoriAsync();
        Task<List<CodelistItem>> GetMålemetodeAsync();
    }
}