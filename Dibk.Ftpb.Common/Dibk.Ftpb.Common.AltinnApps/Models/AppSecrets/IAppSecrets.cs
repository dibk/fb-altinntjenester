﻿namespace Dibk.Ftpb.Common.AltinnApps.Models.AppSecrets
{
    public interface IAppSecrets
    {
        string PdfGeneratorApiKey { get; set; }
        string ArkitektumPrivateKey { get; set; }
        string? FormProcessApiSettingsUri { get; set; }
        string? FormProcessApiSettingsBasicAuthUserName { get; set; }
        string? FormProcessApiSettingsBasicAuthPassword { get; set; }
        string ElasticsearchNodeUri { get; set; }
    }
}