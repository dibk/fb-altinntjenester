﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class Dispensasjon
    {
        [XmlElement("dispensasjonstype")]
        public Kodeliste Dispensasjonstype { get; set; }

        [XmlElement("begrunnelse")]
        public string Begrunnelse { get; set; }

        [XmlElement("beskrivelse")]
        public string Beskrivelse { get; set; }
    }
}