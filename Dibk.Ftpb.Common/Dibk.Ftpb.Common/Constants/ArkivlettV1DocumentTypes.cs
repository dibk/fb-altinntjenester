﻿namespace Dibk.Ftpb.Common.Constants
{
    ///// <summary>
    ///// Arkivlett1
    ///// </summary>
    public static class ArkivlettV1DocumentTypes
    {
        public static readonly string Søk = "SØK";
        public static readonly string AnsvarOgKontroll = "ANKO";
        public static readonly string Foto = "FOTO";
        public static readonly string Kart = "KART";
        public static readonly string Korrespondanse = "KORR";
        public static readonly string Tegning = "TEGN";
        public static readonly string SøknadOmTillatelseIEttrinn = "SØK-ET";
        public static readonly string SøkOmRammetillatelse = "SØK-RS";
        public static readonly string SøkOmTiltakUtenAnsvarsrett = "SØK-TA";
        public static readonly string SøkOmDispensasjon = "SØK-DISP";
        public static readonly string SøkOmIgangsettingstillatelse = "SØK-IG";
        public static readonly string SøkOmMidlertidigBrukstillatelse = "SØK-MB";
        public static readonly string SøkOmFerdigattest = "SØK-FA";
        public static readonly string SøkOmEndringAvTillatelse = "SØK-ES";

        public static readonly string Kontroll = "KONTROLL";

        public static readonly string Planbeskrivelse = "PLANBESKR";
        public static readonly string Arealplankart = "PLANKART";
        public static readonly string Bestemmelser = "PLANBEST";
        public static readonly string JuridiskBindendeIllustrasjon = "JURILLUST";
        public static readonly string Illustrasjon = "ILLUST";
        public static readonly string Vedtak = "VEDTAK";
        public static readonly string Konsekvensutredning = "KONSUTR";
        public static readonly string Kunngjøring = "KUNNGJ";
        public static readonly string Rapport = "RAPPORT";
        public static readonly string Innsigelse = "INNSIGELSE";
        public static readonly string Klage = "KLAGE";
        public static readonly string Tegnforklaring = "TEGNFKL";
        public static readonly string GeoreferertPlankart = "GEOREFPLAN";

        // FT-327 og FBHS-2694 -  Gjennomføringsplan (både som underskjema og vedlegg) skal ha dokumenttype Gjennomføringsplan, ikke ANKO
        public static readonly string Gjennomføringsplan = "Gjennomføringsplan";
    }
}