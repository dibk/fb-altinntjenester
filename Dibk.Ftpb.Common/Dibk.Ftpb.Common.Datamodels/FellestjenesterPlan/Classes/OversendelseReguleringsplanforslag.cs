﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "https://skjema.ft.dibk.no/TEST/reguleringsplanforslag/v1")]
    [XmlRoot("OversendelseReguleringsplanforslag", Namespace = "https://skjema.ft.dibk.no/TEST/reguleringsplanforslag/v1")]
    public class OversendelseReguleringsplanforslag : DataTypeAttributesDatamodelBase
    {
        [XmlElement("planforslag")]
        public PlanforslagBase Planforslag { get; set; }

        [XmlElement("lovreferanse")]
        public Kodeliste Lovreferanse { get; set; }

        [XmlElement("sendesTilKommune")]
        public Kommune SendesTilKommune { get; set; }

        [XmlElement("versjon")]
        public Versjon Versjon { get; set; }

        [XmlElement("vedlegg")]
        public Kodeliste Vedlegg { get; set; }

        [XmlElement("metadata")]
        public MetadataRegfo Metadata { get; set; }

        [XmlElement("forslagsstiller")]
        public AktoerPlan Forslagsstiller { get; set; }

        [XmlElement("plankonsulent")]
        public AktoerPlan Plankonsulent { get; set; }

        [XmlElement("fakturamottaker")]
        public FakturamottakerBase Fakturamottaker { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 11001<br />
        /// <b>DataFormatVersion:</b> 1<br />
        /// <b>DataFormatProvider:</b> DIBK
        /// </summary>
        public OversendelseReguleringsplanforslag()
        {
            DataFormatProvider = "DIBK";
            DataFormatId = "11001";
            DataFormatVersion = "1";
        }
    }
}