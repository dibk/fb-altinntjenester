﻿#!/bin/bash

GENERATE_INDEX_SCRIPT="./generate-index-files.sh"

# Sjekk om generate-index-files.sh eksisterer
if [[ ! -f "$GENERATE_INDEX_SCRIPT" ]]; then
    echo "Error: generate-index-files.sh not found at $GENERATE_INDEX_SCRIPT"
    exit 1
fi

# Kjør skriptet for å generere index.html-filer
echo "Generating index.html files..."
chmod +x "$GENERATE_INDEX_SCRIPT"
"$GENERATE_INDEX_SCRIPT"

# Last miljøvariablene fra local-env.sh
if [ -f local-env.sh ]; then
    source local-env.sh
fi

# Synkroniser lokale filer i Dibk.Ftpb.Schemas/xsd til Azure Blob Storage (kun nye eller endrede filer)
echo "Syncing files to Azure Blob Storage..."

az storage blob sync --account-name $AZURE_STORAGE_ACCOUNT \
                     --account-key $AZURE_STORAGE_KEY \
                     --container $WEB_CONTAINER_NAME \
                     --source Dibk.Ftpb.Common/Dibk.Ftpb.Schemas/xsd

echo "Sync completed."