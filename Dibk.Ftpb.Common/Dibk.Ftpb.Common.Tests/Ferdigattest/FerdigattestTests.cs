using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Dibk.Ftpb.Common.Datamodels;

namespace Dibk.Ftpb.Common.Tests;

[TestClass]
public class FerdigattestTests
{
    [TestMethod]
    public void CurrentVersionIs4()
    {
        var fa = new Ferdigattest();
        Assert.IsInstanceOfType<FerdigattestV4>(fa);
        Assert.IsNotInstanceOfType<FerdigattestV3>(fa);
    }

    [TestMethod]
    public void DeserializeCurrentVersionToClassWithUnspecifiedVersion_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/ferdigattestV4.xml");

        var fa = XmlSerializerUtil.DeserializeFromString<Ferdigattest>(xmlContent, false);

        Assert.IsNotNull(fa);
    }

    [TestMethod]
    public void V3Data_DeserializeTo_V4Model_throws_Exception()
    {
        var xmlContent = File.ReadAllText("TestData/ferdigattestV3.xml");

        Assert.ThrowsException<InvalidOperationException>(new Action(() =>
            XmlSerializerUtil.DeserializeFromString<FerdigattestV4>(xmlContent, false)));
    }

    [TestMethod]
    public void FerdigattestV3_Deserialize_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/ferdigattestV3.xml");

        var fa3 = XmlSerializerUtil.DeserializeFromString<FerdigattestV3>(xmlContent, false);

        Assert.IsNotNull(fa3);

        //DataFormat-attributter
        Assert.AreEqual("DIBK", fa3.DataFormatProvider);
        Assert.AreEqual("10005", fa3.DataFormatId);
        Assert.AreEqual("3.0", fa3.DataFormatVersion);

        //EiendomByggested
        Assert.AreEqual(2, fa3.EiendomByggested.Length);
        var eiendom = fa3.EiendomByggested[0];
        Assert.IsNotNull(eiendom?.Eiendomsidentifikasjon);
        Assert.AreEqual("8", eiendom.Eiendomsidentifikasjon.Bruksnummer);
        Assert.AreEqual("1", eiendom.Eiendomsidentifikasjon.Festenummer);
        Assert.AreEqual("567", eiendom.Eiendomsidentifikasjon.Gaardsnummer);
        Assert.AreEqual("1234", eiendom.Eiendomsidentifikasjon.Kommunenummer);
        Assert.AreEqual("2", eiendom.Eiendomsidentifikasjon.Seksjonsnummer);
        Assert.IsNotNull(eiendom.Adresse);
        Assert.AreEqual("Gateadresse 123", eiendom.Adresse.Adresselinje1);
        Assert.AreEqual("1234", eiendom.Adresse.Postnr);
        Assert.AreEqual("Poststed", eiendom.Adresse.Poststed);
        Assert.AreEqual("NO", eiendom.Adresse.Landkode);
        Assert.AreEqual("123", eiendom.Bygningsnummer);
        Assert.AreEqual("1", eiendom.Bolignummer);
        Assert.AreEqual("Kommune", eiendom.Kommunenavn);

        //KommunensSaksnummer
        Assert.IsNotNull(fa3.KommunensSaksnummer);
        Assert.AreEqual(2024, fa3.KommunensSaksnummer.Saksaar);
        Assert.AreEqual(12345, fa3.KommunensSaksnummer.Sakssekvensnummer);

        //Metadata
        Assert.IsNotNull(fa3.Metadata);
        Assert.AreEqual("Systemnavn", fa3.Metadata.FraSluttbrukersystem);
        Assert.AreEqual("98765", fa3.Metadata.FtbId);
        Assert.AreEqual("Prosjektnavn", fa3.Metadata.Prosjektnavn);
        Assert.AreEqual("54321", fa3.Metadata.Prosjektnr);
        Assert.IsNotNull(fa3.Metadata.ForetrukketSpraak);
        Assert.AreEqual("NO", fa3.Metadata.ForetrukketSpraak.Kodeverdi);
        Assert.AreEqual("Norsk", fa3.Metadata.ForetrukketSpraak.Kodebeskrivelse);

        //GenerelleVilkaar
        Assert.IsNotNull(fa3.GenerelleVilkaar);
        Assert.AreEqual(true, fa3.GenerelleVilkaar.NorskSvenskDansk);

        //SoeknadGjelder
        Assert.IsNotNull(fa3.SoeknadGjelder);
        Assert.IsNotNull(fa3.SoeknadGjelder.Type);
        Assert.AreEqual(2, fa3.SoeknadGjelder.Type.Length);
        var soeknadGjelderType = fa3.SoeknadGjelder.Type[0];
        Assert.AreEqual("Code1", soeknadGjelderType.Kodeverdi);
        Assert.AreEqual("Description1", soeknadGjelderType.Kodebeskrivelse);
        Assert.AreEqual("Her er et følgebrev", fa3.SoeknadGjelder.Foelgebrev);

        //UtfallBesvarelse
        Assert.IsNotNull(fa3.UtfallBesvarelse);
        Assert.AreEqual(2, fa3.UtfallBesvarelse.Length);
        var utfallSvar = fa3.UtfallBesvarelse[0];
        Assert.AreEqual("123", utfallSvar.UtfallId);
        Assert.IsNotNull(utfallSvar.Utfalltype);
        Assert.AreEqual("UTFALL1", utfallSvar.Utfalltype.Kodeverdi);
        Assert.AreEqual("Utfall 1", utfallSvar.Utfalltype.Kodebeskrivelse);
        Assert.IsNotNull(utfallSvar.UtloestFraSjekkpunkt);
        Assert.AreEqual("Sjekkpunkt_1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktId);
        Assert.AreEqual("Sjekkpunkteier 1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktEier);
        Assert.IsNotNull(utfallSvar.Tema);
        Assert.AreEqual("Theme1", utfallSvar.Tema.Kodeverdi);
        Assert.AreEqual("ThemeDescription1", utfallSvar.Tema.Kodebeskrivelse);
        Assert.AreEqual("Utfall 1 tittel", utfallSvar.Tittel);
        Assert.AreEqual("Dette er utfall 1", utfallSvar.Beskrivelse);
        Assert.AreEqual(false, utfallSvar.ErUtfallBesvaresSenere);
        Assert.AreEqual(true, utfallSvar.ErUtfallBesvart);
        Assert.AreEqual("Utfall 1 kommentar", utfallSvar.Kommentar);
        //Vedlegg
        Assert.IsNotNull(utfallSvar.Vedleggsliste);
        Assert.AreEqual(2, utfallSvar.Vedleggsliste.Length);
        var vedlegg = utfallSvar.Vedleggsliste[0];
        Assert.AreEqual("1", vedlegg.Versjonsnummer);
        Assert.IsNotNull(vedlegg.Vedleggstype);
        Assert.AreEqual("Type1", vedlegg.Vedleggstype.Kodeverdi);
        Assert.AreEqual("TypeDescription1", vedlegg.Vedleggstype.Kodebeskrivelse);
        Assert.AreEqual(new DateTime(2024, 03, 12), vedlegg.Versjonsdato);
        Assert.AreEqual("File1.pdf", vedlegg.Filnavn);
        //Underskjema
        Assert.IsNotNull(utfallSvar.Underskjema);
        Assert.AreEqual(2, utfallSvar.Underskjema.Length);
        var underskjema = utfallSvar.Underskjema[0];
        Assert.AreEqual("UNDERSKJEMA1", underskjema.Kodeverdi);
        Assert.AreEqual("Underskjema 1", underskjema.Kodebeskrivelse);

        //KravFerdigattest
        Assert.IsNotNull(fa3.KravFerdigattest);
        Assert.AreEqual(true, fa3.KravFerdigattest.TilfredsstillerTiltaketKraveneFerdigattest);
        Assert.AreEqual(new DateTime(2024, 03, 30), fa3.KravFerdigattest.UtfoertInnen);
        Assert.AreEqual("Alt", fa3.KravFerdigattest.TypeArbeider);
        Assert.AreEqual(new DateTime(2024, 03, 29), fa3.KravFerdigattest.BekreftelseInnen);

        //Rot-egenskaper
        Assert.AreEqual(true, fa3.ErForetattIkkeSoeknadspliktigeJusteringer);
        Assert.AreEqual(true, fa3.ErTilstrekkeligDokumentasjonOverlevertEier);

        //Varmesystem
        Assert.IsNotNull(fa3.Varmesystem);
        Assert.AreEqual(2, fa3.Varmesystem.Varmefordeling.Length);
        Assert.AreEqual("Code1", fa3.Varmesystem.Varmefordeling[0].Kodeverdi);
        Assert.AreEqual("Description1", fa3.Varmesystem.Varmefordeling[0].Kodebeskrivelse);
        Assert.AreEqual(2, fa3.Varmesystem.Energiforsyning.Length);
        Assert.AreEqual("Code3", fa3.Varmesystem.Energiforsyning[0].Kodeverdi);
        Assert.AreEqual("Description3", fa3.Varmesystem.Energiforsyning[0].Kodebeskrivelse);
        Assert.AreEqual(true, fa3.Varmesystem.ErRelevant);

        //Tiltakshaver
        var tiltakshaver = fa3.Tiltakshaver;
        Assert.IsNotNull(tiltakshaver);
        Assert.IsNotNull(tiltakshaver.Partstype);
        Assert.AreEqual("TYPE3", tiltakshaver.Partstype.Kodeverdi);
        Assert.AreEqual("Type 3", tiltakshaver.Partstype.Kodebeskrivelse);
        Assert.AreEqual("12345678910", tiltakshaver.Foedselsnummer);
        Assert.AreEqual("987654321", tiltakshaver.Organisasjonsnummer);
        Assert.AreEqual("Tiltakshaver 1", tiltakshaver.Navn);
        Assert.IsNotNull(tiltakshaver.Adresse);
        Assert.AreEqual("Gateadresse 789", tiltakshaver.Adresse.Adresselinje1);
        Assert.AreEqual("9876", tiltakshaver.Adresse.Postnr);
        Assert.AreEqual("Poststed 3", tiltakshaver.Adresse.Poststed);
        Assert.AreEqual("NO", tiltakshaver.Adresse.Landkode);
        Assert.AreEqual("12345678", tiltakshaver.Telefonnummer);
        Assert.AreEqual("98765432", tiltakshaver.Mobilnummer);
        Assert.AreEqual("tiltakshaver1@example.com", tiltakshaver.Epost);
        var kontaktpersonTth = tiltakshaver.Kontaktperson;
        Assert.IsNotNull(kontaktpersonTth);
        Assert.AreEqual("Kontaktperson 1", kontaktpersonTth.Navn);
        Assert.AreEqual("11111111", kontaktpersonTth.Telefonnummer);
        Assert.AreEqual("22222222", kontaktpersonTth.Mobilnummer);
        Assert.AreEqual("kontaktperson1@example.com", kontaktpersonTth.Epost);

        //AnsvarligSoeker
        Assert.IsNotNull(fa3.AnsvarligSoeker);
        Assert.IsNotNull(fa3.AnsvarligSoeker.Partstype);
        Assert.AreEqual("Code2", fa3.AnsvarligSoeker.Partstype.Kodeverdi);
        Assert.AreEqual("Description2", fa3.AnsvarligSoeker.Partstype.Kodebeskrivelse);
        Assert.AreEqual("98765432109", fa3.AnsvarligSoeker.Foedselsnummer);
        Assert.AreEqual("123456789", fa3.AnsvarligSoeker.Organisasjonsnummer);
        Assert.AreEqual("Ansvarlig Søker 1", fa3.AnsvarligSoeker.Navn);
        Assert.IsNotNull(fa3.AnsvarligSoeker.Adresse);
        Assert.AreEqual("Gateadresse 101", fa3.AnsvarligSoeker.Adresse.Adresselinje1);
        Assert.AreEqual("1111", fa3.AnsvarligSoeker.Adresse.Postnr);
        Assert.AreEqual("Poststed 4", fa3.AnsvarligSoeker.Adresse.Poststed);
        Assert.AreEqual("NO", fa3.AnsvarligSoeker.Adresse.Landkode);
        Assert.AreEqual("99999999", fa3.AnsvarligSoeker.Telefonnummer);
        Assert.AreEqual("88888888", fa3.AnsvarligSoeker.Mobilnummer);
        Assert.AreEqual("ansvarligsoeker1@example.com", fa3.AnsvarligSoeker.Epost);
        var kontaktperson = fa3.AnsvarligSoeker.Kontaktperson;
        Assert.IsNotNull(kontaktperson);
        Assert.AreEqual("Kontaktperson 2", kontaktperson.Navn);
        Assert.AreEqual("33333333", kontaktperson.Telefonnummer);
        Assert.AreEqual("44444444", kontaktperson.Mobilnummer);
        Assert.AreEqual("kontaktperson2@example.com", kontaktperson.Epost);

        //AnsvarForByggesaken
        Assert.IsNotNull(fa3.AnsvarForByggesaken);
        Assert.AreEqual("Code3", fa3.AnsvarForByggesaken.Kodeverdi);
        Assert.AreEqual("Description3", fa3.AnsvarForByggesaken.Kodebeskrivelse);
    }

    [TestMethod]
    public void FerdigattestV4_Deserialize_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/ferdigattestV4.xml");

        var fa4 = XmlSerializerUtil.DeserializeFromString<FerdigattestV4>(xmlContent, false);

        Assert.IsNotNull(fa4);

        //DataFormat-attributter
        Assert.AreEqual("DIBK", fa4.DataFormatProvider);
        Assert.AreEqual("10005", fa4.DataFormatId);
        Assert.AreEqual("4", fa4.DataFormatVersion);

        //EiendomByggested
        Assert.AreEqual(2, fa4.EiendomByggested.Length);
        var eiendom = fa4.EiendomByggested[0];
        Assert.IsNotNull(eiendom?.Eiendomsidentifikasjon);
        Assert.AreEqual("8", eiendom.Eiendomsidentifikasjon.Bruksnummer);
        Assert.AreEqual("1", eiendom.Eiendomsidentifikasjon.Festenummer);
        Assert.AreEqual("567", eiendom.Eiendomsidentifikasjon.Gaardsnummer);
        Assert.AreEqual("1234", eiendom.Eiendomsidentifikasjon.Kommunenummer);
        Assert.AreEqual("2", eiendom.Eiendomsidentifikasjon.Seksjonsnummer);
        Assert.IsNotNull(eiendom.Adresse);
        Assert.AreEqual("Gateadresse 123", eiendom.Adresse.Adresselinje1);
        Assert.AreEqual("1234", eiendom.Adresse.Postnr);
        Assert.AreEqual("Poststed", eiendom.Adresse.Poststed);
        Assert.AreEqual("NO", eiendom.Adresse.Landkode);
        Assert.AreEqual("123", eiendom.Bygningsnummer);
        Assert.AreEqual("1", eiendom.Bolignummer);
        Assert.AreEqual("Kommune", eiendom.Kommunenavn);

        //KommunensSaksnummer
        Assert.IsNotNull(fa4.KommunensSaksnummer);
        Assert.AreEqual(2024, fa4.KommunensSaksnummer.Saksaar);
        Assert.AreEqual(12345, fa4.KommunensSaksnummer.Sakssekvensnummer);

        //Metadata
        Assert.IsNotNull(fa4.Metadata);
        Assert.AreEqual("Systemnavn", fa4.Metadata.FraSluttbrukersystem);
        Assert.AreEqual("98765", fa4.Metadata.FtbId);
        Assert.AreEqual("Prosjektnavn", fa4.Metadata.Prosjektnavn);
        Assert.AreEqual("54321", fa4.Metadata.Prosjektnr);
        Assert.IsNotNull(fa4.Metadata.ForetrukketSpraak);
        Assert.AreEqual("NO", fa4.Metadata.ForetrukketSpraak.Kodeverdi);
        Assert.AreEqual("Norsk", fa4.Metadata.ForetrukketSpraak.Kodebeskrivelse);

        //GenerelleVilkaar
        Assert.IsNotNull(fa4.GenerelleVilkaar);
        Assert.AreEqual(true, fa4.GenerelleVilkaar.NorskSvenskDansk);

        //SoeknadGjelder
        Assert.IsNotNull(fa4.SoeknadGjelder);
        Assert.IsNotNull(fa4.SoeknadGjelder.Type);
        Assert.AreEqual(2, fa4.SoeknadGjelder.Type.Length);
        var soeknadGjelderType = fa4.SoeknadGjelder.Type[0];
        Assert.AreEqual("Code1", soeknadGjelderType.Kodeverdi);
        Assert.AreEqual("Description1", soeknadGjelderType.Kodebeskrivelse);
        Assert.AreEqual("Her er et følgebrev", fa4.SoeknadGjelder.Foelgebrev);

        //UtfallBesvarelse
        Assert.IsNotNull(fa4.UtfallBesvarelse);
        Assert.AreEqual(2, fa4.UtfallBesvarelse.Length);
        var utfallSvar = fa4.UtfallBesvarelse[0];
        Assert.AreEqual("123", utfallSvar.UtfallId);
        Assert.IsNotNull(utfallSvar.Utfalltype);
        Assert.AreEqual("UTFALL1", utfallSvar.Utfalltype.Kodeverdi);
        Assert.AreEqual("Utfall 1", utfallSvar.Utfalltype.Kodebeskrivelse);
        Assert.IsNotNull(utfallSvar.UtloestFraSjekkpunkt);
        Assert.AreEqual("Sjekkpunkt_1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktId);
        Assert.AreEqual("Sjekkpunkteier 1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktEier);
        Assert.IsNotNull(utfallSvar.Tema);
        Assert.AreEqual("Theme1", utfallSvar.Tema.Kodeverdi);
        Assert.AreEqual("ThemeDescription1", utfallSvar.Tema.Kodebeskrivelse);
        Assert.AreEqual("Utfall 1 tittel", utfallSvar.Tittel);
        Assert.AreEqual("Dette er utfall 1", utfallSvar.Beskrivelse);
        Assert.AreEqual(false, utfallSvar.ErUtfallBesvaresSenere);
        Assert.AreEqual(true, utfallSvar.ErUtfallBesvart);
        Assert.AreEqual("Utfall 1 kommentar", utfallSvar.Kommentar);
        //Vedlegg
        Assert.IsNotNull(utfallSvar.Vedleggsliste);
        Assert.AreEqual(2, utfallSvar.Vedleggsliste.Length);
        var vedlegg = utfallSvar.Vedleggsliste[0];
        Assert.AreEqual("1", vedlegg.Versjonsnummer);
        Assert.IsNotNull(vedlegg.Vedleggstype);
        Assert.AreEqual("Type1", vedlegg.Vedleggstype.Kodeverdi);
        Assert.AreEqual("TypeDescription1", vedlegg.Vedleggstype.Kodebeskrivelse);
        Assert.AreEqual(new DateTime(2024, 03, 12), vedlegg.Versjonsdato);
        Assert.AreEqual("File1.pdf", vedlegg.Filnavn);

        //KravFerdigattest
        Assert.IsNotNull(fa4.KravFerdigattest);
        Assert.AreEqual(true, fa4.KravFerdigattest.TilfredsstillerTiltaketKraveneFerdigattest);
        Assert.AreEqual(new DateTime(2024, 03, 30), fa4.KravFerdigattest.UtfoertInnen);
        Assert.AreEqual("Alt", fa4.KravFerdigattest.TypeArbeider);
        Assert.AreEqual(new DateTime(2024, 03, 29), fa4.KravFerdigattest.BekreftelseInnen);

        //Rot-egenskaper
        Assert.AreEqual(true, fa4.ErForetattIkkeSoeknadspliktigeJusteringer);
        Assert.AreEqual(true, fa4.ErTilstrekkeligDokumentasjonOverlevertEier);

        //Varmesystem
        Assert.IsNotNull(fa4.Varmesystem);
        Assert.AreEqual(2, fa4.Varmesystem.Varmefordeling.Length);
        Assert.AreEqual("Code1", fa4.Varmesystem.Varmefordeling[0].Kodeverdi);
        Assert.AreEqual("Description1", fa4.Varmesystem.Varmefordeling[0].Kodebeskrivelse);
        Assert.AreEqual(2, fa4.Varmesystem.Energiforsyning.Length);
        Assert.AreEqual("Code3", fa4.Varmesystem.Energiforsyning[0].Kodeverdi);
        Assert.AreEqual("Description3", fa4.Varmesystem.Energiforsyning[0].Kodebeskrivelse);
        Assert.AreEqual(true, fa4.Varmesystem.ErRelevant);

        //Tiltakshaver
        var tiltakshaver = fa4.Tiltakshaver;
        Assert.IsNotNull(tiltakshaver);
        Assert.IsNotNull(tiltakshaver.Partstype);
        Assert.AreEqual("TYPE3", tiltakshaver.Partstype.Kodeverdi);
        Assert.AreEqual("Type 3", tiltakshaver.Partstype.Kodebeskrivelse);
        Assert.AreEqual("12345678910", tiltakshaver.Foedselsnummer);
        Assert.AreEqual("987654321", tiltakshaver.Organisasjonsnummer);
        Assert.AreEqual("Tiltakshaver 1", tiltakshaver.Navn);
        Assert.IsNotNull(tiltakshaver.Adresse);
        Assert.AreEqual("Gateadresse 789", tiltakshaver.Adresse.Adresselinje1);
        Assert.AreEqual("9876", tiltakshaver.Adresse.Postnr);
        Assert.AreEqual("Poststed 3", tiltakshaver.Adresse.Poststed);
        Assert.AreEqual("NO", tiltakshaver.Adresse.Landkode);
        Assert.AreEqual("12345678", tiltakshaver.Telefonnummer);
        Assert.AreEqual("98765432", tiltakshaver.Mobilnummer);
        Assert.AreEqual("tiltakshaver1@example.com", tiltakshaver.Epost);
        var kontaktpersonTth = tiltakshaver.Kontaktperson;
        Assert.IsNotNull(kontaktpersonTth);
        Assert.AreEqual("Kontaktperson 1", kontaktpersonTth.Navn);
        Assert.AreEqual("11111111", kontaktpersonTth.Telefonnummer);
        Assert.AreEqual("22222222", kontaktpersonTth.Mobilnummer);
        Assert.AreEqual("kontaktperson1@example.com", kontaktpersonTth.Epost);

        //AnsvarligSoeker
        Assert.IsNotNull(fa4.AnsvarligSoeker);
        Assert.IsNotNull(fa4.AnsvarligSoeker.Partstype);
        Assert.AreEqual("Code2", fa4.AnsvarligSoeker.Partstype.Kodeverdi);
        Assert.AreEqual("Description2", fa4.AnsvarligSoeker.Partstype.Kodebeskrivelse);
        Assert.AreEqual("98765432109", fa4.AnsvarligSoeker.Foedselsnummer);
        Assert.AreEqual("123456789", fa4.AnsvarligSoeker.Organisasjonsnummer);
        Assert.AreEqual("Ansvarlig Søker 1", fa4.AnsvarligSoeker.Navn);
        Assert.IsNotNull(fa4.AnsvarligSoeker.Adresse);
        Assert.AreEqual("Gateadresse 101", fa4.AnsvarligSoeker.Adresse.Adresselinje1);
        Assert.AreEqual("1111", fa4.AnsvarligSoeker.Adresse.Postnr);
        Assert.AreEqual("Poststed 4", fa4.AnsvarligSoeker.Adresse.Poststed);
        Assert.AreEqual("NO", fa4.AnsvarligSoeker.Adresse.Landkode);
        Assert.AreEqual("99999999", fa4.AnsvarligSoeker.Telefonnummer);
        Assert.AreEqual("88888888", fa4.AnsvarligSoeker.Mobilnummer);
        Assert.AreEqual("ansvarligsoeker1@example.com", fa4.AnsvarligSoeker.Epost);
        var kontaktperson = fa4.AnsvarligSoeker.Kontaktperson;
        Assert.IsNotNull(kontaktperson);
        Assert.AreEqual("Kontaktperson 2", kontaktperson.Navn);
        Assert.AreEqual("33333333", kontaktperson.Telefonnummer);
        Assert.AreEqual("44444444", kontaktperson.Mobilnummer);
        Assert.AreEqual("kontaktperson2@example.com", kontaktperson.Epost);

        //AnsvarForByggesaken
        Assert.IsNotNull(fa4.AnsvarForByggesaken);
        Assert.AreEqual("Code3", fa4.AnsvarForByggesaken.Kodeverdi);
        Assert.AreEqual("Description3", fa4.AnsvarForByggesaken.Kodebeskrivelse);
    }
}
