﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class Gjeldendeplan
    {
        [XmlElement("navn")]
        public string Navn { get; set; }

        [XmlElement("plantype")]
        public Kodeliste Plantype { get; set; }
    }
}