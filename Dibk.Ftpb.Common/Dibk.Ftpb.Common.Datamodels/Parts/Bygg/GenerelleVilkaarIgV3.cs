﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class GenerelleVilkaarIgV3 : GenerelleVilkaar
    {
        [XmlElement("beroererArbeidsplasser")]
        public bool? BeroererArbeidsplasser { get; set; }
    }
}