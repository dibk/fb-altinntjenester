# ---------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------

#!/usr/bin/env python

import logging
import traceback
import sys
import os
import time
import requests
import json
import datetime

from xml.dom import minidom


def createFolder(folderPath):
        """
        Creates a folder if it does not already exist        
    
        Args:
            folderPath (string), path to folder that needs to be created     
        """

        if not os.path.isdir(folderPath):
            os.makedirs(folderPath)


def logger_setup(cmd,loggingDir):
        """
        Sets up the logger function for the poject
    
        Args:
            cmd (string), name of the fuction that is being logged in this run of the tool
            loggingDir (string), file path where log is to be stored (no trailing \ )    
     

        Returns:
            logger object    
        """

        logger = logging.getLogger('DiBKparsing')
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler('{}\\DiBKparsing.{}.{}.log'.format(loggingDir,cmd, time.strftime("%Y%m%d_%H%M%S")))
        fh.setLevel(logging.DEBUG)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)

        return logger






### ------------------------ m a i n  c o d e -------------------------------- ###
def main(environment):
    """
    Main function: Python code entry.
    """

    try:
        # Setup logging
        loggingFolder = r'c:\temp'
        createFolder(loggingFolder)
        logger = logger_setup("Testlog", loggingFolder)
        logger.debug("Testlogg")

        runStartTime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

        inFile = r'C:\Users\toroskar\Downloads\Correspondence.xml'
        
        mydoc = minidom.parse(inFile)

        items = mydoc.getElementsByTagName('b:StatusV2') 

        for item in items:
            reportee = item.getElementsByTagName("b:Reportee")[0]
            date = item.getElementsByTagName("b:CreatedDate")[0]
            sendersReference = item.getElementsByTagName("b:SendersReference")[0]
            statusList = item.getElementsByTagName("b:StatusChangeV2")
            status = statusList[-1].getElementsByTagName("b:StatusType")[0]

            if reportee.firstChild.data == '992496843':
                print(f"-----------------------------------")
                print(f"Reportee: {reportee.firstChild.data}")
                print(f"CreatedDate: {date.firstChild.data}")
                print(f"SendersReference: {sendersReference.firstChild.data}")
                print(f"Status: {status.firstChild.data}")
                

            
        


        # Write to fil;e
        #outFile = open(f"{loggingFolder}\XMLdata.{runStartTime}.csv", 'w') 
        #for line in csvList:
        #    outFile.write(line + "\n")    
        #outFile.close()

        print("slutt")

    except:
        logger.debug("Unexpected error:", sys.exc_info()[0])
        logger.debug("{}".format(traceback.format_exc(limit=None, chain=True)))





## MAIN FUNCTION ... runs main() if invoked as a standalone script ##
if __name__ == '__main__':
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))    
    main(sys.argv[1])    
    


