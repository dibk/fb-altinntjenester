﻿#!/bin/bash

# Funksjon for å generere index.html-filer
generate_index() {
    local dir=$1
    local relative_path=$(echo "$dir" | sed "s|Dibk.Ftpb.Common/Dibk.Ftpb.Schemas/xsd||")
    local title="skjema.ft.dibk.no - $relative_path/"

    echo "Generating index for directory: $relative_path"

    local content=""
    content+='<!DOCTYPE html>\n'
    content+='<html lang="en">\n'
    content+='<head>\n'
    content+='    <meta charset="UTF-8">\n'
    content+='    <meta name="viewport" content="width=device-width, initial-scale=1.0">\n'
    content+="    <title>$title</title>\n"
    content+='</head>\n'
    content+='<body>\n'
    content+="    <h1>$title</h1>\n"
    content+='    <hr>\n'
    content+='    <pre>'

    # Legg til en lenke til foreldre-mappen hvis vi ikke er i rot-mappen
    if [[ "$relative_path" != "" ]]; then
         parent_path=$(echo "$relative_path" | sed 's|/[^/]*$||')  # Fjerner siste segment fra relative_path
        if [[ -z "$parent_path" ]]; then
            parent_path="/"  # Hvis vi er direkte under rot, setter vi lenken til "/"
        fi
        content+="<a href=\"$parent_path\">[To Parent Directory]</a><br><br>"
    fi

    # Generer en liste over mapper og filer, men utelat index.html
    for entry in "$dir"/*; do
        # Hent endringsdatoen for filen eller mappen
        timestamp=$(stat -c "%y" "$entry" | cut -d'.' -f1)  # Fjerner mikrosekunder fra datoen
        timestamp=$(date -d "$timestamp" "+%m/%d/%Y %I:%M %p")  # Formaterer datoen riktig

        if [ -d "$entry" ]; then
            if [[ "$relative_path" == "" ]] && [[ $(basename "$entry") == "TEST" ]]; then
                continue
            fi
            folder_name=$(basename "$entry")
            content+=$(printf "%-19s %18s <a href=\"%s/%s/\">%s</a><br>" "$timestamp" "&lt;dir&gt;" "$relative_path" "$folder_name" "$folder_name")
        elif [ -f "$entry" ] && [[ $(basename "$entry") != "index.html" ]]; then
            file_size=$(stat --format="%s" "$entry")
            file_name=$(basename "$entry")
            content+=$(printf "%-19s %12s <a href=\"%s/%s\">%s</a><br>" "$timestamp" "$file_size" "$relative_path" "$file_name" "$file_name")
        fi
    done

    content+='</pre>\n'
    content+='    <hr>\n'
    content+='</body>\n'
    content+='</html>\n'

    # Skriv hele innholdet til index.html-filen
    echo -e "$content" > "$dir/index.html"
}

# Eksportér funksjonen slik at den kan brukes av find
export -f generate_index

# Gå gjennom alle kataloger i Dibk.Ftpb.Schemas/xsd og generer index.html-filer
find Dibk.Ftpb.Common/Dibk.Ftpb.Schemas/xsd -type d \( -name 'TEST' -prune \) -o -type d -exec bash -c 'generate_index "$0"' {} \;

echo "Index files generated."