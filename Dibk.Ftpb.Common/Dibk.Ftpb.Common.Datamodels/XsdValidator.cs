﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;

namespace Dibk.Ftpb.Common.Datamodels
{
    /// <summary>
    /// The XsdValidator class is used to validate XML data against a specified XML Schema Definition (XSD). 
    /// It provides a method to validate an XML string against a schema, and returns a report of any validation errors or warnings.
    /// </summary>
    public class XsdValidator
    {
        private Dictionary<string, List<string>> xmlValidationMessages = new Dictionary<string, List<string>>();
        private const int xmlValidationErrorLimit = 25;
        private static List<string> _xmlErrorList = new List<string>();
        private static List<string> _xmlWarningList = new List<string>();

        /// <summary>
        /// Validates the provided XML string against the specified XML Schema Definition (XSD). 
        /// The method returns a dictionary containing lists of validation errors and warnings, if any.
        /// </summary>
        /// <param name="xml">The XML string to validate.</param>
        /// <param name="xmlSchemaName">The name of the XSD file to validate against.</param>
        /// <returns>A dictionary containing lists of validation errors and warnings.</returns>
        public Dictionary<string, List<string>> ValidateXml(string xml, string xmlSchemaName)
        {
            object result = null;
            Dictionary<string, List<string>> validationReport = new Dictionary<string, List<string>>();
            TextReader reader = null;
            try
            {
                _xmlErrorList = new List<string>();
                _xmlWarningList = new List<string>();

                //Todo: fix this, so xsd files can be fetched from NuGet package, and the remove folder "Datamodels"
                using (Stream stream = XsdResolver.GetXsd(xmlSchemaName))
                {
                    if (stream == null)
                    {
                        _xmlErrorList.Add($"Can't find xsd file {xmlSchemaName}.xsd");
                    }
                    else
                    {
                        var schemas = new XmlSchemaSet();
                        schemas.Add(null, XmlReader.Create(stream));

                        XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
                        xmlReaderSettings.ValidationType = ValidationType.Schema;
                        xmlReaderSettings.Schemas.Add(schemas);
                        xmlReaderSettings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                        xmlReaderSettings.ValidationEventHandler += ValidationCallBack;
                        XmlReader validationReader = XmlReader.Create(new StringReader(xml), xmlReaderSettings);

                        while (validationReader.Read())
                        {
                            if (_xmlErrorList.Count >= xmlValidationErrorLimit || _xmlWarningList.Count >= xmlValidationErrorLimit)
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _xmlErrorList.Add(ex.Message);
            }
            finally
            {
                reader?.Close();
            }
            if (_xmlErrorList.Any())
                validationReport.Add("Error", _xmlErrorList);
            if (_xmlWarningList.Any())
                validationReport.Add("Warning", _xmlWarningList);

            return validationReport;
        }

        private void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
            {
                _xmlWarningList.Add("linje " + args.Exception.LineNumber + ", posisjon " + args.Exception.LinePosition + " " + args.Message);
            }
            else if (args.Severity == XmlSeverityType.Error)
            {
                _xmlErrorList.Add("linje " + args.Exception.LineNumber + ", posisjon " + args.Exception.LinePosition + " " + args.Message);
            }
        }
    }
}