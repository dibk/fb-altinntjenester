﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Common.Tests
{
    [TestClass]
    public class DataTypeMapperTests
    {
        [TestMethod]
        public void GetConfig_ExistingValue_returns_Value2()
        {
            var dataType = "Nabovarsel";
            //var dataType = Constants.DataTypes.NabovarselVedlegg; ;
            var attachmentInfo = DataTypeMapper.Get(dataType);

            Assert.IsNotNull(attachmentInfo);
            Assert.IsTrue(attachmentInfo != default);
        }

        [TestMethod]
        public void GetConfig_ExistingValue_returns_Value()
        {
            var dataType = Constants.DataTypes.KartDetaljert;
            var attachmentInfo = DataTypeMapper.Get(dataType);

            Assert.IsNotNull(attachmentInfo);
            Assert.IsTrue(attachmentInfo != default);
        }

        [TestMethod]
        public void GetConfig_ExistingButWrongCasing_throws_KeyNotFoundException()
        {
            string dataType = Constants.DataTypes.KartDetaljert.ToLower();

            Assert.ThrowsException<KeyNotFoundException>(() => DataTypeMapper.Get(dataType));
        }

        [TestMethod]
        public void GetConfig_NonExistingType_throws_KeyNotFoundException()
        {
            string dataType = "DataTypeSomIkkjeFinst";

            Assert.ThrowsException<KeyNotFoundException>(() => DataTypeMapper.Get(dataType));
        }

        [TestMethod]
        public void GetConfig_NullInput_throws_ArgumentNullException()
        {
            string? dataType = null;

            Assert.ThrowsException<ArgumentNullException>(() => DataTypeMapper.Get(dataType));
        }

        [TestMethod]
        public void GetConfigByDisplayName_Gjennomføringsplan_returns_multipleresults()
        {
            string dataType = "Gjennomføringsplan";

            var retVal = DataTypeMapper.GetByDisplayName(dataType);

            Assert.IsNotNull(retVal);
            Assert.IsTrue(retVal != default);
        }

        [TestMethod]
        public void GetConfigByDisplayName_Gjennomføringsplan_subForm_returns_oneResult()
        {
            string dataType = "Gjennomføringsplan";

            var retVal = DataTypeMapper.GetByDisplayName(dataType, Models.ConfigType.SubForm);

            Assert.IsNotNull(retVal);
            Assert.IsTrue(retVal != default);
            Assert.IsTrue(retVal.Count == 1);
            Assert.IsTrue(retVal[0].LimitedTo.Contains(Models.ConfigType.SubForm));
        }

        [TestMethod]
        public void GetConfigByDisplayName_Gjennomføringsplan_mainForm_returns_oneResult()
        {
            string dataType = "Gjennomføringsplan";

            var retVal = DataTypeMapper.GetByDisplayName(dataType, Models.ConfigType.MainForm);

            Assert.IsNotNull(retVal);
            Assert.IsTrue(retVal != default);
            Assert.IsTrue(retVal.Count == 1);
            Assert.IsTrue(retVal[0].LimitedTo.Contains(Models.ConfigType.MainForm));
        }

        [TestMethod]
        public void GetConfigByDisplayName_Gjennomføringsplan_undefined_returns_multiple()
        {
            string dataType = "Gjennomføringsplan";

            var retVal = DataTypeMapper.GetByDisplayName(dataType);

            Assert.IsNotNull(retVal);
            Assert.IsTrue(retVal != default);
            Assert.IsTrue(retVal.Count == 3);
        }
    }
}