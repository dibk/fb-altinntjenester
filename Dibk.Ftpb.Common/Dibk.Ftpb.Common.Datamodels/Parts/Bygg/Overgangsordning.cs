﻿using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Overgangsordning
    {
        [XmlElement("overgangsbestemmelse")]
        public bool? Overgangsbestemmelse { get; set; }

        [XmlElement("datoForHovedsoeknad")]
        public DateTime? DatoForHovedsoeknad { get; set; }
    }
}