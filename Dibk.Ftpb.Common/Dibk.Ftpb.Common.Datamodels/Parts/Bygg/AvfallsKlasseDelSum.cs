﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class AvfallsklasseDelsum
    {
        [XmlElement("avfallsklasse")]
        public Kodeliste Avfallsklasse { get; set; }

        [XmlElement("delsum")]
        public string Delsum { get; set; }
    }
}