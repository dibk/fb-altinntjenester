#!/bin/bash

# List of valid project names
VALID_NAMES=("Dibk.Ftpb.Common" "Dibk.Ftpb.Common.Datamodels" "Dibk.Ftpb.Common.AltinnApps")

# Function to check if a project name is valid
is_valid_name() {
  local name="$1"
  for valid_name in "${VALID_NAMES[@]}"; do
    if [ "$name" == "$valid_name" ]; then
      return 0
    fi
  done
  return 1
}

# Check if a tag value is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <tag-value>"
  exit 1
fi

# Get the input parameter
TAG_VALUE="$1"

# Split the tag value into project name and version, supports pre-release versions
# Example:              Dibk.Ftpb.Common-1.0.0
# Example pre-release:  Dibk.Ftpb.Common-1.0.0-beta
PROJECT_NAME="${TAG_VALUE%%-*}"  # Everything before the first dash
VERSION="${TAG_VALUE#*-}"       # Everything after the first dash

# Check if the project name is valid
if is_valid_name "$PROJECT_NAME"; then
  echo "Project Name: $PROJECT_NAME"
  export PROJECT_NAME=$PROJECT_NAME
  echo "Version: $VERSION"
  export NUPKG_VERSION=$VERSION
else
  echo "Error: '$PROJECT_NAME' is not a valid project name."
  exit 1
fi
