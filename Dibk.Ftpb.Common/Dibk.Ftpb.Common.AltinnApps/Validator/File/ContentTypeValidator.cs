using Altinn.App.Core.Features.FileAnalysis;
using Altinn.App.Core.Features.Validation;
using Altinn.App.Core.Models.Validation;
using Altinn.Platform.Storage.Interface.Models;

namespace Dibk.Ftpb.Common.AltinnApps.Validator.File
{
    public class ContentTypeValidator : IFileValidator
    {
        /// <param name="dataType"></param>
        /// <param name="fileAnalysisResults"></param>
        /// <returns></returns>
        public async Task<(bool Success, IEnumerable<ValidationIssue> Errors)> Validate(DataType dataType,
            IEnumerable<FileAnalysisResult> fileAnalysisResults)
        {
            var errors = fileAnalysisResults
                .Where(result => result.Metadata.ContainsKey("ContentTypeAnalyser"))
                .Select(result =>
                {
                    var message = result.Metadata["ContentTypeAnalyser"];
                    return new ValidationIssue
                    {
                        Source = dataType.Id,
                        Code = "FileExtensionInvalid",
                        Severity = ValidationIssueSeverity.Error,
                        Description = $"Innholdet stemmer ikke overens med angitt fil-extension og content-type: {message}"
                    };
                })
                .ToList();

            return (!errors.Any(), errors);
        }

        /// <summary>
        /// Id for filvalidatoren, settes på den aktuelle datatypen i applicationMetadata
        /// </summary>
        public string Id => "contentTypeValidator";
    }
}