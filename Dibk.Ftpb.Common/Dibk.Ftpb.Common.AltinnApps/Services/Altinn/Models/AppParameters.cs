﻿namespace Dibk.Ftpb.Common.AltinnApps.Services.Altinn.Models
{
    public class AppParameters
    {
        public string App { get; set; }
        public string Org { get; set; }
        public int InstanceOwnerPartyId { get; set; }
        public Guid InstanceGuid { get; set; }
    }
}