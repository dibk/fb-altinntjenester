﻿using System.Xml.Linq;
using DiBK.RuleValidator.Extensions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Dibk.Ftpb.Common.AltinnApps.HttpClients.Codelist
{
    public class CodelistHttpClient : ICodelistHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly IMemoryCache _memoryCache;
        private readonly CodelistSettings _settings;
        private readonly ILogger<CodelistHttpClient> _logger;

        public CodelistHttpClient(
            HttpClient httpClient,
            IMemoryCache memoryCache,
            IOptions<CodelistSettings> options,
            ILogger<CodelistHttpClient> logger)
        {
            _httpClient = httpClient;
            _memoryCache = memoryCache;
            _settings = options.Value;
            _logger = logger;
        }

        public async Task<List<CodelistItem>> GetArealformålAsync()
        {
            return await GetCodelistAsync(_settings.Arealformål);
        }

        public async Task<List<CodelistItem>> GetFeltnavnAsync()
        {
            return await GetCodelistAsync(_settings.Feltnavn);
        }

        public async Task<List<CodelistItem>> GetHensynskategoriAsync()
        {
            return await GetCodelistAsync(_settings.Hensynskategori);
        }

        public async Task<List<CodelistItem>> GetMålemetodeAsync()
        {
            return await GetCodelistAsync(_settings.Målemetode);
        }

        private async Task<List<CodelistItem>> GetCodelistAsync(Uri uri)
        {
            return await _memoryCache.GetOrCreateAsync(uri, async cacheEntry =>
            {
                cacheEntry.SlidingExpiration = TimeSpan.FromDays(_settings.CacheDurationDays);

                try
                {
                    using var response = await _httpClient.GetAsync(uri);
                    response.EnsureSuccessStatusCode();
                    using var stream = await response.Content.ReadAsStreamAsync();

                    return await CreateCodelistAsync(stream);
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, $"Kunne ikke laste ned data fra {uri.AbsoluteUri}.");
                    return new();
                }
            });
        }

        private static async Task<List<CodelistItem>> CreateCodelistAsync(Stream stream)
        {
            var document = await XDocument.LoadAsync(stream, LoadOptions.None, default);

            return document
                .GetElements("//*:dictionaryEntry/*:Definition")
                .Select(element => new CodelistItem(element.GetValue("*:name"), element.GetValue("*:identifier"), element.GetValue("*:description")))
                .OrderBy(feltnavn => feltnavn.Value)
                .ToList();
        }
    }
}