﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Arbeidsplasser
    {
        [XmlElement("framtidige")]
        public bool? Framtidige { get; set; }

        [XmlElement("faste")]
        public bool? Faste { get; set; }

        [XmlElement("midlertidige")]
        public bool? Midlertidige { get; set; }

        [XmlElement("antallAnsatte")]
        public string AntallAnsatte { get; set; }

        [XmlElement("eksisterende")]
        public bool? Eksisterende { get; set; }

        [XmlElement("utleieBygg")]
        public bool? UtleieBygg { get; set; }

        [XmlElement("antallVirksomheter")]
        public string AntallVirksomheter { get; set; }

        [XmlElement("beskrivelse")]
        public string Beskrivelse { get; set; }

        [XmlElement("veiledning")]
        public bool? Veiledning { get; set; }
    }
}