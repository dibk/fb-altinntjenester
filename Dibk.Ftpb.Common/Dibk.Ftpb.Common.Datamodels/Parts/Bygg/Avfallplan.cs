﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Avfallplan
    {
        [XmlElement("avfallsklasse")]
        public Kodeliste Avfallklasse { get; set; }

        [XmlElement("fraksjon")]
        public Kodeliste Avfallfraksjon { get; set; }

        [XmlElement("mengdeFraksjon")]
        public string MengdeFraksjon { get; set; }
    }
}