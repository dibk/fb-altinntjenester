﻿using System;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "http://skjema.kxml.no/dibk/midlertidigbrukstillatelse/v3.0")]
    [XmlRoot("midlertidigbrukstillatelse", Namespace = "http://skjema.kxml.no/dibk/midlertidigbrukstillatelse/v3.0")]
    public class MidlertidigbrukstillatelseV3 : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("metadata")]
        public Metadata Metadata { get; set; }

        [XmlElement("generelleVilkaar")]
        public GenerelleVilkaar GenerelleVilkaar { get; set; }

        [XmlElement("soeknadGjelder")]
        public SoeknadGjelder SoeknadGjelder { get; set; }

        [XmlArray("delsoeknader")]
        [XmlArrayItem("delsoeknad")]
        public Delsoeknad[] Delsoeknader { get; set; }

        [XmlArray("utfallBesvarelse")]
        [XmlArrayItem("utfallSvar")]
        public UtfallSvarV3[] UtfallBesvarelse { get; set; }

        [XmlElement("tiltakshaver")]
        public Aktoer Tiltakshaver { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("datoFerdigattest")]
        public DateTime? DatoFerdigattest { get; set; }

        [XmlElement("gjenstaaendeArbeider")]
        public GjenstaaendeArbeider GjenstaaendeArbeider { get; set; }

        [XmlElement("sikkerhetsnivaa")]
        public Sikkerhetsnivaa Sikkerhetsnivaa { get; set; }

        [XmlElement("ansvarForByggesaken")]
        public Kodeliste AnsvarForByggesaken { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 10004<br />
        /// <b>DataFormatVersion:</b> 3.0<br />
        /// <b>DataFormatProvider:</b> DIBK
        /// </summary>
        public MidlertidigbrukstillatelseV3()
        {
            DataFormatProvider = "DIBK";
            DataFormatId = "10004";
            DataFormatVersion = "3.0";
        }
    }
}