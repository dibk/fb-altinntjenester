﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class UtfallSvar
    {
        [XmlElement("utfallId")]
        public string UtfallId { get; set; }

        [XmlElement("utfallType")]
        public Kodeliste Utfalltype { get; set; }

        [XmlElement("utloestFraSjekkpunkt")]
        public Sjekkpunkt UtloestFraSjekkpunkt { get; set; }

        [XmlElement("tema")]
        public Kodeliste Tema { get; set; }

        [XmlElement("tittel")]
        public string Tittel { get; set; }

        [XmlElement("beskrivelse")]
        public string Beskrivelse { get; set; }

        [XmlElement("erUtfallBesvaresSenere")]
        public bool? ErUtfallBesvaresSenere { get; set; }

        [XmlElement("erUtfallBesvart")]
        public bool? ErUtfallBesvart { get; set; }

        [XmlElement("kommentar")]
        public string Kommentar { get; set; }

        [XmlArray("vedleggsliste")]
        [XmlArrayItem("vedlegg")]
        public Vedlegg[] Vedleggsliste { get; set; }
    }
}