using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Common
{
    /// <summary>
    ///
    /// </summary>
    public static class DataTypeMapper
    {
        /// <summary>
        /// Helper to retrieve DataTypeConfig on a dataType
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Thrown if dataType is null or empty</exception>
        /// <exception cref="KeyNotFoundException">Thrown if the dataType doesn't exist in the list. Make sure the casing is correct.</exception>
        public static DataTypeConfig Get(string dataType)
        {
            if (string.IsNullOrEmpty(dataType))
                throw new ArgumentNullException("dataType");

            var retVal = Attachments.SingleOrDefault(s => s.DataType.Equals(dataType));

            if (retVal == default)
                throw new KeyNotFoundException($"Unable to find AttachmentInformation for {dataType}");

            return retVal;
        }

        /// <summary>
        /// Helper to retrieve DataTypeConfig using displayName
        /// A few dataTypes have the same displayName, so this will return a list of DataTypeConfigs
        /// </summary>
        /// <param name="displayName"></param>
        /// <returns>List of DataTypeConfigs</returns>
        /// <exception cref="ArgumentNullException">Thrown if displayName is null or empty</exception>
        /// <exception cref="KeyNotFoundException">Thrown if the dataType doesn't exist in the list. Make sure the casing is correct.</exception>
        public static List<DataTypeConfig> GetByDisplayName(string displayName, ConfigType limitedToConfig = ConfigType.Undefined)
        {
            if (string.IsNullOrEmpty(displayName))
                throw new ArgumentNullException("displayName");

            List<DataTypeConfig> retVal = new List<DataTypeConfig>();

            retVal = Attachments.Where(s => s.DisplayName.Equals(displayName, StringComparison.OrdinalIgnoreCase)).ToList();

            if (retVal != default && retVal.Count > 1 && limitedToConfig != ConfigType.Undefined)
                retVal = retVal.Where(s => s.LimitedTo.Contains(limitedToConfig)).ToList();

            if (retVal == default)
                throw new KeyNotFoundException($"Unable to find AttachmentInformation for {displayName}");

            return retVal;
        }

        /// <summary>
        /// List of all supported dataTypes in FtPB
        /// </summary>
        public static List<DataTypeConfig> Attachments = new List<DataTypeConfig>()
        {
            new DataTypeConfig(DataTypes.Planbestemmelser, AttachmentDisplayName.Planbestemmelser, ArkivlettV1DocumentTypes.Bestemmelser, ArkivlettV2DocumentTypes.Bestemmelser),
            new DataTypeConfig(DataTypes.PlanbestemmelsePdf, AttachmentDisplayName.Planbestemmelser, ArkivlettV1DocumentTypes.Bestemmelser, ArkivlettV2DocumentTypes.Bestemmelser),
            new DataTypeConfig(DataTypes.PlanbestemmelseXml, AttachmentDisplayName.Planbestemmelser, ArkivlettV1DocumentTypes.Bestemmelser, ArkivlettV2DocumentTypes.Bestemmelser),
            new DataTypeConfig(DataTypes.Plankart, AttachmentDisplayName.Plankart, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.PlankartPdf, AttachmentDisplayName.Arealplankart, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.PlankartGml2d,AttachmentDisplayName.Arealplankart, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.PlankartGml3d,AttachmentDisplayName.Arealplankart, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.Planbeskrivelse, AttachmentDisplayName.Planbeskrivelse, ArkivlettV1DocumentTypes.Planbeskrivelse, ArkivlettV2DocumentTypes.Planbeskrivelse),
            new DataTypeConfig(DataTypes.Varslingsliste, AttachmentDisplayName.Varslingsliste, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Varslingsbrev, AttachmentDisplayName.Varselbrev, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Planvarsel, AttachmentDisplayName.Planvarsel, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.SamlefilMottatteUttalelser, AttachmentDisplayName.SamlefilMottatteUttalelser, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.KommentarTilUttalelser, AttachmentDisplayName.KommentarTilUttalelser, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.JuridiskIllustrasjon, AttachmentDisplayName.JuridiskIllustrasjon, ArkivlettV1DocumentTypes.JuridiskBindendeIllustrasjon, ArkivlettV2DocumentTypes.JuridiskBindendeIllustrasjon),
            new DataTypeConfig(DataTypes.Illustrasjon, AttachmentDisplayName.Illustrasjon, ArkivlettV1DocumentTypes.Illustrasjon, ArkivlettV2DocumentTypes.Illustrasjon),
            new DataTypeConfig(DataTypes.Konsekvensutredning, AttachmentDisplayName.Konsekvensutredning, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Konsekvensutredning),
            new DataTypeConfig(DataTypes.ROSAnalyse, AttachmentDisplayName.ROSAnalyse, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.RosAnalyse),
            new DataTypeConfig(DataTypes.Utredning, AttachmentDisplayName.Utredning, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Utredning),
            new DataTypeConfig(DataTypes.Rapport, AttachmentDisplayName.Rapport, ArkivlettV1DocumentTypes.Rapport, ArkivlettV2DocumentTypes.Rapport),
            new DataTypeConfig(DataTypes.Valideringsrapport, AttachmentDisplayName.Valideringsrapport, ArkivlettV1DocumentTypes.Kontroll, ArkivlettV2DocumentTypes.System),
            new DataTypeConfig(DataTypes.Annet, AttachmentDisplayName.Annet, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.Q),
            new DataTypeConfig(DataTypes.OversendelseReguleringsplanforslag, AttachmentDisplayName.OversendelseReguleringsplanforslag, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.ValideringsrapportSluttrapportBygningsavfall, AttachmentDisplayName.ValideringsrapportSluttrapportBygningsavfall, ArkivlettV1DocumentTypes.Kontroll, ArkivlettV2DocumentTypes.System),
            new DataTypeConfig(DataTypes.KartDetaljert, AttachmentDisplayName.KartDetaljert, ArkivlettV1DocumentTypes.Kart, ArkivlettV2DocumentTypes.Kart),
            new DataTypeConfig(DataTypes.Planinitiativ, AttachmentDisplayName.Planinitiativ, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.ReferatOppstartsmoete, AttachmentDisplayName.ReferatOppstartsmoete, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Planprogram, AttachmentDisplayName.Planprogram, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Planomraade, AttachmentDisplayName.Planomraade, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.PlanomraadePdf, AttachmentDisplayName.PlanomraadePdf, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.PlanomraadeSosi, AttachmentDisplayName.PlanomraadeSosi, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.Varselbrev, AttachmentDisplayName.Varselbrev, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.SvarNabovarselPlan, AttachmentDisplayName.SvarNabovarselPlan, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Hoeringsbrev, AttachmentDisplayName.Hoeringsbrev, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.PlankartGml, AttachmentDisplayName.PlankartGml, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.PlankartSosi, AttachmentDisplayName.PlankartSosi, ArkivlettV1DocumentTypes.Arealplankart, ArkivlettV2DocumentTypes.Arealplankart),
            new DataTypeConfig(DataTypes.Illustrasjoner, AttachmentDisplayName.Illustrasjon, ArkivlettV1DocumentTypes.Illustrasjon, ArkivlettV2DocumentTypes.Illustrasjon),
            new DataTypeConfig(DataTypes.Foelgebrev, AttachmentDisplayName.Folgebrev, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Kunngjoring, AttachmentDisplayName.Kunngjoring, ArkivlettV1DocumentTypes.Kunngjøring, ArkivlettV2DocumentTypes.Kunngjøring),
            new DataTypeConfig(DataTypes.Uttalelse, AttachmentDisplayName.Uttalelse, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Uttalelse),
            new DataTypeConfig(DataTypes.Saksframlegg, AttachmentDisplayName.Saksframlegg, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Saksframlegg),
            new DataTypeConfig(DataTypes.Tegnforklaringer, AttachmentDisplayName.Tegnforklaringer, ArkivlettV1DocumentTypes.Tegnforklaring, ArkivlettV2DocumentTypes.Tegnforklaring),
            new DataTypeConfig(DataTypes.GrunnforholdGeoteknikk, AttachmentDisplayName.GrunnforholdGeoteknikk, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Utredning),
            new DataTypeConfig(DataTypes.GrunnforholdForurensetGrunn, AttachmentDisplayName.GrunnforholdForurensetGrunn, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Utredning),
            new DataTypeConfig(DataTypes.Trafikkutredning, AttachmentDisplayName.Trafikkutredning, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Utredning),
            new DataTypeConfig(DataTypes.Stoyutredning, AttachmentDisplayName.Stoyutredning, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Utredning),
            new DataTypeConfig(DataTypes.ArkeologiskeUndersokelser, AttachmentDisplayName.ArkeologiskeUndersokelser, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Utredning),
            new DataTypeConfig(DataTypes.RammeplanVannAvlopOvervann, AttachmentDisplayName.RammeplanVannAvlopOvervann, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Utredning),
            new DataTypeConfig(DataTypes.BiologiskMangfold, AttachmentDisplayName.BiologiskMangfold, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Utredning),
            new DataTypeConfig(DataTypes.OffentligEttersyn, AttachmentDisplayName.OffentligEttersyn, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.UttalelseOffentligEttersyn, AttachmentDisplayName.UttalelseOffentligEttersyn, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Uttalelsebrev, AttachmentDisplayName.Uttalelsebrev, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Uttalelse),
            new DataTypeConfig(DataTypes.Planuttalelse, AttachmentDisplayName.Planuttalelse, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Uttalelse),
            new DataTypeConfig(DataTypes.UttalelseVedlegg, AttachmentDisplayName.UttalelseVedlegg, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Uttalelse),
            new DataTypeConfig(DataTypes.GjennomføringsplanHovedskjema, AttachmentDisplayName.Gjennomfoeringsplan, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll, null, ConfigType.MainForm),
            new DataTypeConfig(DataTypes.Nabovarsel, AttachmentDisplayName.Nabovarsel, ArkivlettV1DocumentTypes.Søk, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.SuppleringAvSøknad, AttachmentDisplayName.SuppleringAvSøknad, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.SøknadOmEndringAvTillatelse, AttachmentDisplayName.SøknadOmEndringAvTillatelse, ArkivlettV1DocumentTypes.SøkOmEndringAvTillatelse, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.SøknadOmFerdigAttest, AttachmentDisplayName.SøknadOmFerdigAttest, ArkivlettV1DocumentTypes.SøkOmFerdigattest, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.SøknadOmIgangsettingstillatelse, AttachmentDisplayName.SøknadOmIgangsettingstillatelse, ArkivlettV1DocumentTypes.SøkOmIgangsettingstillatelse, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.SøknadOmMidlertidigBrukstillatelse, AttachmentDisplayName.SøknadOmMidlertidigBrukstillatelse, ArkivlettV1DocumentTypes.SøkOmMidlertidigBrukstillatelse, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.SøknadOmRammetillatelse, AttachmentDisplayName.SøknadOmRammetillatelse, ArkivlettV1DocumentTypes.SøkOmRammetillatelse, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.SøknadOmTillatelseIEttrinn, AttachmentDisplayName.SøknadOmTillatelseIEttrinn, ArkivlettV1DocumentTypes.SøknadOmTillatelseIEttrinn, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.SøknadOmTillatelseTilTiltakUtenAnsvarsrett, AttachmentDisplayName.SøknadOmTillatelseTilTiltakUtenAnsvarsrett, ArkivlettV1DocumentTypes.SøkOmTiltakUtenAnsvarsrett, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.Vedleggsopplysninger, AttachmentDisplayName.Vedleggsopplysninger, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Matrikkelopplysninger, AttachmentDisplayName.Matrikkelopplysninger, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.GjenpartNabovarsel, AttachmentDisplayName.GjenpartNabovarsel, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.ArbeidstilsynetSamtykkeSoknad, AttachmentDisplayName.ArbeidstilsynetSamtykkeSoknad, ArkivlettV1DocumentTypes.Søk, ArkivlettV2DocumentTypes.Søknad),
            new DataTypeConfig(DataTypes.AndreRedegjoerelser, AttachmentDisplayName.AndreRedegjoerelser, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.F),
            new DataTypeConfig(DataTypes.OpphoerAnsvarsrett, AttachmentDisplayName.OpphoerAnsvarsrett, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll),
            new DataTypeConfig(DataTypes.PlanForUavhengigKontroll, AttachmentDisplayName.PlanForUavhengigKontroll, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll),
            new DataTypeConfig(DataTypes.SamtykkePlassering, AttachmentDisplayName.SamtykkePlassering, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.AnsvarsrettSelvbygger, AttachmentDisplayName.AnsvarsrettSelvbygger, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll, Vedleggskategori.G),
            new DataTypeConfig(DataTypes.ErklaeringSelvbygger, AttachmentDisplayName.ErklaeringSelvbygger, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll),
            new DataTypeConfig(DataTypes.SoknadOmAnsvarsrettTiltaksklasse1, AttachmentDisplayName.SoknadOmAnsvarsrettTiltaksklasse1, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll, Vedleggskategori.G),
            new DataTypeConfig(DataTypes.Utomhusplan, AttachmentDisplayName.Utomhusplan, ArkivlettV1DocumentTypes.Kart, ArkivlettV2DocumentTypes.Kart, Vedleggskategori.D),
            new DataTypeConfig(DataTypes.Dispensasjonssoeknad, AttachmentDisplayName.Dispensasjonssoeknad, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.RedegjoerelseUnntakTEK, AttachmentDisplayName.RedegjoerelseUnntakTEK, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.KvitteringNabovarsel, AttachmentDisplayName.KvitteringNabovarsel, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Nabomerknader, AttachmentDisplayName.Nabomerknader, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.NabovarselVedlegg, AttachmentDisplayName.NabovarselVedlegg, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.KommentarNabomerknader, AttachmentDisplayName.KommentarNabomerknader, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Situasjonsplan, AttachmentDisplayName.Situasjonsplan, ArkivlettV1DocumentTypes.Kart, ArkivlettV2DocumentTypes.Kart, Vedleggskategori.D),
            new DataTypeConfig(DataTypes.Avkjoerselsplan, AttachmentDisplayName.Avkjoerselsplan, ArkivlettV1DocumentTypes.Kart, ArkivlettV2DocumentTypes.Kart, Vedleggskategori.D),
            new DataTypeConfig(DataTypes.UnderlagUtnytting, AttachmentDisplayName.UnderlagUtnytting, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.TegningEksisterendeFasade, AttachmentDisplayName.TegningEksisterendeFasade, ArkivlettV1DocumentTypes.Tegning, ArkivlettV2DocumentTypes.Tegning, Vedleggskategori.E),
            new DataTypeConfig(DataTypes.TegningEksisterendePlan, AttachmentDisplayName.TegningEksisterendePlan, ArkivlettV1DocumentTypes.Tegning, ArkivlettV2DocumentTypes.Tegning, Vedleggskategori.E),
            new DataTypeConfig(DataTypes.TegningEksisterendeSnitt, AttachmentDisplayName.TegningEksisterendeSnitt, ArkivlettV1DocumentTypes.Tegning, ArkivlettV2DocumentTypes.Tegning, Vedleggskategori.E),
            new DataTypeConfig(DataTypes.TegningNyFasade, AttachmentDisplayName.TegningNyFasade, ArkivlettV1DocumentTypes.Tegning, ArkivlettV2DocumentTypes.Tegning, Vedleggskategori.E),
            new DataTypeConfig(DataTypes.TegningNyPlan, AttachmentDisplayName.TegningNyPlan, ArkivlettV1DocumentTypes.Tegning, ArkivlettV2DocumentTypes.Tegning, Vedleggskategori.E),
            new DataTypeConfig(DataTypes.TegningNyttSnitt, AttachmentDisplayName.TegningNyttSnitt, ArkivlettV1DocumentTypes.Tegning, ArkivlettV2DocumentTypes.Tegning, Vedleggskategori.E),
            new DataTypeConfig(DataTypes.ByggesaksBIM, AttachmentDisplayName.ByggesaksBIM, ArkivlettV1DocumentTypes.Tegning, ArkivlettV2DocumentTypes.Tegning),
            new DataTypeConfig(DataTypes.Situasjonskart, AttachmentDisplayName.Situasjonskart, ArkivlettV1DocumentTypes.Kart, ArkivlettV2DocumentTypes.Kart),
            new DataTypeConfig(DataTypes.Kart, AttachmentDisplayName.Kart, ArkivlettV1DocumentTypes.Kart, ArkivlettV2DocumentTypes.Kart, Vedleggskategori.F),
            new DataTypeConfig(DataTypes.Folgebrev, AttachmentDisplayName.Folgebrev, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.F),
            new DataTypeConfig(DataTypes.RedegjoerelseSkredOgFlom, AttachmentDisplayName.RedegjoerelseSkredOgFlom, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.F),
            new DataTypeConfig(DataTypes.RedegjoerelseAndreNaturMiljoeforhold, AttachmentDisplayName.RedegjoerelseAndreNaturMiljoeforhold, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.F),
            new DataTypeConfig(DataTypes.RedegjoerelseGrunnforhold, AttachmentDisplayName.RedegjoerelseGrunnforhold, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.F),
            new DataTypeConfig(DataTypes.RedegjoerelseEstetikk, AttachmentDisplayName.RedegjoerelseEstetikk, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.F),
            new DataTypeConfig(DataTypes.RedegjoerelseForurensetGrunn, AttachmentDisplayName.RedegjoerelseForurensetGrunn, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.F),
            new DataTypeConfig(DataTypes.Gjennomføringsplan, AttachmentDisplayName.Gjennomfoeringsplan, ArkivlettV1DocumentTypes.Gjennomføringsplan, ArkivlettV2DocumentTypes.AnsvarOgKontroll, Vedleggskategori.G, ConfigType.SubForm, ConfigType.Attachment),
            new DataTypeConfig(DataTypes.GjennomføringsplanPdf, AttachmentDisplayName.Gjennomfoeringsplan, ArkivlettV1DocumentTypes.Gjennomføringsplan, ArkivlettV2DocumentTypes.AnsvarOgKontroll, Vedleggskategori.G),
            new DataTypeConfig(DataTypes.ErklaeringAnsvarsrett, AttachmentDisplayName.ErklaeringAnsvarsrett, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll, Vedleggskategori.G),
            new DataTypeConfig(DataTypes.Kontrollerklæring, AttachmentDisplayName.Kontrollerklæring, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll),
            new DataTypeConfig(DataTypes.Samsvarserklæring, AttachmentDisplayName.Samsvarserklæring, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll),
            new DataTypeConfig(DataTypes.BoligspesifikasjonMatrikkel, AttachmentDisplayName.BoligspesifikasjonMatrikkel, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.H),
            new DataTypeConfig(DataTypes.UttalelseVedtakAnnenOffentligMyndighet, AttachmentDisplayName.UttalelseVedtakAnnenOffentligMyndighet, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.I),
            new DataTypeConfig(DataTypes.UttalelseKulturminnemyndighet, AttachmentDisplayName.UttalelseKulturminnemyndighet, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.SamtykkeArbeidstilsynet, AttachmentDisplayName.SamtykkeArbeidstilsynet, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.I),
            new DataTypeConfig(DataTypes.AvkjoeringstillatelseVegmyndighet, AttachmentDisplayName.AvkjoeringstillatelseVegmyndighet, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.RekvisisjonAvOppmaalingsforretning, AttachmentDisplayName.RekvisisjonAvOppmaalingsforretning, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.AvklaringVA, AttachmentDisplayName.AvklaringVA, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.Q),
            new DataTypeConfig(DataTypes.AvklaringHoyspent, AttachmentDisplayName.AvklaringHoyspent, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse, Vedleggskategori.Q),            
            new DataTypeConfig(DataTypes.SluttrapportForBygningsavfall, AttachmentDisplayName.SluttrapportForBygningsavfall, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Miljoesaneringsbeskrivelse, AttachmentDisplayName.Miljoesaneringsbeskrivelse, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Foto, AttachmentDisplayName.Foto, ArkivlettV1DocumentTypes.Foto, ArkivlettV2DocumentTypes.Foto),
            new DataTypeConfig(DataTypes.KvitteringForLevertAvfall, AttachmentDisplayName.KvitteringForLevertAvfall, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Organisasjonsplan, AttachmentDisplayName.Organisasjonsplan, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll),
            new DataTypeConfig(DataTypes.Revisjonserklaering, AttachmentDisplayName.Revisjonserklaering, ArkivlettV1DocumentTypes.AnsvarOgKontroll, ArkivlettV2DocumentTypes.AnsvarOgKontroll),
            new DataTypeConfig(DataTypes.TiltakshaverSignatur, AttachmentDisplayName.TiltakshaverSignatur, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse), // Annet? - i følge alfa eByggesakTypes
            new DataTypeConfig(DataTypes.RapportFraMiljoekartlegging, AttachmentDisplayName.RapportFraMiljoekartlegging, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.SoknadUnntasOffentlighet, AttachmentDisplayName.SoknadUnntasOffentlighet, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.DekryptertFoedselsnummerTiltakshaver, AttachmentDisplayName.DekryptertFoedselsnummerTiltakshaver, ArkivlettV1DocumentTypes.Kontroll, ArkivlettV2DocumentTypes.System),
            new DataTypeConfig(DataTypes.OpplysningerGittINabovarselDekrypteringsNoekkel, AttachmentDisplayName.OpplysningerGittINabovarselDekrypteringsNoekkel, ArkivlettV1DocumentTypes.Kontroll, ArkivlettV2DocumentTypes.System),
            new DataTypeConfig(DataTypes.Soknad, AttachmentDisplayName.Soknad, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Arbeidsmiljoutvalg, AttachmentDisplayName.Arbeidsmiljoutvalg, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.ArbeidstakersRepresentant, AttachmentDisplayName.ArbeidstakersRepresentant, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Bedriftshelsetjeneste, AttachmentDisplayName.Bedriftshelsetjeneste, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Verneombud, AttachmentDisplayName.Verneombud, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.LeietakerArbeidsgiver, AttachmentDisplayName.LeietakerArbeidsgiver, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.BeskrivelseTypeArbeidProsess, AttachmentDisplayName.BeskrivelseTypeArbeidProsess, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Arbeidsmiljofaktorer, AttachmentDisplayName.Arbeidsmiljofaktorer, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.ArbeidsgiversMedvirkning, AttachmentDisplayName.ArbeidsgiversMedvirkning, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.ForUtleiebygg, AttachmentDisplayName.ForUtleiebygg, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.BistandFraBHT, AttachmentDisplayName.BistandFraBHT, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.Ventilasjon, AttachmentDisplayName.Ventilasjon, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
            new DataTypeConfig(DataTypes.AnsattesMedvirkning, AttachmentDisplayName.AnsattesMedvirkning, ArkivlettV1DocumentTypes.Korrespondanse, ArkivlettV2DocumentTypes.Korrespondanse),
        };
    }
}