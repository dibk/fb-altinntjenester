﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class FakturamottakerBase
    {
        [XmlElement("organisasjonsnummer")]
        public string Organisasjonsnummer { get; set; }

        [XmlElement("fakturareferanse")]
        public string Fakturareferanse { get; set; }
    }
}
