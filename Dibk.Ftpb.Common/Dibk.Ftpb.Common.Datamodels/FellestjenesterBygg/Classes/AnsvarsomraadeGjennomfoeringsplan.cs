﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    public class AnsvarsomraadeGjennomfoeringsplan
    {
        [XmlElement("funksjon")]
        public Kodeliste Funksjon { get; set; }

        [XmlElement("ansvarsomraade")]
        public string Ansvarsomraade { get; set; }

        [XmlElement("tiltaksklasse")]
        public Kodeliste Tiltaksklasse { get; set; }

        [XmlElement("foretak")]
        public Foretak Foretak { get; set; }

        [XmlElement("samsvarKontrollPlanlagtVedRammetillatelse")]
        public bool? SamsvarKontrollPlanlagtVedRammetillatelse { get; set; }

        [XmlElement("samsvarKontrollPlanlagtVedIgangsettingstillatelse")]
        public bool? SamsvarKontrollPlanlagtVedIgangsettingstillatelse { get; set; }

        [XmlElement("samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse")]
        public bool? SamsvarKontrollPlanlagtVedMidlertidigBrukstillatelse { get; set; }

        [XmlElement("samsvarKontrollPlanlagtVedFerdigattest")]
        public bool? SamsvarKontrollPlanlagtVedFerdigattest { get; set; }

        [XmlElement("samsvarKontrollForeliggerVedRammetillatelse")]
        public DateTime? SamsvarKontrollForeliggerVedRammetillatelse { get; set; }

        [XmlElement("samsvarKontrollForeliggerVedIgangsettingstillatelse")]
        public DateTime? SamsvarKontrollForeliggerVedIgangsettingstillatelse { get; set; }

        [XmlElement("samsvarKontrollForeliggerVedMidlertidigBrukstillatelse")]
        public DateTime? SamsvarKontrollForeliggerVedMidlertidigBrukstillatelse { get; set; }

        [XmlElement("samsvarKontrollForeliggerVedFerdigattest")]
        public DateTime? SamsvarKontrollForeliggerVedFerdigattest { get; set; }

        [XmlElement("ansvarsomraadetAvsluttet")]
        public bool? AnsvarsomraadetAvsluttet { get; set; }

        [XmlElement("erklaeringArkivreferanse")]
        public string ErklaeringArkivreferanse { get; set; }

        [XmlElement("erklaeringSignert")]
        public bool? ErklaeringSignert { get; set; }

        [XmlElement("sluttbrukersystemReferanse")]
        public string SluttbrukersystemReferanse { get; set; }

        [XmlElement("sluttbrukersystemStatus")]
        public string SluttbrukersystemStatus { get; set; }

        [XmlElement("ansvarsomraadeSistEndret")]
        public DateTime? AnsvarsomraadeSistEndret { get; set; }

        [XmlElement("filnavnVedlegg")]
        public string FilnavnVedlegg { get; set; }

        [XmlElement("ansvarsomraadeStatus")]
        public Kodeliste AnsvarsomraadeStatus { get; set; }
    }
}
