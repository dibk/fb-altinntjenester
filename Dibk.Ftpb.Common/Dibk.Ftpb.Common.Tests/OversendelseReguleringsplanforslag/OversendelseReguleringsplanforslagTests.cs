using Dibk.Ftpb.Common.Datamodels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace Dibk.Ftpb.Common.Tests;

[TestClass]
public class OversendelseReguleringsplanforslagTests
{
    [TestMethod]
    public void DeserializeCurrentVersionToClassWithUnspecifiedVersion_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/oversendelseReguleringsplanforslag.xml");

        var regfo = XmlSerializerUtil.DeserializeFromString<OversendelseReguleringsplanforslag>(xmlContent, false);

        Assert.IsNotNull(regfo);
    }

    [TestMethod]
    public void Regfo_DeserializeXml_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/oversendelseReguleringsplanforslag.xml");

        var regfo = XmlSerializerUtil.DeserializeFromString<OversendelseReguleringsplanforslag>(xmlContent, false);

        Assert.IsNotNull(regfo);

        //DataFormat-attributter
        Assert.AreEqual("DIBK", regfo.DataFormatProvider);
        Assert.AreEqual("11001", regfo.DataFormatId);
        Assert.AreEqual("1", regfo.DataFormatVersion);

        //Planinformasjon
        Assert.IsNotNull(regfo.Planforslag);
        Assert.AreEqual("Eksempel Plannavn", regfo.Planforslag.Plannavn);
        Assert.AreEqual("Plan123", regfo.Planforslag.ArealplanId);
        Assert.IsNotNull(regfo.Planforslag.Plantype);
        Assert.AreEqual("01", regfo.Planforslag.Plantype.Kodeverdi);
        Assert.AreEqual("Områdeplan", regfo.Planforslag.Plantype.Kodebeskrivelse);
        Assert.IsNotNull(regfo.Planforslag.KommunensSaksnummer);
        Assert.AreEqual(2023, regfo.Planforslag.KommunensSaksnummer.Saksaar);
        Assert.AreEqual(789, regfo.Planforslag.KommunensSaksnummer.Sakssekvensnummer);

        //Lovreferanse
        Assert.IsNotNull(regfo.Lovreferanse);
        Assert.AreEqual("\u00a712-3", regfo.Lovreferanse.Kodeverdi);
        Assert.AreEqual("Pbl \u00a712-3", regfo.Lovreferanse.Kodebeskrivelse);

        //SendesTilKommune
        Assert.AreEqual("5678", regfo.SendesTilKommune.Nummer);
        Assert.AreEqual("Eksempelsund", regfo.SendesTilKommune.Navn);

        //Versjon
        Assert.IsNotNull(regfo.Versjon);
        Assert.AreEqual(new DateTime(2023, 10, 01, 0, 0, 0), regfo.Versjon.Versjonsdato);
        Assert.AreEqual("1.0", regfo.Versjon.Versjonsnummer);
        Assert.AreEqual(true, regfo.Versjon.ErFoersteVersjon);
        Assert.AreEqual(false, regfo.Versjon.ErAlternativ);
        Assert.AreEqual("Ref-ABC-123", regfo.Versjon.AlternativReferanse);

        //Vedlegg
        Assert.IsNotNull(regfo.Vedlegg);
        Assert.AreEqual("V01", regfo.Vedlegg.Kodeverdi);
        Assert.AreEqual("Situasjonskart", regfo.Vedlegg.Kodebeskrivelse);

        //Metadata
        Assert.IsNotNull(regfo.Metadata);
        Assert.AreEqual("FT12345678", regfo.Metadata.FtbId);
        Assert.AreEqual("H-12345678910", regfo.Metadata.Hovedinnsendingsnummer);
        Assert.AreEqual(true, regfo.Metadata.KlartForSigneringFraSluttbrukersystem);
        Assert.AreEqual("PlanSystem X", regfo.Metadata.FraSluttbrukersystem);
        Assert.AreEqual("Prosjektet", regfo.Metadata.Prosjektnavn);
        Assert.AreEqual("P-123", regfo.Metadata.Prosjektnr);

        //Forslagsstiller
        var forslagsstiller = regfo.Forslagsstiller;
        Assert.IsNotNull(forslagsstiller);
        Assert.IsNotNull(forslagsstiller.Partstype);
        Assert.AreEqual("FS", forslagsstiller.Partstype.Kodeverdi);
        Assert.AreEqual("Forslagsstiller", forslagsstiller.Partstype.Kodebeskrivelse);
        Assert.AreEqual("01019012345", forslagsstiller.Foedselsnummer);
        Assert.AreEqual("987654321", forslagsstiller.Organisasjonsnummer);
        Assert.AreEqual("Ola Nordmann", forslagsstiller.Navn);
        Assert.IsNotNull(forslagsstiller.Adresse);
        Assert.AreEqual("Gateveien 1", forslagsstiller.Adresse.Adresselinje1);
        Assert.AreEqual("Leil. 2", forslagsstiller.Adresse.Adresselinje2);
        Assert.AreEqual("", forslagsstiller.Adresse.Adresselinje3);
        Assert.AreEqual("0123", forslagsstiller.Adresse.Postnr);
        Assert.AreEqual("Oslo", forslagsstiller.Adresse.Poststed);
        Assert.AreEqual("NO", forslagsstiller.Adresse.Landkode);
        Assert.AreEqual("90909090", forslagsstiller.Telefon);
        Assert.AreEqual("ola@nordmann.no", forslagsstiller.Epost);

        //Plankonsulent
        Assert.IsNotNull(regfo.Plankonsulent);
        Assert.IsNotNull(regfo.Plankonsulent.Partstype);
        Assert.AreEqual("PK", regfo.Plankonsulent.Partstype.Kodeverdi);
        Assert.AreEqual("Plankonsulent", regfo.Plankonsulent.Partstype.Kodebeskrivelse);
        Assert.IsNull(regfo.Plankonsulent.Foedselsnummer);
        Assert.AreEqual("123456789", regfo.Plankonsulent.Organisasjonsnummer);
        Assert.AreEqual("Kari Hansen AS", regfo.Plankonsulent.Navn);
        Assert.IsNotNull(regfo.Plankonsulent.Adresse);
        Assert.AreEqual("Kontorveien 5", regfo.Plankonsulent.Adresse.Adresselinje1);
        Assert.AreEqual("0456", regfo.Plankonsulent.Adresse.Postnr);
        Assert.AreEqual("Bergen", regfo.Plankonsulent.Adresse.Poststed);
        Assert.AreEqual("NO", regfo.Plankonsulent.Adresse.Landkode);
        Assert.AreEqual("87654321", regfo.Plankonsulent.Telefon);
        Assert.AreEqual("post@karihansen.no", regfo.Plankonsulent.Epost);

        //Fakturamottaker
        Assert.AreEqual("123456789", regfo.Fakturamottaker.Organisasjonsnummer);
        Assert.AreEqual("Faktura789", regfo.Fakturamottaker.Fakturareferanse);
    }
}
