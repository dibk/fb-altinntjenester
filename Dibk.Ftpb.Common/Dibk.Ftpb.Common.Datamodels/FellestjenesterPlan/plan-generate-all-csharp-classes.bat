REM
REM cd xsd
echo "offentligEttersyn.xsd"
xsd.exe .\Xsd\offentligEttersyn.xsd /c /n:no.kxml.skjema.dibk.offentligEttersyn
move /y offentligEttersyn.cs .\Classes\OffentligEttersyn.cs
REM
echo "uttalelseOffentligEttersyn.xsd"
xsd.exe .\Xsd\uttalelseOffentligEttersyn.xsd /c /n:no.kxml.skjema.dibk.uttalelseOffentligEttersyn
move /y uttalelseOffentligEttersyn.cs .\Classes\UttalelseOffentligEttersyn.cs
REM
echo "oversendelseReguleringsplanforslag.xsd"
xsd.exe .\Xsd\oversendelseReguleringsplanforslag.xsd /c /n:no.kxml.skjema.dibk.oversendelseReguleringsplanforslag
move /y oversendelseReguleringsplanforslag.cs .\Classes\OversendelseReguleringsplanforslag.cs
REM
echo "planbestemmelser.xsd"
xsd.exe .\Xsd\planbestemmelser.xsd /c /n:no.kxml.skjema.dibk.planbestemmelser
move /y planbestemmelser.cs .\Classes\Planbestemmelser.cs
REM
echo "planuttalelseHoeringsmyndigheter.xsd"
xsd.exe .\Xsd\planuttalelseHoeringsmyndigheter.xsd /c /n:no.kxml.skjema.dibk.planuttalelseHoeringsmyndigheter
move /y planuttalelseHoeringsmyndigheter.cs .\Classes\PlanuttalelseHoeringsmyndigheter.cs
REM
echo "planvarselHoeringsmyndigheter.xsd"
xsd.exe .\Xsd\planvarselHoeringsmyndigheter.xsd /c /n:no.kxml.skjema.dibk.planvarselHoeringsmyndigheter
move /y planvarselHoeringsmyndigheter.cs .\Classes\PlanvarselHoeringsmyndigheter.cs
REM
echo "planvarsel.xsd"
xsd.exe .\Xsd\planvarsel.xsd /c /n:no.kxml.skjema.dibk.planvarsel
move /y planvarsel.cs .\Classes\Planvarsel.cs
REM
echo "planuttalelse.xsd"
xsd.exe .\Xsd\planuttalelse.xsd /c /n:no.kxml.skjema.dibk.planuttalelse
move /y planuttalelse.cs .\Classes\Planuttalelse.cs
cd ..
pause