﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Avfallsortering
    {
        [XmlElement("mengdeSortert")]
        public string MengdeSortert { get; set; }

        [XmlElement("mengdeTotalt")]
        public string MengdeTotalt { get; set; }

        [XmlElement("sorteringsgrad")]
        public string Sorteringsgrad { get; set; }

        [XmlElement("mengdeAvfallPerAreal")]
        public string MengdeAvfallPerAreal { get; set; }

        //[XmlElement("disponeringsmaateDelSum")]
        //public DisponeringsmaateDelSum DisponeringsmaateDelSum { get; set; }
    }
}