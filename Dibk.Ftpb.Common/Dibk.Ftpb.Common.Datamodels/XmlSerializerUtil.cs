﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    /// <summary>
    /// Stringriter that uses UTF8 encoding
    /// </summary>
    public sealed class Utf8StringWriter : StringWriter
    {
        /// <inheritdoc/>
        public override Encoding Encoding
        { get { return Encoding.UTF8; } }
    }

    /// <summary>
    /// A utility class for XML serialization and deserialization. It provides methods for serializing an object to an XML string and deserializing an XML string back into an object.
    /// It also includes methods for removing namespaces from an XML string, which can make it easier to work with the XML and enable deserialization of the XML string into an object.
    /// </summary>
    public class XmlSerializerUtil
    {
        /// <summary>
        /// Deserializes the provided XML string into an object of type T. <br/>
        /// The XML string is first stripped of any namespaces to facilitate the deserialization process. <br/>
        /// Namespace stripping may be turned off by setting the <paramref name="removeNamespaces"/> parameter to <see langword="false"/>.
        /// </summary>
        /// <typeparam name="T">The type of object to be deserialized from the XML string.</typeparam>
        /// <param name="objectData">The XML string to deserialize.</param>
        /// <param name="removeNamespaces">Flag for stripping namespaces. Default value: <see langword="true"/></param>
        /// <returns>The deserialized object of type T.</returns>
        public static T DeserializeFromString<T>(string objectData, bool removeNamespaces = true)
        {
            return (T)DeserializeFromString(objectData, typeof(T), removeNamespaces);
        }

        private static object DeserializeFromString(string objectData, Type type, bool removeNamespaces)
        {
            object result;

            TextReader reader = null;
            try
            {
                var xmlString = removeNamespaces ? RemoveAllNamespaces(objectData) : objectData;
                using (var stringReader = new StringReader(xmlString))
                {
                    var serializer = new XmlSerializer(type);

                    result = serializer.Deserialize(stringReader);
                }
            }
            finally
            {
                reader?.Close();
            }

            return result;
        }

        /// <summary>
        /// Serializes the provided object to an XML string
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public static string Serialize(object form)
        {
            var serializer = new XmlSerializer(form.GetType());
            var stringWriter = new Utf8StringWriter();
            serializer.Serialize(stringWriter, form);
            return stringWriter.ToString();
        }

        //** Remove name spaceses
        private static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        //Core recursion function
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }

            var complexElement = new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));

            if (xmlDocument.Attributes().Any(a => a.Name == "dataFormatProvider"))
            {
                complexElement.Add(new XAttribute("dataFormatProvider", xmlDocument.Attribute("dataFormatProvider").Value));
                complexElement.Add(new XAttribute("dataFormatId", xmlDocument.Attribute("dataFormatId").Value));
                complexElement.Add(new XAttribute("dataFormatVersion", xmlDocument.Attribute("dataFormatVersion").Value));
            }
            
            return complexElement;
        }

        /// <summary>
        /// Removes all namespaces from the provided XML string
        /// This makes it easier to work with the XML and possible to deserialize the XML string into an object
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static string RemoveNamespaces(string xmlString)
        {
            try
            {
                XDocument document = XDocument.Parse(RemoveAllNamespaces(xmlString));
                return document.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}