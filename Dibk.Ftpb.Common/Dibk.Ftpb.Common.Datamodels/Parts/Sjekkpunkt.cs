﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class Sjekkpunkt
    {
        [XmlElement("sjekkpunktId")]
        public string SjekkpunktId { get; set; }

        [XmlElement("sjekkpunktEier")]
        public string SjekkpunktEier { get; set; }
    }
}
