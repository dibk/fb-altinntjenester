﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "http://skjema.kxml.no/dibk/ferdigattest/3.0")]
    [XmlRoot("ferdigattest", Namespace = "http://skjema.kxml.no/dibk/ferdigattest/3.0")]
    public class FerdigattestV3 : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("metadata")]
        public Metadata Metadata { get; set; }

        [XmlElement("generelleVilkaar")]
        public GenerelleVilkaar GenerelleVilkaar { get; set; }

        [XmlElement("soeknadGjelder")]
        public SoeknadGjelderBase SoeknadGjelder { get; set; }

        [XmlArray("utfallBesvarelse")]
        [XmlArrayItem("utfallSvar")]
        public UtfallSvarV3[] UtfallBesvarelse { get; set; }

        [XmlElement("kravFerdigattest")]
        public KravFerdigattest KravFerdigattest { get; set; }

        [XmlElement("foretattIkkeSoeknadspliktigeJusteringer")]
        public bool? ErForetattIkkeSoeknadspliktigeJusteringer { get; set; }

        [XmlElement("tilstrekkeligDokumentasjonOverlevertEier")]
        public bool? ErTilstrekkeligDokumentasjonOverlevertEier { get; set; }

        [XmlElement("varmesystem")]
        public Varmesystem Varmesystem { get; set; }

        [XmlElement("tiltakshaver")]
        public Aktoer Tiltakshaver { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("ansvarForByggesaken")]
        public Kodeliste AnsvarForByggesaken { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 10005<br />
        /// <b>DataFormatVersion:</b> 3.0<br />
        /// <b>DataFormatProvider:</b> DIBK
        /// </summary>
        public FerdigattestV3()
        {
            DataFormatProvider = "DIBK";
            DataFormatId = "10005";
            DataFormatVersion = "3.0";
        }
    }
}