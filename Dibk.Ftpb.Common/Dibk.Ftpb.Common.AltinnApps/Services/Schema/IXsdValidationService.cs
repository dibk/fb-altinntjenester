﻿using Dibk.Ftpb.Common.AltinnApps.Constants;
using Dibk.Ftpb.Common.AltinnApps.Services.Schema.Models;
using DiBK.RuleValidator.Extensions;
using Microsoft.AspNetCore.Http;

namespace Dibk.Ftpb.Common.AltinnApps.Services.Schema
{
    public interface IXsdValidationService
    {
        List<SchemaRule> Validate(DisposableList<InputData> inputData);
        SchemaRule Validate(IFormFile xmlFile, DataType dataType);
    }
}