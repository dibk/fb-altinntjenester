﻿using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Sikkerhetsnivaa
    {
        [XmlElement("harTilstrekkeligSikkerhet")]
        public bool? HarTilstrekkeligSikkerhet { get; set; }

        [XmlElement("typeArbeider")]
        public string TypeArbeider { get; set; }

        [XmlElement("utfoertInnen")]
        public DateTime? UtfoertInnen { get; set; }

        [XmlElement("bekreftelseInnen")]
        public DateTime? BekreftelseInnen { get; set; }
    }
}