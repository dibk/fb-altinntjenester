﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "http://skjema.kxml.no/dibk/gjennomforingsplan/6.0")]
    [XmlRoot("Gjennomfoeringsplan", Namespace = "http://skjema.kxml.no/dibk/gjennomforingsplan/6.0")]
    public sealed class Gjennomfoeringsplan : GjennomfoeringsplanV7
    {
    }
}
