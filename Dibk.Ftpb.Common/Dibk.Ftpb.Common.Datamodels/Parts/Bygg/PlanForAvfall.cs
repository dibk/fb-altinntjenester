﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class PlanForAvfall
    {
        [XmlArray("avfall")]
        [XmlArrayItem("avfallplan")]
        public Avfallplan[] Avfall { get; set; }

        [XmlArray("avfallsklasseDelsum")]
        [XmlArrayItem("avfallsklassedelsum")]
        public AvfallsklasseDelsum[] AvfallsklasseDelsum { get; set; }

        [XmlElement("sortering")]
        public Avfallsortering Avfallsortering { get; set; }
    }
}