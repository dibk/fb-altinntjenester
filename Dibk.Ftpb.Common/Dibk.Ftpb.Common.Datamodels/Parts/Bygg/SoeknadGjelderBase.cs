﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class SoeknadGjelderBase
    {
        [XmlArray("type")]
        [XmlArrayItem("kode")]
        public Kodeliste[] Type { get; set; }

        [XmlElement("foelgebrev")]
        public string Foelgebrev { get; set; }
    }
}