﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "https://skjema.ft.dibk.no/ferdigattest/v4")]
    [XmlRoot("ferdigattest", Namespace = "https://skjema.ft.dibk.no/ferdigattest/v4")]
    public class FerdigattestV4 : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("metadata")]
        public Metadata Metadata { get; set; }

        [XmlElement("generelleVilkaar")]
        public GenerelleVilkaar GenerelleVilkaar { get; set; }

        [XmlElement("soeknadGjelder")]
        public SoeknadGjelderBase SoeknadGjelder { get; set; }

        [XmlArray("utfallBesvarelse")]
        [XmlArrayItem("utfallSvar")]
        public UtfallSvar[] UtfallBesvarelse { get; set; }

        [XmlElement("kravFerdigattest")]
        public KravFerdigattest KravFerdigattest { get; set; }

        [XmlElement("foretattIkkeSoeknadspliktigeJusteringer")]
        public bool? ErForetattIkkeSoeknadspliktigeJusteringer { get; set; }

        [XmlElement("tilstrekkeligDokumentasjonOverlevertEier")]
        public bool? ErTilstrekkeligDokumentasjonOverlevertEier { get; set; }

        [XmlElement("varmesystem")]
        public Varmesystem Varmesystem { get; set; }

        [XmlElement("tiltakshaver")]
        public Aktoer Tiltakshaver { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("ansvarForByggesaken")]
        public Kodeliste AnsvarForByggesaken { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 10005<br />
        /// <b>DataFormatVersion:</b> 4<br />
        /// <b>DataFormatProvider:</b> DIBK
        /// </summary>
        public FerdigattestV4()
        {
            DataFormatProvider = "DIBK";
            DataFormatId = "10005";
            DataFormatVersion = "4";
        }
    }
}