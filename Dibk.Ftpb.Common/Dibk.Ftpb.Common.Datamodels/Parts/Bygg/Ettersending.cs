﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Ettersending
    {
        [XmlArray("vedlegg")]
        [XmlArrayItem("vedleggsliste")]
        public Vedlegg[] Vedlegg { get; set; }

        [XmlElement("tema")]
        public Kodeliste Tema { get; set; }

        [XmlElement("tittel")]
        public string Tittel { get; set; }

        [XmlElement("kommentar")]
        public string Kommentar { get; set; }

        [XmlArray("underskjema")]
        [XmlArrayItem("skjemanavn")]
        public Kodeliste[] Underskjema { get; set; }
    }
}