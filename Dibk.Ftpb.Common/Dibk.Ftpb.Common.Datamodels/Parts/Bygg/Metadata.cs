﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Metadata
    {
        [XmlElement("fraSluttbrukersystem")]
        public string FraSluttbrukersystem { get; set; }
        
        [XmlElement("ftbId")]
        public string FtbId { get; set; }

        [XmlElement("prosjektnavn")]
        public string Prosjektnavn { get; set; }

        [XmlElement("prosjektnr")]
        public string Prosjektnr { get; set; }

        [XmlElement("foretrukketSpraak")]
        public Kodeliste ForetrukketSpraak { get; set; }
    }
}