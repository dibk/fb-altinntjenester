﻿using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class Signatur
    {
        [XmlElement("signaturdato")]
        public DateTime? Signaturdato { get; set; }

        [XmlElement("signertAv")]
        public string SignertAv { get; set; }

        [XmlElement("signertPaaVegneAv")]
        public string SignertPaaVegneAv { get; set; }
    }
}