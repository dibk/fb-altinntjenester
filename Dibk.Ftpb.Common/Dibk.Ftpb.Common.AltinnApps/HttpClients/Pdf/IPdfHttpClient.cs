﻿namespace Dibk.Ftpb.Common.AltinnApps.HttpClients.Pdf
{
    public interface IPdfHttpClient
    {
        Task<MemoryStream> GeneratePdf(string html);
    }
}