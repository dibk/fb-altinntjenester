﻿namespace Dibk.Ftpb.Common.AltinnApps.Constants
{
    public enum DataType
    {
        Oversendelse = 1,
        Planbestemmelser = 2,
        Plankart2D = 3,
        Plankart3D = 4,
        Undefined = -1
    }

    public enum ValidatorType
    {
        Reguleringsplanforslag
    }
}