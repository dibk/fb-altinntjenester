﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class SluttrapportForAvfall
    {
        [XmlArray("avfall")]
        [XmlArrayItem("avfallrapport")]
        public Avfallrapport[] Avfallrapport { get; set; }

        [XmlElement("sortering")]
        public Avfallsortering Avfallsortering { get; set; }

        [XmlArray("disponeringsmaateDelsum")]
        [XmlArrayItem("disponeringsmaatedelsum")]
        public AvfalldisponeringsmaateDelsum[] AvfalldisponeringsmaateDelsum { get; set; }

        [XmlElement("gjenstaaendeAvfall")]
        public string GjenstaaendeAvfall { get; set; }

        [XmlArray("avfallsklasseDelsum")]
        [XmlArrayItem("avfallsklassedelsum")]
        public AvfallsklasseDelsum[] AvfallsklasseDelsum { get; set; }
    }
}