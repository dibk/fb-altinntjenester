namespace Dibk.Ftpb.Common.AltinnApps.Constants
{
    public static class UILocation
    {
        public const string Oversendelse = "Oversendelse";
        public const string Planbestemmelser = "Planbestemmelser";
        public const string Plankart = "Plankart";
    }

    public static class RuleMessagePrefix
    {
        public const string Warning = "*WARNING*";
    }

    public class DataFormatId
    {
        public static readonly string InnsendingAvReguleringsplanforslag = "InnsendingAvReguleringsplanforslag";
    }

    public class Partstype
    {
        public static readonly string Privatperson = "Privatperson";
        public static readonly string Organisasjon = "Organisasjon";
        public static readonly string Foretak = "Foretak";
        public static readonly string OffentligMyndighet = "Offentlig myndighet";
    }
}