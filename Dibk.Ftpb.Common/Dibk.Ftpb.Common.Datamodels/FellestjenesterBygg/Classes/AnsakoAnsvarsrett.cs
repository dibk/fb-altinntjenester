﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    [XmlRoot("ErklaeringAnsvarsrett")]
    public class AnsakoAnsvarsrett : DataTypeAttributesDatamodelBase
    {
        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("ansvarsrett")]
        public Ansvarsrett Ansvarsretts { get; set; }

        [XmlElement("fraSluttbrukersystem")]
        public string FraSluttbrukersystem { get; set; }

        [XmlElement("prosjektnavn")]
        public string Prosjektnavn { get; set; }

        [XmlElement("prosjektnr")]
        public string Prosjektnr { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] eiendomByggested { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 10000<br />
        /// <b>DataFormatVersion:</b> 1<br />
        /// <b>DataFormatProvider:</b> SERES
        /// </summary>
        public AnsakoAnsvarsrett()
        {
            DataFormatProvider = "SERES";
            DataFormatId = "10000";
            DataFormatVersion = "1";
        }
    }
}
