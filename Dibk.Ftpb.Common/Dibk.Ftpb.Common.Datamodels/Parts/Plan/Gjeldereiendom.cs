﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Plan
{
    public class Gjeldereiendom
    {
        [XmlElement("bolignummer")]
        public string Bolignummer { get; set; }

        [XmlElement("bygningsnummer")]
        public string Bygningsnummer { get; set; }

        [XmlElement("eiendomsidentifikasjon")]
        public Eiendomsidentifikasjon Eiendomsidentifikasjon { get; set; }

        [XmlElement("adresse")]
        public Adresse Adresse { get; set; }

        [XmlElement("kommunenavn")]
        public string Kommunenavn { get; set; }
    }
}