﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenTelemetry.Exporter;
using OpenTelemetry.Logs;
using OpenTelemetry.Metrics;
using OpenTelemetry.Trace;

namespace Dibk.Ftpb.Common.AltinnApps;

/// <summary>
/// OpenTelemetry configuration extensions
/// </summary>
public static class OpenTelemetryExtensions
{
    /// <summary>
    /// Set up OpenTelemetry - config key 'OpenTelemetry:ElasticExporter:Endpoint' must be available.
    /// Fails silently if no config is available.
    /// </summary>
    /// <param name="services"></param>
    /// <param name="config"></param>
    public static void AddOtelElasticExporters(this IServiceCollection services, IConfiguration config)
    {        
        string? endpoint = config.GetValue<string>("OpenTelemetry:ElasticExporter:Endpoint");
        
        //Skip OpenTelemetry configuration if endoint is not set
        if (!Uri.TryCreate(endpoint, UriKind.Absolute, out var parsedEnpoint))
        {
            return;
        }

        services.Configure<OtlpExporterOptions>("ElasticExporter", config.GetSection("OpenTelemetry:ElasticExporter"));
        // Configure exporters for telemetry signals (metrics, traces and logging).
        services.ConfigureOpenTelemetryMeterProvider(builder =>
        {
            builder.AddOtlpExporter("ElasticExporter", (opts, readerOptions) =>
            {
                readerOptions.PeriodicExportingMetricReaderOptions.ExportIntervalMilliseconds = 5_000;
                readerOptions.PeriodicExportingMetricReaderOptions.ExportTimeoutMilliseconds = 4_000;
            });
        });

        services.ConfigureOpenTelemetryTracerProvider(builder =>
        {
            // Customize sampling
            builder.SetSampler(new ParentBasedSampler(new AlwaysOnSampler()));
            builder.AddOtlpExporter("ElasticExporter", configure: null);
        });

        services.ConfigureOpenTelemetryLoggerProvider(builder =>
        {
            builder.AddOtlpExporter("ElasticExporter", configureExporter: null);
        });
    }
}