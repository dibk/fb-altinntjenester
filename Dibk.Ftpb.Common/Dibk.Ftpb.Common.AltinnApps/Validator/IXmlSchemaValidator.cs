﻿namespace Dibk.Ftpb.Common.AltinnApps.Validator
{
    public interface IXmlSchemaValidator
    {
        public List<string> Validate(string key, Stream xmlStream);
    }
}