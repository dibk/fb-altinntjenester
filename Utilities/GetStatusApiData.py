# ---------------------------------------------------------------------------------------
# Python script for getting FT STATUS API data
# ---------------------------------------------------------------------------------------

#!/usr/bin/env python

import logging
import traceback
import sys
import os
import time
import requests
import json
import datetime
from requests.auth import HTTPBasicAuth
import re


class AltinnData:

    def __init__(self, serviceName, serviceCode, serviceEditionCode, url, validFrom, validTo):
        self.ServiceName = serviceName
        self.ServiceCode = serviceCode
        self.ServiceEditionCode = serviceEditionCode
        self.Url = url
        self.ValidFrom = validFrom
        self.ValidTo = validTo
        self.MainForm = None
        self.Forms = []
        self.Attchments = []       

    def AddMainForm(self, altinnForm):
        self.MainForm = altinnForm

    def AppendForm(self, altinnForm):
        self.Forms.append(altinnForm)

    def AppendAttachment(self, altinnAttachment):
        self.Attchments.append(altinnAttachment)


class AltinnForm:

    def __init__(self, formName, dataFormatId, dataFormatVersion, formType, schema):
        self.FormName = formName
        self.DataFormatID = dataFormatId
        self.DataFormatVersion = dataFormatVersion
        self.FormType = formType
        self.Schema = schema


class AltinnAttachment:

    def __init__(self, attachmentTypeName, allowedFileTypes, minAttachmentCount, maxAttachmentCount, maxFileSize):
        self.AttachmentTypeName = attachmentTypeName
        self.AllowedFileTypes = allowedFileTypes
        self.MinAttachmentCount = minAttachmentCount
        self.MaxAttachmentCount = maxAttachmentCount
        self.MaxFileSize = maxFileSize



# Helper class for keeping a table of strings.
class KeepMyText:

    def __init__(self, *args, **kwargs):
        self.myText = []

    def EraseMyText(self):
        self.myText = []

    def AppendMyText(self, newText):
        self.myText.append(newText)

    def GetMyText(self):
        lines = ""
        for line in self.myText:
            lines = lines + line + "\n"
        return lines


class ReadApi:

    def __init__(self, *args, **kwargs):
        self.data = None    
        self.isOK = False
        self.isRequestDone = False

    def SendRequest(self, url, user, passwd):
        try:
            self.data = requests.get(url, auth=HTTPBasicAuth(user,passwd))
            if self.data.status_code == 200:
                self.isOK = True
                self.isRequestDone = True
        except:
            self.isRequestDone = False

        return self.data.status_code

    def GetResultJson(self):
        if self.isOK:
            return self.data.json
        else:
            return None

    def GetResultText(self):
        if self.isOK:
            return self.data.text
        else:
            return None

    def GetResultContent(self):
        if self.isOK:
            return self.data.content
        else:
            return None

    def GetHttpStatus(self):
        if self.isRequestDone:
            return self.data.status_code
        else:
            return None



def createFolder(folderPath):
        """
        Creates a folder if it does not already exist        
    
        Args:
            folderPath (string), path to folder that needs to be created     
        """

        if not os.path.isdir(folderPath):
            os.makedirs(folderPath)


def logger_setup(cmd,loggingDir):
        """
        Sets up the logger function for the poject
    
        Args:
            cmd (string), name of the fuction that is being logged in this run of the tool
            loggingDir (string), file path where log is to be stored (no trailing \ )    
     

        Returns:
            logger object    
        """

        logger = logging.getLogger('DiBK-FTB')
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler('{}\\DiBK-FTB.{}.{}.log'.format(loggingDir,cmd, time.strftime("%Y%m%d_%H%M%S")))
        fh.setLevel(logging.DEBUG)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)

        return logger






### ------------------------ m a i n  c o d e -------------------------------- ###
def main(environment, ar, basicAuthUser, basicAuthPassword):
    """
    Main function: Python code entry.
    """

    try:
        # Setup logging
        loggingFolder = r'c:\temp'
        createFolder(loggingFolder)
        logger = logger_setup("APIlog", loggingFolder)
        logger.debug("APIlog")

        runStartTime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

        # Server
        environment = environment.upper()
        if environment == "PROD":
            envir = "https://admbygg.dibk.no/api/status/"
        elif environment == "TT02":
            envir = "https://test-admbygg.dibk.no/api/status/"
        else:
            logger.error("Commandline arg not PROD or TT02")
            exit()

        url = f"{envir}{ar}"

        logger.info(f"URL: {url}")

        # Collect STATUS API data
        logger.debug(f"Getting API Status Data for {url}")
        r = ReadApi()
        r.SendRequest(url,basicAuthUser,basicAuthPassword)
        if not r.isOK:
            logger.error(f"Could not get data from URL {url}")
            exit
            
        altinnDat = json.loads(r.GetResultText())
        logger.debug(f"Altinn service data received {json.dumps(altinnDat, sort_keys=True, indent=4)}")


        statusList = []
        statusList.append("Nummer,Tid,Status,Info")

        # Liste ut alle Distribusjoner 
        idx = 1
        for dist in altinnDat["Distribusjoner"]:
            sendt = dist["Sendt"]
            status = dist["Status"]

            #"2019-03-03T20:16:34.853"
            matchString = "(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}).?(\d{0,3})"
            
            reMatch = re.search(matchString, sendt)
            if(reMatch):
               sendt = f"{reMatch.group(4)}:{reMatch.group(5)}:{reMatch.group(6)}" 

            try:    
                referanseIdSendt = dist["ReferanseIdSendt"]
                distribusjonstype = dist["Distribusjonstype"]
                print(f"[{idx}]: {sendt} {referanseIdSendt} {distribusjonstype} {status}")
                statusList.append(f"{idx},{sendt},{status},{referanseIdSendt},{distribusjonstype}")

            except:
                print("---------------------------------------------------------------------")
                print(f"[{idx}]: {sendt}")
                print(f"{dist}")
                print("---------------------------------------------------------------------")
                statusList.append(f"{idx},{sendt},{status},{dist}")
 
            idx+=1

        # Write to fil;e
        outFile = open(f"{loggingFolder}\StatusApiDist.{runStartTime}.csv", 'w') 
        for line in statusList:
            outFile.write(line + "\n")    
        outFile.close()
        

    except:
        logger.debug("Unexpected error:", sys.exc_info()[0])
        logger.debug("{}".format(traceback.format_exc(limit=None, chain=True)))


## MAIN FUNCTION ... runs main() if invoked as a standalone script ##
if __name__ == '__main__':
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))
    # ARGS: PROD/TT02, AR, basicAuthUser, basicAuthPassword    
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])    
    
