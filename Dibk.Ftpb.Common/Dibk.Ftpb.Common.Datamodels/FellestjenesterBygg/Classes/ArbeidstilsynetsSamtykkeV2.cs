﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Atil;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    [XmlRoot("ArbeidstilsynetsSamtykke")]
    public class ArbeidstilsynetsSamtykkeV2 : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("tiltakshaver")]
        public Aktoer Tiltakshaver { get; set; }

        [XmlElement("arbeidsplasser")]
        public Arbeidsplasser Arbeidsplasser { get; set; }

        [XmlElement("fakturamottaker")]
        public Fakturamottaker Fakturamottaker { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("betaling")]
        public Betaling Betaling
        { get; set; }

        [XmlArray("krav")]
        [XmlArrayItem("sjekklistekrav")]
        public Sjekklistekrav[] Sjekklistekrav { get; set; }

        [XmlElement("beskrivelseAvTiltak")]
        public BeskrivelseAvTiltak BeskrivelseAvTiltak { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("arbeidstilsynetsSaksnummer")]
        public Saksnummer ArbeidstilsynetsSaksnummer { get; set; }

        [XmlElement("metadata")]
        public MetadataAtilSamtykke Metadata { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 6821<br />
        /// <b>DataFormatVersion:</b> 45957<br />
        /// <b>DataFormatProvider:</b> SERES
        /// </summary>
        public ArbeidstilsynetsSamtykkeV2()
        {
            DataFormatProvider = "SERES";
            DataFormatId = "6821";
            DataFormatVersion = "45957";
        }
    }
}
