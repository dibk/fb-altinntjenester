using Altinn.App.Core.Features.FileAnalysis;
using Altinn.App.Core.Features.Validation;
using Altinn.App.Core.Models.Validation;
using Altinn.Platform.Storage.Interface.Models;

namespace Dibk.Ftpb.Common.AltinnApps.Validator
{
    /// <summary>
    /// Implements FileValidator and helps handling validation messages.
    /// </summary>
    public class DatamodelValidator : IFileValidator
    {
        /// <summary>
        /// Id for filvalidatoren, settes på den aktuelle datatypen i applicationMetadata
        /// </summary>
        public string Id => "datamodelValidator";
        
        /// <summary>
        /// Lager valideringsmeldinger ut fra resultatet av filanalysen
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="fileAnalysisResults"></param>
        /// <returns></returns>
        public Task<(bool Success, IEnumerable<ValidationIssue> Errors)> Validate(DataType dataType, IEnumerable<FileAnalysisResult> fileAnalysisResults)
        {
            List<ValidationIssue> errors = new();

            var deserializationResult = fileAnalysisResults.Where(f => f.Metadata.ContainsKey("Deserialization"));

            foreach (var result in deserializationResult)
            {
                var message = result.Metadata["Deserialization"];

                ValidationIssue validationIssue = new()
                {
                    Source = dataType.Id,
                    Code = "Model is not valid",
                    Severity = ValidationIssueSeverity.Error,
                    Description = $"Innholdet er ikke gyldig i henhold til datamodell. Feilmelding: {message}",
                };

                errors.Add(validationIssue);
            }

            return Task.FromResult<(bool Success, IEnumerable<ValidationIssue> Errors)>(
                errors.Any()
                ? (false, errors)
                : (true, errors));
        }
    }
}
