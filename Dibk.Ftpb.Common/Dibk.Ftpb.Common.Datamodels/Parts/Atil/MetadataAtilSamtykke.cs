﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Atil
{
    public class MetadataAtilSamtykke
    {
        [XmlElement("fraSluttbrukersystem")]
        public string FraSluttbrukersystem { get; set; }

        [XmlElement("ftbId")]
        public string FtbId { get; set; }

        [XmlElement("prosjektnavn")]
        public string Prosjektnavn { get; set; }

        [XmlElement("sluttbrukersystemUrl")]
        public string SluttbrukersystemUrl { get; set; }

        [XmlElement("hovedinnsendingsnummer")]
        public string Hovedinnsendingsnummer { get; set; }

        [XmlElement("erNorskSvenskDansk")]
        public bool? ErNorskSvenskDansk { get; set; }

        [XmlElement("klartForSigneringFraSluttbrukersystem")]
        public bool? KlartForSigneringFraSluttbrukersystem { get; set; }

        [XmlElement("unntattOffentlighet")]
        public bool? UnntattOffentlighet { get; set; }

        [XmlElement("kommunensOrganisasjonsnummer")]
        public string KommunensOrganisasjonsnummer { get; set; }

    }
}
