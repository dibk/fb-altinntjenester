using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Dibk.Ftpb.Common.Datamodels;

namespace Dibk.Ftpb.Common.Tests;

[TestClass]
public class GjennomfoeringsplanTests
{
    [TestMethod]
    public void CurrentVersionIs7()
    {
        var ig = new Gjennomfoeringsplan();
        Assert.IsInstanceOfType<GjennomfoeringsplanV7>(ig);
    }

    [TestMethod]
    public void DeserializeCurrentVersionToClassWithUnspecifiedVersion_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/Gjennomfoeringsplan.xml");

        var ig = XmlSerializerUtil.DeserializeFromString<Gjennomfoeringsplan>(xmlContent, false);

        Assert.IsNotNull(ig);
    }

    [TestMethod]
    public void DeserializeXml_Gjennomfoeringsplan_V7_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/Gjennomfoeringsplan.xml");

        var gfp7 = XmlSerializerUtil.DeserializeFromString<GjennomfoeringsplanV7>(xmlContent, false);

        Assert.IsNotNull(gfp7);

        //DataFormat-attributter
        Assert.AreEqual("SERES", gfp7.DataFormatProvider);
        Assert.AreEqual("6146", gfp7.DataFormatId);
        Assert.AreEqual("44096", gfp7.DataFormatVersion);

        //EiendomByggested
        Assert.IsNotNull(gfp7.EiendomByggested);
        Assert.IsNotNull(gfp7.EiendomByggested[0]);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Eiendomsidentifikasjon);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Seksjonsnummer);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Gaardsnummer);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Bruksnummer);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Festenummer);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Kommunenummer);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Gatenavn);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Husnr);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Adresselinje1);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Adresselinje2);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Adresselinje3);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Postnr);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Poststed);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Bokstav);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Adresse.Landkode);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Bygningsnummer);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Kommunenavn);
        Assert.IsNotNull(gfp7.EiendomByggested[0].Bolignummer);

        Assert.AreEqual("78", gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Bruksnummer);
        Assert.AreEqual("9", gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Festenummer);
        Assert.AreEqual("56", gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Gaardsnummer);
        Assert.AreEqual("1234", gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Kommunenummer);
        Assert.AreEqual("10", gfp7.EiendomByggested[0].Eiendomsidentifikasjon.Seksjonsnummer);
        Assert.AreEqual("Street 1", gfp7.EiendomByggested[0].Adresse.Adresselinje1);
        Assert.AreEqual("1234", gfp7.EiendomByggested[0].Adresse.Postnr);
        Assert.AreEqual("City", gfp7.EiendomByggested[0].Adresse.Poststed);
        Assert.AreEqual("Main Street", gfp7.EiendomByggested[0].Adresse.Gatenavn);
        Assert.AreEqual("NO", gfp7.EiendomByggested[0].Adresse.Landkode);
        Assert.AreEqual("12", gfp7.EiendomByggested[0].Adresse.Husnr);
        Assert.AreEqual("A", gfp7.EiendomByggested[0].Adresse.Bokstav);
        Assert.AreEqual("100", gfp7.EiendomByggested[0].Bygningsnummer);
        Assert.AreEqual("B100", gfp7.EiendomByggested[0].Bolignummer);
        Assert.AreEqual("CityName", gfp7.EiendomByggested[0].Kommunenavn);

        //KommunensSaksnummer
        Assert.IsNotNull(gfp7.KommunensSaksnummer);
        Assert.AreEqual(2023, gfp7.KommunensSaksnummer.Saksaar);
        Assert.AreEqual(456, gfp7.KommunensSaksnummer.Sakssekvensnummer);

        //Versjon
        Assert.IsNotNull(gfp7.Versjon);
        Assert.AreEqual("1.0", gfp7.Versjon);

        //Signatur
        Assert.IsNotNull(gfp7.Signatur);
        Assert.AreEqual("John Doe", gfp7.Signatur.SignertAv);
        Assert.AreEqual("Jane Doe", gfp7.Signatur.SignertPaaVegneAv);
        Assert.AreEqual(new DateTime(2023, 05, 28, 09, 00, 00), gfp7.Signatur.Signaturdato);

        //AnsvarligSoekerTiltaksklasse

        Assert.IsNotNull(gfp7.AnsvarligSoekerTiltaksklasse);
        Assert.AreEqual("Kode1", gfp7.AnsvarligSoekerTiltaksklasse.Kodeverdi);
        Assert.AreEqual("Beskrivelse av kode 1", gfp7.AnsvarligSoekerTiltaksklasse.Kodebeskrivelse);

        //AnsvarsomraadeGjennomfoeringsplan
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan);
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan[0]);
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan[0].Funksjon);
        Assert.AreEqual("F1", gfp7.Gjennomfoeringsplan[0].Funksjon.Kodeverdi);
        Assert.AreEqual("F1", gfp7.Gjennomfoeringsplan[0].Funksjon.Kodeverdi);
        Assert.AreEqual("test", gfp7.Gjennomfoeringsplan[0].Ansvarsomraade);
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan[0].Tiltaksklasse);
        Assert.AreEqual("T1", gfp7.Gjennomfoeringsplan[0].Tiltaksklasse.Kodeverdi);
        Assert.AreEqual("Tiltaksklasse 1", gfp7.Gjennomfoeringsplan[0].Tiltaksklasse.Kodebeskrivelse);
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan[0].Foretak);
        Assert.AreEqual(true, gfp7.Gjennomfoeringsplan[0].Foretak.HarSentralGodkjenning);
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan[0].Foretak.Partstype);
        Assert.AreEqual("T1", gfp7.Gjennomfoeringsplan[0].Foretak.Partstype.Kodeverdi);
        Assert.AreEqual("Tiltaksklasse 1", gfp7.Gjennomfoeringsplan[0].Foretak.Partstype.Kodebeskrivelse);
        Assert.AreEqual("12345678910", gfp7.Gjennomfoeringsplan[0].Foretak.Foedselsnummer);
        Assert.AreEqual("987654321", gfp7.Gjennomfoeringsplan[0].Foretak.Organisasjonsnummer);
        Assert.AreEqual("testnavn", gfp7.Gjennomfoeringsplan[0].Foretak.Navn);
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan[0].Foretak.Adresse);
        Assert.AreEqual("testgate 1", gfp7.Gjennomfoeringsplan[0].Foretak.Adresse.Adresselinje1);
        Assert.AreEqual("1234", gfp7.Gjennomfoeringsplan[0].Foretak.Adresse.Postnr);
        Assert.AreEqual("teststed", gfp7.Gjennomfoeringsplan[0].Foretak.Adresse.Poststed);
        Assert.AreEqual("NO", gfp7.Gjennomfoeringsplan[0].Foretak.Adresse.Landkode);
        Assert.AreEqual("12345678", gfp7.Gjennomfoeringsplan[0].Foretak.Telefonnummer);
        Assert.AreEqual("98765432", gfp7.Gjennomfoeringsplan[0].Foretak.Mobilnummer);
        Assert.AreEqual("jane.smith@example.com", gfp7.Gjennomfoeringsplan[0].Foretak.Epost);
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan[0].Foretak.Kontaktperson);
        Assert.AreEqual("jane.smith@example.com", gfp7.Gjennomfoeringsplan[0].Foretak.Kontaktperson.Epost);
        Assert.AreEqual("Jane Smith", gfp7.Gjennomfoeringsplan[0].Foretak.Kontaktperson.Navn);
        Assert.AreEqual("11223344", gfp7.Gjennomfoeringsplan[0].Foretak.Kontaktperson.Telefonnummer);
        Assert.AreEqual("44332211", gfp7.Gjennomfoeringsplan[0].Foretak.Kontaktperson.Mobilnummer);
        Assert.AreEqual(true, gfp7.Gjennomfoeringsplan[0].SamsvarKontrollPlanlagtVedRammetillatelse);
        Assert.AreEqual(true, gfp7.Gjennomfoeringsplan[0].SamsvarKontrollPlanlagtVedIgangsettingstillatelse);
        Assert.AreEqual(true, gfp7.Gjennomfoeringsplan[0].SamsvarKontrollPlanlagtVedMidlertidigBrukstillatelse);
        Assert.AreEqual(true, gfp7.Gjennomfoeringsplan[0].SamsvarKontrollPlanlagtVedFerdigattest);
        Assert.AreEqual(new DateTime(2023, 05, 28), gfp7.Gjennomfoeringsplan[0].SamsvarKontrollForeliggerVedRammetillatelse);
        Assert.AreEqual(new DateTime(2023, 06, 28), gfp7.Gjennomfoeringsplan[0].SamsvarKontrollForeliggerVedIgangsettingstillatelse);
        Assert.AreEqual(new DateTime(2023, 07, 28), gfp7.Gjennomfoeringsplan[0].SamsvarKontrollForeliggerVedMidlertidigBrukstillatelse);
        Assert.AreEqual(new DateTime(2023, 08, 28), gfp7.Gjennomfoeringsplan[0].SamsvarKontrollForeliggerVedFerdigattest);
        Assert.AreEqual(false, gfp7.Gjennomfoeringsplan[0].AnsvarsomraadetAvsluttet);
        Assert.AreEqual("Ref1234", gfp7.Gjennomfoeringsplan[0].ErklaeringArkivreferanse);
        Assert.AreEqual(true, gfp7.Gjennomfoeringsplan[0].ErklaeringSignert);
        Assert.AreEqual("SysRef1234", gfp7.Gjennomfoeringsplan[0].SluttbrukersystemReferanse);
        Assert.AreEqual("Status1", gfp7.Gjennomfoeringsplan[0].SluttbrukersystemStatus);
        Assert.AreEqual(new DateTime(2023, 05, 28), gfp7.Gjennomfoeringsplan[0].AnsvarsomraadeSistEndret);
        Assert.AreEqual("vedlegg.pdf", gfp7.Gjennomfoeringsplan[0].FilnavnVedlegg);
        Assert.IsNotNull(gfp7.Gjennomfoeringsplan[0].AnsvarsomraadeStatus);
        Assert.AreEqual("Status1", gfp7.Gjennomfoeringsplan[0].AnsvarsomraadeStatus.Kodeverdi);
        Assert.AreEqual("Status beskrivelse 1", gfp7.Gjennomfoeringsplan[0].AnsvarsomraadeStatus.Kodebeskrivelse);

        //Metadata
        Assert.IsNotNull(gfp7.Metadata);
        Assert.AreEqual("ID1234", gfp7.Metadata.FtbId);
        Assert.AreEqual("Prosjekt X", gfp7.Metadata.Prosjektnavn);
        Assert.AreEqual("http://example.com", gfp7.Metadata.SluttbrukersystemUrl);
        Assert.AreEqual("12345", gfp7.Metadata.Hovedinnsendingsnummer);
        Assert.AreEqual(true, gfp7.Metadata.KlartForSigneringFraSluttbrukersystem);
        Assert.AreEqual("System1", gfp7.Metadata.FraSluttbrukersystem);

        //AnsvarligSoeker
        Assert.IsNotNull(gfp7.AnsvarligSoeker);
        Assert.IsNotNull(gfp7.AnsvarligSoeker.Partstype);
        Assert.AreEqual("T1", gfp7.AnsvarligSoeker.Partstype.Kodeverdi);
        Assert.AreEqual("Tiltaksklasse 1", gfp7.AnsvarligSoeker.Partstype.Kodebeskrivelse);
        Assert.AreEqual("12345678910", gfp7.AnsvarligSoeker.Foedselsnummer);

        var kontaktperson = gfp7.AnsvarligSoeker.Kontaktperson;
        Assert.IsNotNull(kontaktperson);
        Assert.AreEqual("Jane Smith", kontaktperson.Navn);
        Assert.AreEqual("11223344", kontaktperson.Telefonnummer);
        Assert.AreEqual("44332211", kontaktperson.Mobilnummer);
        Assert.AreEqual("jane.smith@example.com", kontaktperson.Epost);
        Assert.AreEqual("987654321", gfp7.AnsvarligSoeker.Organisasjonsnummer);
        Assert.AreEqual("testnavn", gfp7.AnsvarligSoeker.Navn);

        var adresse = gfp7.AnsvarligSoeker.Adresse;
        Assert.IsNotNull(adresse);
        Assert.AreEqual("testgate 1", adresse.Adresselinje1);
        Assert.AreEqual("1234", adresse.Postnr);
        Assert.AreEqual("teststed", adresse.Poststed);
        Assert.AreEqual("NO", adresse.Landkode);
        Assert.AreEqual("12345678", gfp7.AnsvarligSoeker.Telefonnummer);
        Assert.AreEqual("98765432", gfp7.AnsvarligSoeker.Mobilnummer);
        Assert.AreEqual("jane.smith@example.com", gfp7.AnsvarligSoeker.Epost);
    }
}
