﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Aktoer
    {
        [XmlElement("partstype")]
        public Kodeliste Partstype { get; set; }

        [XmlElement("foedselsnummer")]
        public string Foedselsnummer { get; set; }

        [XmlElement("organisasjonsnummer")]
        public string Organisasjonsnummer { get; set; }

        [XmlElement("navn")]
        public string Navn { get; set; }

        [XmlElement("adresse")]
        public EnkelAdresse Adresse { get; set; }

        [XmlElement("telefonnummer")]
        public string Telefonnummer { get; set; }

        [XmlElement("mobilnummer")]
        public string Mobilnummer { get; set; }

        [XmlElement("epost")]
        public string Epost { get; set; }

        [XmlElement("kontaktperson")]
        public Kontaktperson Kontaktperson { get; set; }
    }
}