﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Ansvarsomraade
    {
        [XmlElement("beskrivelseAvAnsvarsomraade")]
        public string BeskrivelseAvAnsvarsomraade { get; set; }

        [XmlElement("dekkesOmraadeAvSentralGodkjenning")]
        public bool? DekkesOmraadeAvSentralGodkjenning { get; set; }

        [XmlElement("funksjon")]
        public Kodeliste Funksjon { get; set; }

        [XmlElement("samsvarKontrollVedFerdigattest")]
        public bool? SamsvarKontrollVedFerdigattest { get; set; }

        [XmlElement("samsvarKontrollVedMidlertidigBrukstillatelse")]
        public bool? SamsvarKontrollVedMidlertidigBrukstillatelse { get; set; }

        [XmlElement("samsvarKontrollVedRammetillatelse")]
        public bool? SamsvarKontrollVedRammetillatelse { get; set; }

        [XmlElement("tiltaksklasse")]
        public Kodeliste Tiltaksklasse { get; set; }

        [XmlElement("soeknadssystemetsReferanse")]
        public string SoeknadssystemetsReferanse { get; set; }
    }
}