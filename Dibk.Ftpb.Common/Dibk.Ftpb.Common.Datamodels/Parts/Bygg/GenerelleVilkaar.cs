﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class GenerelleVilkaar
    {
        [XmlElement("norskSvenskDansk")]
        public bool? NorskSvenskDansk { get; set; }
    }
}