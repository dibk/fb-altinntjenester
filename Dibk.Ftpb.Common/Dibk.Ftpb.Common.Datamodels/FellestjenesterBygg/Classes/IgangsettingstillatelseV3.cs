﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "http://skjema.kxml.no/dibk/igangsettingstillatelse/v3.0")]
    [XmlRoot("igangsettingstillatelse", Namespace = "http://skjema.kxml.no/dibk/igangsettingstillatelse/v3.0")]
    public class IgangsettingstillatelseV3 : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("metadata")]
        public Metadata Metadata { get; set; }

        [XmlElement("generelleVilkaar")]
        public GenerelleVilkaarIgV3 GenerelleVilkaar { get; set; }

        [XmlElement("soeknadGjelder")]
        public SoeknadGjelder SoeknadGjelder { get; set; }

        [XmlArray("delsoeknader")]
        [XmlArrayItem("delsoeknad")]
        public Delsoeknad[] Delsoeknader { get; set; }

        [XmlArray("utfallBesvarelse")]
        [XmlArrayItem("utfallSvar")]
        public UtfallSvarV3[] UtfallBesvarelse { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("ansvarForByggesaken")]
        public Kodeliste AnsvarForByggesaken { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 10003<br />
        /// <b>DataFormatVersion:</b> 3.0<br />
        /// <b>DataFormatProvider:</b> DIBK
        /// </summary>
        public IgangsettingstillatelseV3()
        {
            DataFormatProvider = "DIBK"; 
            DataFormatId = "10003";
            DataFormatVersion = "3.0";
        }
    }
}