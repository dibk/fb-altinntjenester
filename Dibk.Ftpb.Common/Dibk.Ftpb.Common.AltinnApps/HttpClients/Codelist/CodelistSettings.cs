﻿namespace Dibk.Ftpb.Common.AltinnApps.HttpClients.Codelist
{
    public class CodelistSettings
    {
        public static readonly string SectionName = "Codelists";
        public Uri Arealformål { get; set; }
        public Uri Feltnavn { get; set; }
        public Uri Hensynskategori { get; set; }
        public Uri Målemetode { get; set; }
        public int CacheDurationDays { get; set; }
    }
}