# FB-AltinnTjenester

Her finner du alle skjemaer og ressursfiler som trengs for å sette opp innsendingstjenestene i Altinn for FellestjenesterBygg-prosjektet.

## Dibk.Ftpb.Common.Datamodels

Dibk.Ftpb.Common.Datamodels er et .Net Standard prosjekt for distribuering av XSDer og pregenererte C# klasser.

### Legge til nye XSDer og generere C# klasser

Nye XSDer må plasseres enten i mappen for 'FellestjenesterBygg' eller for 'FellestjenesterPlan'. For at XSDen skal være med og tilgjengelig i biblioteket må man huske på å legge den til som en 'Embedded Resource' i prosjektet Dibk.Ftpb.Common.Datamodels. Dette kan gjøres enten via Visual Studio eller ved manuell editering av ``` Dibk.Ftpb.Common.Datamodels.csproj ```

Eksempel:

````xml
<ItemGroup>
    .    
    .
    <EmbeddedResource Include="FellestjenesterPlan\Xsd\uttalelseOffentligEttersyn.xsd" />
</ItemGroup>
````

## Dibk.Ftpb.Common

Dette er et prosjekt som bør inneholde klasser, konstanter og verdier som brukes på tvers i Ftpbs interne økosystem.

### CI/CD og Nuget

Det er satt opp automatisk build av Dibk.Ftpb.Common solution vha Bitbucket pipelines. <https://bitbucket.org/dibk/fb-altinntjenester/pipelines>

#### Ny versjon av pakke

Ønsker man å tilgjengeliggjøre en ny versjon av en NuGet-pakke gjøres dette vha tags (standard git tag). For å unngå generering unødvendige nye versjoner må det anngis i tagen hvilken prosjekt det skal genereres pakke for.
Strukturen på tag må være som følger `` {prosjektnavn}-{versjonsnummer} ``  
Eksempel: `` Dibk.Ftpb.Common-0.1.1 `` eller `` Dibk.Ftpb.Common.AltinnApps-0.7.1-preview-1 `` 

Det er også støtte for å lage pre-release versjoner `` Dibk.Ftpb.Common-0.1.2-alfa ``

Release av en ny versjon kan gjøres fra hvilken som helst branch, men det er ønskelig at man merker releaser fra development og feature-brancher som pre-release.

Merk at det ligger en liste med gyldige navn på nugetpakker i fila __load-project-info-from-tag.sh__ som benyttes i pipeline ved bygging av prosjektet. Eventuelle nye pakker må legges til her.
