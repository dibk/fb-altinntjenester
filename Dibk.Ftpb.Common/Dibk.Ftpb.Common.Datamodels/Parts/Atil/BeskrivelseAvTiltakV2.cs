﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Atil
{
    public class BeskrivelseAvTiltakV2
    {
        [XmlElement("bruk")]
        public FormaaltypeV2 Formaaltype { get; set; }

        [XmlArray("typer")]
        [XmlArrayItem("kode")]
        public Kodeliste[] Tiltakstype { get; set; }

        [XmlElement("BRA")]
        public string BRA { get; set; }
    }
}