﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Ansvarsrett
    {
        [XmlElement("erklaeringAnsvarligProsjekterende")]
        public bool? ErklaeringAnsvarligProsjekterende { get; set; }

        [XmlElement("erklaeringAnsvarligUtfoerende")]
        public bool? ErklaeringAnsvarligUtfoerende { get; set; }

        [XmlElement("erklaeringAnsvarligKontrollerende")]
        public bool? ErklaeringAnsvarligKontrollerende { get; set; }

        [XmlArray("ansvarsomraader")]
        [XmlArrayItem("ansvarsomraade")]
        public Ansvarsomraade[] Ansvarsomraader { get; set; }

        [XmlElement("foretak")]
        public Foretak Foretak { get; set; }
    }
}