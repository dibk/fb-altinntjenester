﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Mangelbesvarelse
    {
        [XmlArray("vedlegg")]
        [XmlArrayItem("vedleggsliste")]
        public Vedlegg[] Vedlegg { get; set; }

        [XmlElement("tema")]
        public Kodeliste Tema { get; set; }

        [XmlElement("mangel")]
        public Mangel Mangel { get; set; }

        [XmlElement("tittel")]
        public string Tittel { get; set; }

        [XmlElement("beskrivelseFraKommunen")]
        public string BeskrivelseFraKommunen { get; set; }

        [XmlElement("erMangelBesvart")]
        public bool? ErMangelBesvart { get; set; }

        [XmlElement("erMangelBesvaresSenere")]
        public bool? ErMangelBesvaresSenere { get; set; }

        [XmlElement("kommentar")]
        public string Kommentar { get; set; }

        [XmlArray("underskjema")]
        [XmlArrayItem("skjemanavn")]
        public Kodeliste[] Underskjema { get; set; }
    }
}