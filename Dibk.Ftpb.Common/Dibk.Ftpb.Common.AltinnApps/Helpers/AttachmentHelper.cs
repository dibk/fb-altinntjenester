﻿using Dibk.Ftpb.Common.Constants;

namespace Dibk.Ftpb.Common.AltinnApps.Helpers
{
    public class AttachmentHelper
    {
        public static string GetFilename(string dataType, string plannavn = null, string fileExtension = "xml", string versjon = null, string vertikalnivaa = null, string epsgkode = null)
        {
            if (string.IsNullOrWhiteSpace(dataType))
            {
                // TODO logging 
                return null;
            }

            var name = dataType;

            if (dataType == DataTypes.OversendelseReguleringsplanforslag || dataType == DataTypes.RefDataAsPdf)
            {
                name = "oversendelse-planforslag";
            }
            else if (dataType == DataTypes.Planbestemmelser)
            {
                name = "Planbestemmelser";
            }
            else if (dataType == DataTypes.Plankart)
            {
                name = "Plankart";
            }
            else if (dataType == DataTypes.Plankart)
            {
                name = "Plankart";
            }
            else if (dataType == DataTypes.Illustrasjon)
            {
                name = "illustrasjon-egendefinertDel";
            }
            else if (dataType == DataTypes.Valideringsrapport)
            {
                return "Valideringsrapport";
            }

            return CreateFileName(name, plannavn, versjon, fileExtension, vertikalnivaa, epsgkode);
        }


        private static string CreateFileName(string name, string plannavn, string versjon, string contentType, string vertikalnivaa = null, string epsgkode = null)
        {
            var fileName = name;

            if (!string.IsNullOrWhiteSpace(plannavn))
            {
                fileName += "-" + plannavn;
            }

            if (!string.IsNullOrWhiteSpace(versjon))
            {
                fileName += "-" + versjon;
            }

            if (!string.IsNullOrWhiteSpace(vertikalnivaa))
            {
                fileName += "-" + vertikalnivaa;
            }

            if (!string.IsNullOrWhiteSpace(epsgkode))
            {
                fileName += "-" + epsgkode;
            }

            if (string.IsNullOrWhiteSpace(contentType))
            {
                // todo logge eller gi evt feil... 
                // return null?
            }

            fileName += "." + contentType;

            return fileName;
        }

        public static List<string> AttachmentOrder
        {
            get
            {
                var attachmentOrder = new List<string>
                {
                    DataTypes.OversendelseReguleringsplanforslag,
                    DataTypes.RefDataAsPdf,
                    DataTypes.Planbestemmelser,
                    DataTypes.Plankart,
                    DataTypes.Planbeskrivelse,
                    DataTypes.Planvarsel,
                    DataTypes.Varslingsliste,
                    DataTypes.UttalelseSamling,
                    DataTypes.UttalelseKommentarer,
                    DataTypes.Illustrasjon,
                    DataTypes.Konsekvensutredning,
                    DataTypes.ROSAnalyse,
                    DataTypes.Utredning,
                    DataTypes.Rapport,
                    DataTypes.Valideringsrapport,
                    DataTypes.Annet
                };

                return attachmentOrder;
            }
        }
    }
}