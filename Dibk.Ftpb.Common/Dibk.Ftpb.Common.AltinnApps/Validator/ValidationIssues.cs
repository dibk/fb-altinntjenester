﻿using Altinn.App.Core.Models.Validation;

namespace Dibk.Ftpb.Common.AltinnApps.Validator;

/// <summary>
/// Common validation errors
/// </summary>
public static class ValidationIssues
{
    /// <summary>
    /// Standard error for serialization issues.
    /// </summary>
    /// <param name="dataElementId"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static ValidationIssue GetKonverteringAvDatastroemIssue<T>(string dataElementId) => new()
    {
        Severity = ValidationIssueSeverity.Error,
        Code = "500",
        DataElementId = dataElementId,
        Description = $"Feil under konvertering av datastrøm til {typeof(T).Name}"
    };

    /// <summary>
    /// Standard error for validation issues.
    /// </summary>
    /// <param name="dataElementId"></param>
    /// <param name="dataType"></param>
    /// <returns></returns>
    public static ValidationIssue GetFeilUnderValideringIssue(string dataElementId, string dataType) => new()
    {
        Severity = ValidationIssueSeverity.Error,
        Code = "500",
        DataElementId = dataElementId,
        Description = $"Feil under validering av {dataType}, dataElement id: {dataElementId}"
    };
}
