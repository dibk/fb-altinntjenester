using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Dibk.Ftpb.Common.Datamodels;

namespace Dibk.Ftpb.Common.Tests;

[TestClass]
public class IgangsettingstillatelseTests
{
    [TestMethod]
    public void CurrentVersionIs4()
    {
        var ig = new Igangsettingstillatelse();
        Assert.IsInstanceOfType<IgangsettingstillatelseV4>(ig);
        Assert.IsNotInstanceOfType<IgangsettingstillatelseV3>(ig);
    }

    [TestMethod]
    public void DeserializeCurrentVersionToClassWithUnspecifiedVersion_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/IgangsettingstillatelseV4.xml");

        var ig = XmlSerializerUtil.DeserializeFromString<Igangsettingstillatelse>(xmlContent, false);

        Assert.IsNotNull(ig);
    }

    [TestMethod]
    public void V3Data_DeserializeTo_V4Model_throws_Exception()
    {
        var xmlContent = File.ReadAllText("TestData/IgangsettingstillatelseV3.xml");

        Assert.ThrowsException<InvalidOperationException>(new Action(() =>
            XmlSerializerUtil.DeserializeFromString<IgangsettingstillatelseV4>(xmlContent, false)));
    }

    [TestMethod]
    public void IgangsettingstillatelseV3_DeserializeXml_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/IgangsettingstillatelseV3.xml");

        var igv3 = XmlSerializerUtil.DeserializeFromString<IgangsettingstillatelseV3>(xmlContent, false);

        Assert.IsNotNull(igv3);

        //DataFormat-attributter
        Assert.AreEqual("DIBK", igv3.DataFormatProvider);
        Assert.AreEqual("10003", igv3.DataFormatId);
        Assert.AreEqual("3.0", igv3.DataFormatVersion);

        //EiendomByggested
        Assert.AreEqual(2, igv3.EiendomByggested.Length);
        Assert.IsNotNull(igv3.EiendomByggested[0].Eiendomsidentifikasjon);
        Assert.AreEqual("89", igv3.EiendomByggested[0].Eiendomsidentifikasjon.Bruksnummer);
        Assert.AreEqual("10", igv3.EiendomByggested[0].Eiendomsidentifikasjon.Festenummer);
        Assert.AreEqual("567", igv3.EiendomByggested[0].Eiendomsidentifikasjon.Gaardsnummer);
        Assert.AreEqual("1234", igv3.EiendomByggested[0].Eiendomsidentifikasjon.Kommunenummer);
        Assert.AreEqual("11", igv3.EiendomByggested[0].Eiendomsidentifikasjon.Seksjonsnummer);
        Assert.IsNotNull(igv3.EiendomByggested[0].Adresse);
        Assert.AreEqual("Eksempelveien 123", igv3.EiendomByggested[0].Adresse.Adresselinje1);
        Assert.AreEqual("1234", igv3.EiendomByggested[0].Adresse.Postnr);
        Assert.AreEqual("Eksempelby", igv3.EiendomByggested[0].Adresse.Poststed);
        Assert.AreEqual("NO", igv3.EiendomByggested[0].Adresse.Landkode);
        Assert.AreEqual("123", igv3.EiendomByggested[0].Bygningsnummer);
        Assert.AreEqual("1", igv3.EiendomByggested[0].Bolignummer);
        Assert.AreEqual("Eksempelkommune", igv3.EiendomByggested[0].Kommunenavn);

        //KommunensSaksnummer
        Assert.IsNotNull(igv3.KommunensSaksnummer);
        Assert.AreEqual(2024, igv3.KommunensSaksnummer.Saksaar);
        Assert.AreEqual(56789, igv3.KommunensSaksnummer.Sakssekvensnummer);

        //Metadata
        Assert.IsNotNull(igv3.Metadata);
        Assert.AreEqual("FTB123", igv3.Metadata.FtbId);
        Assert.AreEqual("Eksempelprosjekt", igv3.Metadata.Prosjektnavn);
        Assert.AreEqual("123456", igv3.Metadata.Prosjektnr);
        Assert.IsNotNull(igv3.Metadata.ForetrukketSpraak);
        Assert.AreEqual("NO", igv3.Metadata.ForetrukketSpraak.Kodeverdi);
        Assert.AreEqual("Norsk", igv3.Metadata.ForetrukketSpraak.Kodebeskrivelse);
        Assert.AreEqual("SystemX", igv3.Metadata.FraSluttbrukersystem);

        //GenerelleVilkaar
        Assert.IsNotNull(igv3.GenerelleVilkaar);
        Assert.AreEqual(false, igv3.GenerelleVilkaar.BeroererArbeidsplasser);
        Assert.AreEqual(true, igv3.GenerelleVilkaar.NorskSvenskDansk);

        //Delsøknad
        Assert.IsNotNull(igv3.Delsoeknader);
        Assert.AreEqual(1, igv3.Delsoeknader.Length);
        var delsoeknad = igv3.Delsoeknader[0];
        Assert.IsNotNull(delsoeknad);
        Assert.AreEqual("Bygging av garasje", delsoeknad.DelAvTiltaket);
        Assert.AreEqual(new DateTime(2024, 02, 16), delsoeknad.Tillatelsedato);
        Assert.AreEqual("DS123", delsoeknad.Delsoeknadsnummer);
        Assert.AreEqual("Eksempelkommentar", delsoeknad.Kommentar);
        Assert.IsNotNull(delsoeknad.Type);
        Assert.AreEqual(1, delsoeknad.Type.Length);
        var delsoeknadType = delsoeknad.Type[0];
        Assert.AreEqual("TYPE1", delsoeknadType.Kodeverdi);
        Assert.AreEqual("Type 1", delsoeknadType.Kodebeskrivelse);

        //SoeknadGjelder
        Assert.IsNotNull(igv3.SoeknadGjelder);
        Assert.IsNotNull(igv3.SoeknadGjelder.Type);
        Assert.AreEqual(1, igv3.SoeknadGjelder.Type.Length);
        var soeknadGjelderType = igv3.SoeknadGjelder.Type[0];
        Assert.AreEqual("TYPE2", soeknadGjelderType.Kodeverdi);
        Assert.AreEqual("Type 2", soeknadGjelderType.Kodebeskrivelse);
        Assert.AreEqual("Her er et følgebrev", igv3.SoeknadGjelder.Foelgebrev);
        Assert.AreEqual(true, igv3.SoeknadGjelder.GjelderHeleTiltaket);
        Assert.AreEqual("Bygning av bolig", igv3.SoeknadGjelder.DelAvTiltaket);
        Assert.AreEqual("DS456", igv3.SoeknadGjelder.Delsoeknadsnummer);

        //UtfallBesvarelse
        Assert.IsNotNull(igv3.UtfallBesvarelse);
        Assert.AreEqual(2, igv3.UtfallBesvarelse.Length);
        var utfallSvar = igv3.UtfallBesvarelse[0];
        Assert.IsNotNull(utfallSvar.Utfalltype);
        Assert.AreEqual("UTFALL1", utfallSvar.Utfalltype.Kodeverdi);
        Assert.AreEqual("Utfall 1", utfallSvar.Utfalltype.Kodebeskrivelse);
        Assert.AreEqual("123456", utfallSvar.UtfallId);
        Assert.IsNotNull(utfallSvar.Tema);
        Assert.AreEqual("TEMA1", utfallSvar.Tema.Kodeverdi);
        Assert.AreEqual("Tema 1", utfallSvar.Tema.Kodebeskrivelse);
        Assert.AreEqual("Utfall 1 tittel", utfallSvar.Tittel);
        Assert.IsNotNull(utfallSvar.UtloestFraSjekkpunkt);
        Assert.AreEqual("Sjekkpunkt_1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktId);
        Assert.AreEqual("Sjekkpunkteier 1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktEier);
        Assert.AreEqual("Dette er utfall 1", utfallSvar.Beskrivelse);
        Assert.AreEqual(false, utfallSvar.ErUtfallBesvaresSenere);
        Assert.AreEqual(true, utfallSvar.ErUtfallBesvart);
        Assert.AreEqual("Utfall 1 kommentar", utfallSvar.Kommentar);
        //Vedlegg
        Assert.IsNotNull(utfallSvar.Vedleggsliste);
        Assert.AreEqual(2, utfallSvar.Vedleggsliste.Length);
        var vedlegg = utfallSvar.Vedleggsliste[0];
        Assert.AreEqual("V1", vedlegg.Versjonsnummer);
        Assert.IsNotNull(vedlegg.Vedleggstype);
        Assert.AreEqual("TYPE1", vedlegg.Vedleggstype.Kodeverdi);
        Assert.AreEqual("Type 1", vedlegg.Vedleggstype.Kodebeskrivelse);
        Assert.AreEqual(new DateTime(2024, 02, 16), vedlegg.Versjonsdato);
        Assert.AreEqual("vedlegg1.pdf", vedlegg.Filnavn);
        //Underskjema
        Assert.IsNotNull(utfallSvar.Underskjema);
        Assert.AreEqual(2, utfallSvar.Underskjema.Length);
        var underskjema = utfallSvar.Underskjema[0];
        Assert.AreEqual("UNDERSKJEMA1", underskjema.Kodeverdi);
        Assert.AreEqual("Underskjema 1", underskjema.Kodebeskrivelse);

        //AnsvarligSoeker
        Assert.IsNotNull(igv3.AnsvarligSoeker);
        Assert.IsNotNull(igv3.AnsvarligSoeker.Partstype);
        Assert.AreEqual("TYPE3", igv3.AnsvarligSoeker.Partstype.Kodeverdi);
        Assert.AreEqual("Type 3", igv3.AnsvarligSoeker.Partstype.Kodebeskrivelse);
        Assert.AreEqual("12345678910", igv3.AnsvarligSoeker.Foedselsnummer);
        var kontaktperson = igv3.AnsvarligSoeker.Kontaktperson;
        Assert.IsNotNull(kontaktperson);
        Assert.AreEqual("Per Hansen", kontaktperson.Navn);
        Assert.AreEqual("12345678", kontaktperson.Telefonnummer);
        Assert.AreEqual("98765432", kontaktperson.Mobilnummer);
        Assert.AreEqual("per@example.com", kontaktperson.Epost);
        Assert.AreEqual("987654321", igv3.AnsvarligSoeker.Organisasjonsnummer);
        Assert.AreEqual("Eksempelfirma AS", igv3.AnsvarligSoeker.Navn);
        var adresse = igv3.AnsvarligSoeker.Adresse;
        Assert.IsNotNull(adresse);
        Assert.AreEqual("Eksempelet 1", adresse.Adresselinje1);
        Assert.AreEqual("1234", adresse.Postnr);
        Assert.AreEqual("Eksempelby", adresse.Poststed);
        Assert.AreEqual("NO", adresse.Landkode);
        Assert.AreEqual("12345678", igv3.AnsvarligSoeker.Telefonnummer);
        Assert.AreEqual("98765432", igv3.AnsvarligSoeker.Mobilnummer);
        Assert.AreEqual("post@eksempel.no", igv3.AnsvarligSoeker.Epost);

        Assert.IsNotNull(igv3.AnsvarForByggesaken);
        Assert.AreEqual("TYPE4", igv3.AnsvarForByggesaken.Kodeverdi);
        Assert.AreEqual("Type 4", igv3.AnsvarForByggesaken.Kodebeskrivelse);
    }

    [TestMethod]
    public void IgangsettingstillatelseV4_DeserializeXml_throws_NoExceptions()
    {
        var xmlContent = File.ReadAllText("TestData/IgangsettingstillatelseV4.xml");

        var igv4 = XmlSerializerUtil.DeserializeFromString<IgangsettingstillatelseV4>(xmlContent, false);

        Assert.IsNotNull(igv4);

        //DataFormat-attributter
        Assert.AreEqual("DIBK", igv4.DataFormatProvider);
        Assert.AreEqual("10003", igv4.DataFormatId);
        Assert.AreEqual("4", igv4.DataFormatVersion);

        //EiendomByggested
        Assert.AreEqual(2, igv4.EiendomByggested.Length);
        Assert.IsNotNull(igv4.EiendomByggested[0].Eiendomsidentifikasjon);
        Assert.AreEqual("89", igv4.EiendomByggested[0].Eiendomsidentifikasjon.Bruksnummer);
        Assert.AreEqual("10", igv4.EiendomByggested[0].Eiendomsidentifikasjon.Festenummer);
        Assert.AreEqual("567", igv4.EiendomByggested[0].Eiendomsidentifikasjon.Gaardsnummer);
        Assert.AreEqual("1234", igv4.EiendomByggested[0].Eiendomsidentifikasjon.Kommunenummer);
        Assert.AreEqual("11", igv4.EiendomByggested[0].Eiendomsidentifikasjon.Seksjonsnummer);
        Assert.IsNotNull(igv4.EiendomByggested[0].Adresse);
        Assert.AreEqual("Eksempelveien 123", igv4.EiendomByggested[0].Adresse.Adresselinje1);
        Assert.AreEqual("1234", igv4.EiendomByggested[0].Adresse.Postnr);
        Assert.AreEqual("Eksempelby", igv4.EiendomByggested[0].Adresse.Poststed);
        Assert.AreEqual("NO", igv4.EiendomByggested[0].Adresse.Landkode);
        Assert.AreEqual("123", igv4.EiendomByggested[0].Bygningsnummer);
        Assert.AreEqual("1", igv4.EiendomByggested[0].Bolignummer);
        Assert.AreEqual("Eksempelkommune", igv4.EiendomByggested[0].Kommunenavn);

        //KommunensSaksnummer
        Assert.IsNotNull(igv4.KommunensSaksnummer);
        Assert.AreEqual(2024, igv4.KommunensSaksnummer.Saksaar);
        Assert.AreEqual(56789, igv4.KommunensSaksnummer.Sakssekvensnummer);

        //Metadata
        Assert.IsNotNull(igv4.Metadata);
        Assert.AreEqual("FTB123", igv4.Metadata.FtbId);
        Assert.AreEqual("Eksempelprosjekt", igv4.Metadata.Prosjektnavn);
        Assert.AreEqual("123456", igv4.Metadata.Prosjektnr);
        Assert.IsNotNull(igv4.Metadata.ForetrukketSpraak);
        Assert.AreEqual("NO", igv4.Metadata.ForetrukketSpraak.Kodeverdi);
        Assert.AreEqual("Norsk", igv4.Metadata.ForetrukketSpraak.Kodebeskrivelse);
        Assert.AreEqual("SystemX", igv4.Metadata.FraSluttbrukersystem);

        //GenerelleVilkaar
        Assert.IsNotNull(igv4.GenerelleVilkaar);
        Assert.AreEqual(false, igv4.GenerelleVilkaar.BeroererArbeidsplasser);
        Assert.AreEqual(true, igv4.GenerelleVilkaar.NorskSvenskDansk);

        //Delsøknad
        Assert.IsNotNull(igv4.Delsoeknader);
        Assert.AreEqual(1, igv4.Delsoeknader.Length);
        var delsoeknad = igv4.Delsoeknader[0];
        Assert.IsNotNull(delsoeknad);
        Assert.AreEqual("Bygging av garasje", delsoeknad.DelAvTiltaket);
        Assert.AreEqual(new DateTime(2024, 02, 16), delsoeknad.Tillatelsedato);
        Assert.AreEqual("DS123", delsoeknad.Delsoeknadsnummer);
        Assert.AreEqual("Eksempelkommentar", delsoeknad.Kommentar);
        Assert.IsNotNull(delsoeknad.Type);
        Assert.AreEqual(1, delsoeknad.Type.Length);
        var delsoeknadType = delsoeknad.Type[0];
        Assert.AreEqual("TYPE1", delsoeknadType.Kodeverdi);
        Assert.AreEqual("Type 1", delsoeknadType.Kodebeskrivelse);

        //SoeknadGjelder
        Assert.IsNotNull(igv4.SoeknadGjelder);
        Assert.IsNotNull(igv4.SoeknadGjelder.Type);
        Assert.AreEqual(1, igv4.SoeknadGjelder.Type.Length);
        var soeknadGjelderType = igv4.SoeknadGjelder.Type[0];
        Assert.AreEqual("TYPE2", soeknadGjelderType.Kodeverdi);
        Assert.AreEqual("Type 2", soeknadGjelderType.Kodebeskrivelse);
        Assert.AreEqual("Her er et følgebrev", igv4.SoeknadGjelder.Foelgebrev);
        Assert.AreEqual(true, igv4.SoeknadGjelder.GjelderHeleTiltaket);
        Assert.AreEqual("Bygning av bolig", igv4.SoeknadGjelder.DelAvTiltaket);
        Assert.AreEqual("DS456", igv4.SoeknadGjelder.Delsoeknadsnummer);

        //UtfallBesvarelse
        Assert.IsNotNull(igv4.UtfallBesvarelse);
        Assert.AreEqual(2, igv4.UtfallBesvarelse.Length);
        var utfallSvar = igv4.UtfallBesvarelse[0];
        Assert.IsNotNull(utfallSvar.Utfalltype);
        Assert.AreEqual("UTFALL1", utfallSvar.Utfalltype.Kodeverdi);
        Assert.AreEqual("Utfall 1", utfallSvar.Utfalltype.Kodebeskrivelse);
        Assert.AreEqual("123456", utfallSvar.UtfallId);
        Assert.IsNotNull(utfallSvar.Tema);
        Assert.AreEqual("TEMA1", utfallSvar.Tema.Kodeverdi);
        Assert.AreEqual("Tema 1", utfallSvar.Tema.Kodebeskrivelse);
        Assert.AreEqual("Utfall 1 tittel", utfallSvar.Tittel);
        Assert.IsNotNull(utfallSvar.UtloestFraSjekkpunkt);
        Assert.AreEqual("Sjekkpunkt_1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktId);
        Assert.AreEqual("Sjekkpunkteier 1", utfallSvar.UtloestFraSjekkpunkt.SjekkpunktEier);
        Assert.AreEqual("Dette er utfall 1", utfallSvar.Beskrivelse);
        Assert.AreEqual(false, utfallSvar.ErUtfallBesvaresSenere);
        Assert.AreEqual(true, utfallSvar.ErUtfallBesvart);
        Assert.AreEqual("Utfall 1 kommentar", utfallSvar.Kommentar);
        //Vedlegg
        Assert.IsNotNull(utfallSvar.Vedleggsliste);
        Assert.AreEqual(2, utfallSvar.Vedleggsliste.Length);
        var vedlegg = utfallSvar.Vedleggsliste[0];
        Assert.AreEqual("V1", vedlegg.Versjonsnummer);
        Assert.IsNotNull(vedlegg.Vedleggstype);
        Assert.AreEqual("TYPE1", vedlegg.Vedleggstype.Kodeverdi);
        Assert.AreEqual("Type 1", vedlegg.Vedleggstype.Kodebeskrivelse);
        Assert.AreEqual(new DateTime(2024, 02, 16), vedlegg.Versjonsdato);
        Assert.AreEqual("vedlegg1.pdf", vedlegg.Filnavn);

        //AnsvarligSoeker
        Assert.IsNotNull(igv4.AnsvarligSoeker);
        Assert.IsNotNull(igv4.AnsvarligSoeker.Partstype);
        Assert.AreEqual("TYPE3", igv4.AnsvarligSoeker.Partstype.Kodeverdi);
        Assert.AreEqual("Type 3", igv4.AnsvarligSoeker.Partstype.Kodebeskrivelse);
        Assert.AreEqual("12345678910", igv4.AnsvarligSoeker.Foedselsnummer);
        var kontaktperson = igv4.AnsvarligSoeker.Kontaktperson;
        Assert.IsNotNull(kontaktperson);
        Assert.AreEqual("Per Hansen", kontaktperson.Navn);
        Assert.AreEqual("12345678", kontaktperson.Telefonnummer);
        Assert.AreEqual("98765432", kontaktperson.Mobilnummer);
        Assert.AreEqual("per@example.com", kontaktperson.Epost);
        Assert.AreEqual("987654321", igv4.AnsvarligSoeker.Organisasjonsnummer);
        Assert.AreEqual("Eksempelfirma AS", igv4.AnsvarligSoeker.Navn);
        var adresse = igv4.AnsvarligSoeker.Adresse;
        Assert.IsNotNull(adresse);
        Assert.AreEqual("Eksempelet 1", adresse.Adresselinje1);
        Assert.AreEqual("1234", adresse.Postnr);
        Assert.AreEqual("Eksempelby", adresse.Poststed);
        Assert.AreEqual("NO", adresse.Landkode);
        Assert.AreEqual("12345678", igv4.AnsvarligSoeker.Telefonnummer);
        Assert.AreEqual("98765432", igv4.AnsvarligSoeker.Mobilnummer);
        Assert.AreEqual("post@eksempel.no", igv4.AnsvarligSoeker.Epost);

        Assert.IsNotNull(igv4.AnsvarForByggesaken);
        Assert.AreEqual("TYPE4", igv4.AnsvarForByggesaken.Kodeverdi);
        Assert.AreEqual("Type 4", igv4.AnsvarForByggesaken.Kodebeskrivelse);
    }
}
