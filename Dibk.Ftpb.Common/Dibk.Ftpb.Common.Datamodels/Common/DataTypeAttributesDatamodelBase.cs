﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    public abstract class DataTypeAttributesDatamodelBase
    {
        [XmlAttribute("dataFormatProvider")]
        public string DataFormatProvider { get; set; }

        [XmlAttribute("dataFormatId")]
        public string DataFormatId { get; set; }

        [XmlAttribute("dataFormatVersion")]
        public string DataFormatVersion { get; set; }
    }
}