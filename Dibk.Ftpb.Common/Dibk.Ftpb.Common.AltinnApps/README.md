# Dibk.Ftpb.Common.AltinnApps

Verktøyklasser for å konfigurere og sette opp Altinn 3 applikasjoner i DIBK sitt FtPB-økosystem.

## Eksport av OTEL data til Elasticsearch

Registrering av service for eksport av OTEL-metrics til Elasticsearch gjøres vha extensionmetoden `AddOtelElasticExporters`. Det er viktig å påse at servicen blir registert etter at settings er henta fra KeyVault.

<details>

<summary>Eksempel på registrering av service</summary>

``` csharp
// ###########################################################################
// # Unless you are sure what you are doing do not change the following code #
// ###########################################################################

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

ConfigureServices(builder.Services, builder.Configuration);

builder.WebHost.ConfigureAppWebHost(args);

if (!builder.Environment.IsDevelopment())
{
    builder.AddAzureKeyVaultAsConfigProvider(); // KeyVault lastes her
}

builder.Services.AddOtelElasticExporters(builder.Configuration); // Registrere her

WebApplication app = builder.Build();

Configure();

app.Run();

```

</details>

### Appsettings

Settings som må være på plass for at ekspostering av OpenTelemetry-data til Elasticsearch skal fungere

appsettings.json må inneholde følgende:

``` json
{
  "AppSettings": {    
    "UseOpenTelemetry": true
  },
  "OTEL_RESOURCE_ATTRIBUTES": "deployment.environment=Local",
  "OTEL_METRICS_EXPORTER": "otlp",
  "OTEL_LOGS_EXPORTER": "otlp",
  "OpenTelemetry": {
    "ElasticExporter": {
      "Endpoint": "https://ein-eller-ana-identifikator.apm.uksouth.azure.elastic-cloud.com:443"
    }
  }
}  
```

Miljøspesifikke `appsettings.{miljø}.json` inneholder endringer fra standarden. Dette er typisk som følger:

``` json
{
  "OTEL_RESOURCE_ATTRIBUTES": "deployment.environment=Test", // Miljø
  "OpenTelemetry": {
    "ElasticExporter": {
      "Endpoint": "https://ein-heilt-ana-identifiketor.apm.uksouth.azure.elastic-cloud.com:443" // endepunkt
    }
  }
}
```

### Key vault

Key vault (eller secrets.json ved lokal utvikling) må inneholde authorization settings
Key: `OpenTelemetry--ElasticExporter--Headers`
Value: `Authorization=Bearer heMMEligHET2000`
