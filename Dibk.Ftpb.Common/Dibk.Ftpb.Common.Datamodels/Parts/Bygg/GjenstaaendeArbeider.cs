﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class GjenstaaendeArbeider
    {
        [XmlElement("gjenstaaendeInnenfor")]
        public string GjenstaaendeInnenfor { get; set; }

        [XmlElement("gjenstaaendeUtenfor")]
        public string GjenstaaendeUtenfor { get; set; }
    }
}