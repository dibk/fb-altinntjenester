using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using no.kxml.skjema.dibk.gjennomforingsplanV6;
using System;
using System.IO;

namespace Dibk.Ftpb.Common.Tests;

[TestClass]
public class XsdValidatorTests
{
    private XsdValidator _xsdValidator = new();

    [TestMethod]
    public void ValidateXsd_IgangsettingstillatelseV3_returns_NoErrors()
    {
        var xmlContent = File.ReadAllText("TestData/IgangsettingstillatelseV3.xml");

        var validationResult = _xsdValidator.ValidateXml(xmlContent, "igangsettingstillatelseV3");

        Assert.IsFalse(validationResult.ContainsKey("Error"));
    }

    [TestMethod]
    public void ValidateXsd_MidlertidigbrukstillatelseV3_returns_NoErrors()
    {
        var xmlContent = File.ReadAllText("TestData/midlertidigbrukstillatelseV3.xml");

        var validationResult = _xsdValidator.ValidateXml(xmlContent, "midlertidigbrukstillatelseV3");

        Assert.IsFalse(validationResult.ContainsKey("Error"));
    }

    [TestMethod]
    public void ValidateXsd_FerdigattestV3_returns_NoErrors()
    {
        var xmlContent = File.ReadAllText("TestData/ferdigattestV3.xml");

        var validationResult = _xsdValidator.ValidateXml(xmlContent, "ferdigattestV3");

        Assert.IsFalse(validationResult.ContainsKey("Error"));
    }

    [TestMethod]
    public void ValidateXsd_Planvarsel_returns_NoErrors()
    {
        var xmlContent = File.ReadAllText("TestData/planvarsel.xml");

        var validationResult = _xsdValidator.ValidateXml(xmlContent, "planvarsel");

        Assert.IsFalse(validationResult.ContainsKey("Error"));
    }

    [TestMethod]
    public void ValidateXsd_GjennomfoeringsplanV6_returns_NoErrors()
    {
        var xmlContent = File.ReadAllText("TestData/Gjennomfoeringsplan.xml");

        var validationResult = _xsdValidator.ValidateXml(xmlContent, "gjennomforingsplan6");

        Assert.IsFalse(validationResult.ContainsKey("Error"));
    }

    [TestMethod]
    public void ValidateXsd_GjennomfoeringsplanV7_with_V6_xml_returns_NoErrors()
    {
        var xmlContent = File.ReadAllText("TestData/Gjennomfoeringsplan.xml");

        var validationResult = _xsdValidator.ValidateXml(xmlContent, "gjennomforingsplan7");

        Assert.IsFalse(validationResult.ContainsKey("Error"));
    }

    [TestMethod]
    public void ValidateXsd_Gjennomfoeringsplan_V7_is_V6_compatible_Succeeds()
    {
        var gfp6 = new GjennomfoeringsplanType
        {
            eiendomByggested = new EiendomType[]
            {
                new()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = "4030",
                        gaardsnummer = "56",
                        bruksnummer = "78",
                        festenummer = "9",
                        seksjonsnummer = "10"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Street 1",
                        adresselinje2 = "",
                        adresselinje3 = "",
                        postnr = "3800",
                        poststed = "City",
                        landkode = "NO",
                        gatenavn = "Main Street",
                        husnr = "12",
                        bokstav = "A"
                    },
                    bygningsnummer = "100",
                    bolignummer = "H0101",
                    kommunenavn = "Midt-Telemark"
                },
            },
            kommunensSaksnummer = new SaksnummerType { saksaar = "2023", sakssekvensnummer = "456" },
            versjon = "1.0",
            signatur = new SignaturType { signaturdato = DateTime.Now, signertAv = "Meg", signertPaaVegneAv = "Meg" },
            ansvarligSoekerTiltaksklasse = new KodeType { kodeverdi = "Kode1", kodebeskrivelse = "Beskrivelse av kode 1" },
            gjennomfoeringsplan = new AnsvarsomraadeType[]
            {
                new()
                {
                    ansvarsomraade = "Ansvarsomr�de 1",
                    ansvarsomraadeSistEndret = DateTime.Now,
                    ansvarsomraadeStatus = new KodeType { kodeverdi = "status", kodebeskrivelse = "beskrivelse" },
                    tiltaksklasse = new KodeType { kodeverdi = "1", kodebeskrivelse = "Klasse 1"},
                }
            },
            metadata = new MetadataType()
            {
                fraSluttbrukersystem = "System1",
                ftbId = "ID1234",
                prosjektnavn = "Prosjekt X",
                sluttbrukersystemUrl = "http://example.com",
                hovedinnsendingsnummer = "12345",
                klartForSigneringFraSluttbrukersystem = true
            },
            ansvarligSoeker = new PartType
            {
                partstype = new KodeType { kodeverdi = "person", kodebeskrivelse = "person" },
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "testgate 1",
                    adresselinje2 = "",
                    adresselinje3 = "",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "B� i Telemark"
                },
                navn = "testnavn",
                epost = "test@noko.no",
                organisasjonsnummer = null,
                foedselsnummer = "12345678910",
                kontaktperson = new KontaktpersonType
                {
                    epost = "kontakt@person.no",
                    navn = "Kontakt Person",
                    mobilnummer = "12345678",
                    telefonnummer = "87654321"
                },
                mobilnummer = "98765432",
                telefonnummer = "12345678"
            }
        };

        var xml = XmlSerializerUtil.Serialize(gfp6);

        var xsdValidator = new XsdValidator();
        var validationResult_V6 = xsdValidator.ValidateXml(xml, "gjennomforingsplan6");
        var validationResult_V7 = xsdValidator.ValidateXml(xml, "gjennomforingsplan7");

        Assert.IsFalse(validationResult_V6.ContainsKey("Error"));
        Assert.IsFalse(validationResult_V7.ContainsKey("Error"));
    }


    [TestMethod]
    public void ValidateXsd_Gjennomfoeringsplan_V7_does_not_enforce_property_sequence_but_V6_does()
    {
        var gfp7 = new GjennomfoeringsplanV7
        {
            EiendomByggested = new Eiendom[]
            {
                new()
                {
                    Eiendomsidentifikasjon = new Eiendomsidentifikasjon
                    {
                        Kommunenummer = "4030",
                        Gaardsnummer = "56",
                        Bruksnummer = "78",
                        Festenummer = "9",
                        Seksjonsnummer = "10"
                    },
                    Adresse = new Adresse
                    {
                        Adresselinje1 = "Street 1",
                        Adresselinje2 = "",
                        Adresselinje3 = "",
                        Postnr = "3800",
                        Poststed = "City",
                        Landkode = "NO",
                        Gatenavn = "Main Street",
                        Husnr = "12",
                        Bokstav = "A"
                    },
                    Bygningsnummer = "100",
                    Bolignummer = "H0101",
                    Kommunenavn = "Midt-Telemark"
                },
            },
            KommunensSaksnummer = new Saksnummer { Saksaar = 2023, Sakssekvensnummer = 456 },
            Versjon = "1.0",
            Signatur = new Signatur { Signaturdato = DateTime.Now, SignertAv = "Meg", SignertPaaVegneAv = "Meg" },
            AnsvarligSoekerTiltaksklasse = new Kodeliste { Kodeverdi = "Kode1", Kodebeskrivelse = "Beskrivelse av kode 1" },
            Gjennomfoeringsplan = new AnsvarsomraadeGjennomfoeringsplan[]
            {
                new()
                {
                    Ansvarsomraade = "Ansvarsomr�de 1",
                    AnsvarsomraadeSistEndret = DateTime.Now,
                    AnsvarsomraadeStatus = new Kodeliste{ Kodeverdi = "status", Kodebeskrivelse = "beskrivelse"},
                    Tiltaksklasse = new Kodeliste{ Kodeverdi = "1", Kodebeskrivelse = "Klasse 1"},
                }
            },
            Metadata = new MetadataGjennomfoeringsplan
            {
                FraSluttbrukersystem = "System1",
                FtbId = "ID1234",
                Prosjektnavn = "Prosjekt X",
                SluttbrukersystemUrl = "http://example.com",
                Hovedinnsendingsnummer = "12345",
                KlartForSigneringFraSluttbrukersystem = true
            },
            AnsvarligSoeker = new Aktoer
            {
                Partstype = new Kodeliste { Kodeverdi = "person", Kodebeskrivelse = "person" },
                Adresse = new EnkelAdresse
                {
                    Adresselinje1 = "testgate 1",
                    Adresselinje2 = "",
                    Adresselinje3 = "",
                    Landkode = "NO",
                    Postnr = "3800",
                    Poststed = "B� i Telemark"
                },
                Navn = "testnavn",
                Epost = "test@noko.no",
                Organisasjonsnummer = null,
                Foedselsnummer = "12345678910",
                Kontaktperson = new Kontaktperson
                {
                    Epost = "kontakt@person.no",
                    Navn = "Kontakt Person",
                    Mobilnummer = "12345678",
                    Telefonnummer = "87654321"
                },
                Mobilnummer = "98765432",
                Telefonnummer = "12345678"
            }
        };

        var xml = XmlSerializerUtil.Serialize(gfp7);

        var xsdValidator = new XsdValidator();
        var validationResult_V6 = xsdValidator.ValidateXml(xml, "gjennomforingsplan6");
        var validationResult_V7 = xsdValidator.ValidateXml(xml, "gjennomforingsplan7");

        Assert.IsTrue(validationResult_V6.ContainsKey("Error"));
        Assert.IsFalse(validationResult_V7.ContainsKey("Error"));
    }

    
}