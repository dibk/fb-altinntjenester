﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Koden er generert av et verktøy.
//     Kjøretidsversjon:4.0.30319.42000
//
//     Endringer i denne filen kan føre til feil virkemåte, og vil gå tapt hvis
//     koden genereres på nytt.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.8.3928.0.
// 
namespace no.kxml.skjema.dibk.meldingstjenesteDAT {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute("melding", Namespace="", IsNullable=false)]
    public partial class SystemMessage_M {
        
        private FtbMessage ftbDATMessageField;
        
        private string dataFormatProviderField;
        
        private string dataFormatIdField;
        
        private string dataFormatVersionField;
        
        private System.Xml.XmlAttribute[] anyAttrField;
        
        public SystemMessage_M() {
            this.dataFormatProviderField = "SERES";
            this.dataFormatIdField = "6965";
            this.dataFormatVersionField = "46544";
        }
        
        /// <remarks/>
        public FtbMessage FtbDATMessage {
            get {
                return this.ftbDATMessageField;
            }
            set {
                this.ftbDATMessageField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string dataFormatProvider {
            get {
                return this.dataFormatProviderField;
            }
            set {
                this.dataFormatProviderField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string dataFormatId {
            get {
                return this.dataFormatIdField;
            }
            set {
                this.dataFormatIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string dataFormatVersion {
            get {
                return this.dataFormatVersionField;
            }
            set {
                this.dataFormatVersionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAnyAttributeAttribute()]
        public System.Xml.XmlAttribute[] AnyAttr {
            get {
                return this.anyAttrField;
            }
            set {
                this.anyAttrField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FtbMessage {
        
        private string headlineField;
        
        private string messageField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string headline {
            get {
                return this.headlineField;
            }
            set {
                this.headlineField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
    }
}
