# ---------------------------------------------------------------------------------------
# Python script for getting active ALtinn serviced for serice provider DiBK.
# Gets the main list, then loops through and collects the metadata for the 
# individual services.
# Uses c:/temp as working folder .... look for loggingFolder = r'c:\temp'
# Set envir = "https://tt02.altinn.no" for PROD or TT02
# Account is DIBK or DAT
# Output is a text file in markdown that can be pasted into a markdown block
# in Confluence
# ---------------------------------------------------------------------------------------

#!/usr/bin/env python

import logging
import traceback
import sys
import os
import time
import requests
import json
import datetime


class AltinnData:

    def __init__(self, serviceName, serviceCode, serviceEditionCode, url, validFrom, validTo):
        self.ServiceName = serviceName
        self.ServiceCode = serviceCode
        self.ServiceEditionCode = serviceEditionCode
        self.Url = url
        self.ValidFrom = validFrom
        self.ValidTo = validTo
        self.MainForm = None
        self.Forms = []
        self.Attchments = []       

    def AddMainForm(self, altinnForm):
        self.MainForm = altinnForm

    def AppendForm(self, altinnForm):
        self.Forms.append(altinnForm)

    def AppendAttachment(self, altinnAttachment):
        self.Attchments.append(altinnAttachment)


class AltinnForm:

    def __init__(self, formName, dataFormatId, dataFormatVersion, formType, schema):
        self.FormName = formName
        self.DataFormatID = dataFormatId
        self.DataFormatVersion = dataFormatVersion
        self.FormType = formType
        self.Schema = schema


class AltinnAttachment:

    def __init__(self, attachmentTypeName, allowedFileTypes, minAttachmentCount, maxAttachmentCount, maxFileSize):
        self.AttachmentTypeName = attachmentTypeName
        self.AllowedFileTypes = allowedFileTypes
        self.MinAttachmentCount = minAttachmentCount
        self.MaxAttachmentCount = maxAttachmentCount
        self.MaxFileSize = maxFileSize



# Helper class for keeping a table of strings.
class KeepMyText:

    def __init__(self, *args, **kwargs):
        self.myText = []

    def EraseMyText(self):
        self.myText = []

    def AppendMyText(self, newText):
        self.myText.append(newText)

    def GetMyText(self):
        lines = ""
        for line in self.myText:
            lines = lines + line + "\n"
        return lines


class ReadApi:

    def __init__(self, *args, **kwargs):
        self.data = None    
        self.isOK = False
        self.isRequestDone = False

    def SendRequest(self, url):
        try:
            self.data = requests.get(url)
            if self.data.status_code == 200:
                self.isOK = True
                self.isRequestDone = True
        except:
            self.isRequestDone = False

        return self.data.status_code

    def GetResultJson(self):
        if self.isOK:
            return self.data.json
        else:
            return None

    def GetResultText(self):
        if self.isOK:
            return self.data.text
        else:
            return None

    def GetResultContent(self):
        if self.isOK:
            return self.data.content
        else:
            return None

    def GetHttpStatus(self):
        if self.isRequestDone:
            return self.data.status_code
        else:
            return None



def createFolder(folderPath):
        """
        Creates a folder if it does not already exist        
    
        Args:
            folderPath (string), path to folder that needs to be created     
        """

        if not os.path.isdir(folderPath):
            os.makedirs(folderPath)


def logger_setup(cmd,loggingDir):
        """
        Sets up the logger function for the poject
    
        Args:
            cmd (string), name of the fuction that is being logged in this run of the tool
            loggingDir (string), file path where log is to be stored (no trailing \ )    
     

        Returns:
            logger object    
        """

        logger = logging.getLogger('DiBKmeta')
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler('{}\\DiBKmeta.{}.{}.log'.format(loggingDir,cmd, time.strftime("%Y%m%d_%H%M%S")))
        fh.setLevel(logging.DEBUG)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)

        return logger



def GetMarkupTableString(t0 = " ", t1 = " "):
    return (f"|{t0}|{t1}|")

def GetMarkupTableStringEmphasize(t0 = " ", t1 = " "):
    return (f"||{t0}|{t1}|")


def GetMarkupBulletListString(textSting, numIndents = 1):
    bullets = ""
    for x in range(0, numIndents, 1):
        bullets = bullets + "*" 
    return (f"{bullets} {textSting}")

def GetSchemaLocation(server, serCode, serEdition, formatId, formatVers):
    #https://www.altinn.no/api/metadata/formtask/4400/1/forms/5504/41796/xsd"/>
    return(f"{server}/api/metadata/formtask/{serCode}/{serEdition}/forms/{formatId}/{formatVers}/xsd")


def GenerateMarkupTable(altinnDataList):
    txt = KeepMyText()
    dateString = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    txt.AppendMyText(f"||Metadata||Lagret: {dateString}||")

    # Loop trough the services
    for mk in altinnDataList:
        # Add the main form stuff
        txt.AppendMyText(GetMarkupTableStringEmphasize(t0 = mk.ServiceName))
        
        # .. data
        txt.AppendMyText(GetMarkupTableString(t0 = f"Service Code: {mk.ServiceCode}, ServiceEditionCode: {mk.ServiceEditionCode}"))
        txt.AppendMyText(GetMarkupTableString(t0 = f"{mk.ValidFrom} til {mk.ValidTo}"))

        str = f"[{mk.Url}]"
        txt.AppendMyText(GetMarkupTableString(t0 = str))

        # Add Main Form
        txt.AppendMyText(GetMarkupTableString(t0 = "*Hovedskjema*", t1 = f"*{mk.MainForm.FormName}* (DataFormatId: {mk.MainForm.DataFormatID}, DataFormatVersion: {mk.MainForm.DataFormatVersion})"))
        txt.AppendMyText(GetMarkupTableString(t1 = f"[{mk.MainForm.Schema}]"))

        # Add the subforms
        tag = "*Underskjema*"    
        for sub in mk.Forms:
            txt.AppendMyText(GetMarkupTableString(t0 = f"{tag}", t1 = f"*{sub.FormName}* (DataFormatId: {sub.DataFormatID}, DataFormatVersion: {sub.DataFormatVersion})"))
            txt.AppendMyText(GetMarkupTableString(t1 = f"[{sub.Schema}]"))
            tag = " "

        # Add the attachments 
        tag = "*Vedlegg*"    
        for att in mk.Attchments:
            txt.AppendMyText(GetMarkupTableString(t0 = f"{tag}", t1 = f"*{att.AttachmentTypeName}*"))
            if att.AllowedFileTypes == "*.*":
                txt.AppendMyText(GetMarkupTableString(t1 = f"Alle filtyper tillat"))
            else:
                txt.AppendMyText(GetMarkupTableString(t1 = f"{att.AllowedFileTypes}"))
            txt.AppendMyText(GetMarkupTableString(t1 = f"Antall: {att.MinAttachmentCount} til {att.MaxAttachmentCount}, {att.MaxFileSize}MB max"))
            tag = " "

    return txt.GetMyText()


### ------------------------ m a i n  c o d e -------------------------------- ###
def main(environment, account):
    """
    Main function: Python code entry.
    """

    try:
        # Setup logging
        loggingFolder = r'c:\temp'
        createFolder(loggingFolder)
        logger = logger_setup("Testlog", loggingFolder)
        logger.debug("Testlogg")

        skipForms = [('5059', 1),
                     ('5059', 2),
                     ('3031', 1),
                     ('2482', 1),
                     ('4354', 1),
                     ('5693', 1),
                     ('5017', 2),
                     ('5350', 1),
                     ('4845', 2)]


        runStartTime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

        # Server
        environment = environment.upper()
        if environment == "PROD":
            envir = "https://altinn.no"
        elif environment == "TT02":
            envir = "https://tt02.altinn.no"
        else:
            logger.error("Commandline arg not PROD or TT02")
            exit()

        account = account.upper()
        if account == "DIBK":
            url = f"{envir}/api/metadata?$filter=ServiceOwnerCode%20eq%20%27DIBK%27"
        elif account == "DAT":
            url = f"{envir}/api/metadata?$filter=ServiceOwnerCode%20eq%20%27DAT%27"
        else:
            logger.error("Commandline arg not DIBK or DAT")
            exit()


        # Main Data Object
        altinnData = []

        logger.info(f"URL: {url}")

        # collect all the metadata entries
        logger.debug(f"Getting metadata from {url}")
        r = ReadApi()
        r.SendRequest(url)
        if not r.isOK:
            logger.error(f"Could not get data from URL {url}")
            exit
            
        altinnDat = json.loads(r.GetResultText())
        logger.debug(f"Altinn service data received {json.dumps(altinnDat, sort_keys=True, indent=4)}")

        myMarkupText = KeepMyText()
        
        # loop through and get the details
        for service in altinnDat:


            # Tuple of form ID
            formId = (service['ServiceCode'],service['ServiceEditionCode'])

            # Skip interation if serviceCode, ServiceCodeEdition is in the skipList
            # https://stackoverflow.com/questions/2191699/find-an-element-in-a-list-of-tuples
            if (len([item for item in skipForms if item == formId])) != 0:
                continue

            # https://www.altinn.no/api/metadata/formtask/4419/3
            url = f"{envir}/api/metadata/formtask/{service['ServiceCode']}/{service['ServiceEditionCode']}"

            metr = ReadApi()
            metr.SendRequest(url)

            if metr.SendRequest(url) != 200:
                logger.error(f"Error getting data for ServiceName: {service['ServiceName']}")
                logger.error(f"Did not get metadata for service : {url}, http error code {metr.GetHttpStatus()}")
                logger.error(f"Skipping ... ")
            else:    
                servjson = json.loads(metr.GetResultContent())
       
                logger.info(f"------------------------------~~~------------------------------")
                logger.info(f"ServiceName: {servjson['ServiceName']}")
                logger.info(f"ServiceCode: {servjson['ServiceCode']}")
                logger.info(f"ServiceEditionCode: {servjson['ServiceEditionCode']}")

                meta = AltinnData(servjson['ServiceName'], servjson['ServiceCode'], servjson['ServiceEditionCode'], url, servjson['ValidFrom'], servjson['ValidTo'])

                myMarkupText.AppendMyText("\n\n")
                myMarkupText.AppendMyText(f"*{servjson['ServiceName']}* ({servjson['ServiceCode']}/{servjson['ServiceEditionCode']})")
                subForms = None


                logger.info(f"> Forms")
                for form in servjson['FormsMetaData']:
                    logger.info(f" -- {form['FormName']} ({form['DataFormatID']},{form['DataFormatVersion']}) ... {form['FormType']}")
                    schema = GetSchemaLocation(envir, servjson['ServiceCode'], servjson['ServiceEditionCode'],form['DataFormatID'], form['DataFormatVersion'])

                    if form['FormType'] == 'MainForm':
                        meta.AddMainForm(AltinnForm(form['FormName'], form['DataFormatID'], form['DataFormatVersion'], form['FormType'], schema))
                        myMarkupText.AppendMyText(f"{form['FormName']} ({form['DataFormatID']},{form['DataFormatVersion']})")
                    elif form['FormType'] == 'SubForm' and subForms == None:
                        meta.AppendForm(AltinnForm(form['FormName'], form['DataFormatID'], form['DataFormatVersion'], form['FormType'], schema))    
                        myMarkupText.AppendMyText(GetMarkupBulletListString(f"Underskjema", numIndents=1))
                        myMarkupText.AppendMyText(GetMarkupBulletListString(f"{form['FormName']} ({form['DataFormatID']},{form['DataFormatVersion']})", numIndents=2))
                        subForms = True
                    else:
                        meta.AppendForm(AltinnForm(form['FormName'], form['DataFormatID'], form['DataFormatVersion'], form['FormType'], schema))    
                        myMarkupText.AppendMyText(GetMarkupBulletListString(f"{form['FormName']} ({form['DataFormatID']},{form['DataFormatVersion']})", numIndents=2))


                if 'AttachmentRules' in servjson.keys():    
                    logger.info(f"> Attachments")
                    myMarkupText.AppendMyText(GetMarkupBulletListString(f"Vedlegg", numIndents=1))
                    for attach in servjson['AttachmentRules']:
                        meta.AppendAttachment(AltinnAttachment(attach['AttachmentTypeName'], attach['AllowedFileTypes'], attach['MinAttachmentCount'], attach['MaxAttachmentCount'], attach['MaxFileSize']))    
                        logger.info(f" -- {attach['AttachmentTypeName']}")
                        myMarkupText.AppendMyText(GetMarkupBulletListString(f"{attach['AttachmentTypeName']}", numIndents=2))

                # Update main data object
                altinnData.append(meta)

        outFile = open(f"{loggingFolder}\markupDataBullet.{environment}.{account}.{runStartTime}.txt", 'w') 
        outFile.write("------------- Bullet List ------------- \n\n")
        outFile.write(myMarkupText.GetMyText())    
        outFile.close()

        outFile = open(f"{loggingFolder}\markupDataTable.{environment}.{account}.{runStartTime}.txt", 'w') 
        outFile.write(GenerateMarkupTable(altinnData))    
        outFile.close()

        # Serialize and write object to file
        outFile = open(f"{loggingFolder}\TjenesteKonfigurasjon.{environment}.{account}.{runStartTime}.json", 'w') 
        outFile.write(json.dumps(altinnData, default=lambda o: o.__dict__, sort_keys=True, indent=4))    
        outFile.close()

    except:
        logger.debug("Unexpected error:", sys.exc_info()[0])
        logger.debug("{}".format(traceback.format_exc(limit=None, chain=True)))





## MAIN FUNCTION ... runs main() if invoked as a standalone script ##
if __name__ == '__main__':
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))    
    main(sys.argv[1], sys.argv[2])    
    


