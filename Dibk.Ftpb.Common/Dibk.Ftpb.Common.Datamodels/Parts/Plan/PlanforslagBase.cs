﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Plan
{
    public class PlanforslagBase
    {
        [XmlElement("plannavn")]
        public string Plannavn { get; set; }

        [XmlElement("arealplanId")]
        public string ArealplanId { get; set; }

        [XmlElement("plantype")]
        public Kodeliste Plantype { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }
    }
}