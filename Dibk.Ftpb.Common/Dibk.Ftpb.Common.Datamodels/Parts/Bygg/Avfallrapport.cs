﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Avfallrapport
    {
        [XmlElement("avfallsklasse")]
        public Kodeliste Avfallklasse { get; set; }

        [XmlElement("fraksjon")]
        public Kodeliste Avfallfraksjon { get; set; }

        [XmlElement("mengdeFraksjon")]
        public string MengdeFraksjon { get; set; }

        [XmlElement("disponering")]
        public Avfalldisponering Avfalldisponering { get; set; }
    }
}