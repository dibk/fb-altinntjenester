﻿using Microsoft.AspNetCore.Html;

namespace Dibk.Ftpb.Common.AltinnApps.Helpers
{
    public class TemplateHelper
    {
        public static HtmlString CommaSeparate(params string[] values) => new(string.Join(", ", values.Where(value => !string.IsNullOrWhiteSpace(value))));
    }
}