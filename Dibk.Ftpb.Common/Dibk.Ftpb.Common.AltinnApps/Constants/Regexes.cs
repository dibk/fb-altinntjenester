﻿using System.Text.RegularExpressions;

namespace Dibk.Ftpb.Common.AltinnApps.Constants
{
    public static class Regexes
    {
        public static readonly Regex OversendelseXml = new(@"xmlns=""http:\/\/skjema\.geonorge\.no\/SOSI\/produktspesifikasjon\/Reguleringsplanforslag\/5\.0\/Oversendelse""", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        public static readonly Regex PlanbestemmelserXml = new(@"xmlns(:\w+)?=""http:\/\/skjema\.geonorge\.no\/SOSI\/produktspesifikasjon\/Reguleringsplanforslag\/5\.0\/Planbestemmelser""", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        public static readonly Regex Plankart2dGml = new(@"xmlns:app=""http:\/\/skjema\.geonorge\.no\/SOSI\/produktspesifikasjon\/Reguleringsplanforslag\/5\.0""[\s\S]*?srsDimension=""2""", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        public static readonly Regex Plankart3dGml = new(@"xmlns:app=""http:\/\/skjema\.geonorge\.no\/SOSI\/produktspesifikasjon\/Reguleringsplanforslag\/5\.0""[\s\S]*?srsDimension=""3""", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        public static readonly Regex SrsNameRegex = new(@"^(http:\/\/www\.opengis\.net\/def\/crs\/EPSG\/0\/|^urn:ogc:def:crs:EPSG::)(?<epsg>\d+)$", RegexOptions.Compiled);
    }
}
