﻿using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Plan
{
    /// <summary>
    /// Brukes for å inkludere versjonsinformasjon om en plan.
    /// </summary>
    public class Versjon
    {
        /// <summary>
        /// Dato for versjonen
        /// </summary>
        [XmlElement("dato")]
        public DateTime? Versjonsdato { get; set; }

        /// <summary>
        /// Versjonsnummer
        /// </summary>
        [XmlElement("nummer")]
        public string Versjonsnummer { get; set; }

        /// <summary>
        /// Indikerer om dette er første versjon av planen
        /// </summary>
        [XmlElement("erFoersteVersjon")]
        public bool? ErFoersteVersjon { get; set; }

        /// <summary>
        /// Indikerer om dette er en alternativ versjon av planen. <br/>
        /// Det er lov å sende inn flere alternativer med samme versjonsnummer.
        /// </summary>
        [XmlElement("erAlternativ")]
        public bool? ErAlternativ { get; set; }

        /// <summary>
        /// Referanse for å skille alternative versjoner fra hverandre.
        /// </summary>
        [XmlElement("alternativReferanse")]
        public string AlternativReferanse { get; set; }
    }
}
