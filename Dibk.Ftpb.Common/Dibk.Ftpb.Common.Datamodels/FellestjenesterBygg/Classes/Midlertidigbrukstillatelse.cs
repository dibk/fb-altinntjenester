﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "https://skjema.ft.dibk.no/midlertidigbrukstillatelse/v4")]
    [XmlRoot("midlertidigbrukstillatelse", Namespace = "https://skjema.ft.dibk.no/midlertidigbrukstillatelse/v4")]
    public sealed class Midlertidigbrukstillatelse : MidlertidigbrukstillatelseV4
    {
    }
}