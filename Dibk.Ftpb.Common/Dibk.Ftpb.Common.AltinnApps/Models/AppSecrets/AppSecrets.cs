﻿namespace Dibk.Ftpb.Common.AltinnApps.Models.AppSecrets
{
    public class AppSecrets : IAppSecrets
    {
        public string PdfGeneratorApiKey { get; set; }
        public string ArkitektumPrivateKey { get; set; }
        public string? FormProcessApiSettingsUri { get; set; }
        public string? FormProcessApiSettingsBasicAuthUserName { get; set; }
        public string? FormProcessApiSettingsBasicAuthPassword { get; set; }
        public string ElasticsearchNodeUri { get; set; }
    }
}