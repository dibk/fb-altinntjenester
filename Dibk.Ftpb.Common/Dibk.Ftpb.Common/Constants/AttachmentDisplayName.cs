namespace Dibk.Ftpb.Common.Constants
{
    /// <summary>
    /// Attachment display name - Hvem har definert disse?
    /// </summary>
    public static class AttachmentDisplayName
    {
        // Plantjenester

        public static readonly string KartDetaljert = "Detaljert kart";
        public static readonly string Planinitiativ = "Planinitiativ";
        public static readonly string ReferatOppstartsmoete = "Referat fra oppstartsmøte";
        public static readonly string Planprogram = "Planprogram";
        public static readonly string Planomraade = "Planområde i GML-format";
        public static readonly string PlanomraadePdf = "Planområde i PDF-format";
        public static readonly string PlanomraadeSosi = "Planområde i SOSI-format";
        public static readonly string Varselbrev = "Varselbrev";
        public static readonly string Hoeringsbrev = "Høringsbrev";
        public static readonly string Plankart = "Plankart";
        public static readonly string PlankartPdf = "Plankart i PDF-format";
        public static readonly string PlankartGml = "Plankart i GML-format";
        public static readonly string PlankartSosi = "Plankart i SOSI-format";
        public static readonly string Planbestemmelser = "Planbestemmelser";
        public static readonly string Planbeskrivelse = "Planbeskrivelse";
        public static readonly string ROSAnalyse = "ROS-Analyse";
        public static readonly string Konsekvensutredning = "Konsekvensutredning";
        public static readonly string Folgebrev = "Følgebrev";
        public static readonly string OversendelseReguleringsplanforslag = "Oversendelse reguleringsplanforslag";
        public static readonly string JuridiskIllustrasjon = "Juridisk illustrasjon";
        public static readonly string Illustrasjon = "Illustrasjoner";
        public static readonly string Kunngjoring = "Kunngjøring";
        public static readonly string Rapport = "Rapport";
        public static readonly string Utredning = "Utredning";
        public static readonly string Uttalelse = "Uttalelse";
        public static readonly string Valideringsrapport = "Valideringsrapport";
        public static readonly string ValideringsrapportSluttrapportBygningsavfall = "Valideringsrapport for sluttrapport for bygningsavfall";
        public static readonly string Saksframlegg = "Saksbremlegg";
        public static readonly string Tegnforklaringer = "Tegnforklaringer";
        public static readonly string Varslingsliste = "Varslingsliste";
        public static readonly string SamlefilMottatteUttalelser = "Samlefil mottatte uttalelser";
        public static readonly string KommentarTilUttalelser = "Kommentar til uttalelser";
        public static readonly string GrunnforholdGeoteknikk = "Grunnforhold";
        public static readonly string GrunnforholdForurensetGrunn = "Grunnforhold forurenset grunn";
        public static readonly string Trafikkutredning = "Trefikkutredning";
        public static readonly string Stoyutredning = "Støyutredning";
        public static readonly string ArkeologiskeUndersokelser = "Arkeologisk undersøkelse";
        public static readonly string RammeplanVannAvlopOvervann = "Rammeplan vann og avløp overvann";
        public static readonly string BiologiskMangfold = "Biologisk mangfold";
        public static readonly string Arealplankart = "Arealplankart";
        public static readonly string OffentligEttersyn = "OffentligEttersyn";
        public static readonly string UttalelseOffentligEttersyn = "UttalelseOffentligEttersyn";
        public static readonly string Uttalelsebrev = "Uttalelsebrev";
        public static readonly string Planvarsel = "Planvarsel";
        public static readonly string Planuttalelse = "Planuttalelse";
        public static readonly string UttalelseVedlegg = "Uttalelsevedlegg";
        public static readonly string SvarNabovarselPlan = "Svar nabovarsel plan";

        // Bygg - Hovedskjema

        public static readonly string Nabovarsel = "Nabovarsel";
        public static readonly string SuppleringAvSøknad = "Supplering av søknad";
        public static readonly string SøknadOmEndringAvTillatelse = "Søknad om endring av tillatelse";
        public static readonly string SøknadOmFerdigAttest = "Søknad om ferdigattest";
        public static readonly string SøknadOmIgangsettingstillatelse = "Søknad om igangsettingstillatelse";
        public static readonly string SøknadOmMidlertidigBrukstillatelse = "Søknad om midlertidig brukstillatelse";
        public static readonly string SøknadOmRammetillatelse = "Søknad om rammetillatelse";
        public static readonly string SøknadOmTillatelseIEttrinn = "Søknad om tillatelse i ett trinn";
        public static readonly string SøknadOmTillatelseTilTiltakUtenAnsvarsrett = "Søknad om tillatelse til tiltak uten ansvarsrett";

        // Bygg - Underskjema
        public static readonly string Vedleggsopplysninger = "Vedleggsopplysninger";
        public static readonly string Matrikkelopplysninger = "Matrikkelopplysninger";
        public static readonly string GjenpartNabovarsel = "Opplysninger gitt i nabovarsel";
        public static readonly string SluttrapportForBygningsavfall = "Sluttrapport for bygningsavfall";

        // Arbeidstilsynet
        public static readonly string ArbeidstilsynetSamtykkeSoknad = "Søknad om Arbeidstilsynets samtykke";

        // Vedlegg

        // -- Felles Plan og bygg

        public static readonly string Annet = "Annet";

        // -- Bygg

        public static readonly string ErklaeringAnsvarsrett = "Erklæring om ansvarsrett";
        public static readonly string Samsvarserklæring = "Samsvarserklæring";
        public static readonly string Kontrollerklæring = "Kontrollerklæring med sluttrapport";

        public static readonly string AndreRedegjoerelser = "Andre redegjørelser";
        public static readonly string AvkjoeringstillatelseVegmyndighet = "Avkjøringstillatelse fra vegmyndigheten";
        public static readonly string Avkjoerselsplan = "Avkjoerselsplan";
        public static readonly string AvklaringHoyspent = "Avklaring av plassering nær høyspentledning";
        public static readonly string AvklaringVA = "Avklaring av plassering nær VA-ledninger";
        public static readonly string BoligspesifikasjonMatrikkel = "Boligspesifikasjon i matrikkel";
        public static readonly string ByggesaksBIM = "ByggesaksBIM";
        public static readonly string Dispensasjonssoeknad = "Dispensasjonssøknad";
        public static readonly string Foto = "Foto";
        public static readonly string Gjennomfoeringsplan = "Gjennomføringsplan";
        public static readonly string Kart = "Kart";
        public static readonly string KommentarNabomerknader = "Kommentar til nabomerknader";
        public static readonly string KvitteringForLevertAvfall = "Kvittering for levert avfall";
        public static readonly string KvitteringNabovarsel = "Kvittering for nabovarsel";
        public static readonly string Miljoesaneringsbeskrivelse = "Miljø saneringsbeskrivelse";
        public static readonly string Nabomerknader = "Nabomerknader";
        public static readonly string NabovarselVedlegg = "Nabovarsel";
        public static readonly string OpphoerAnsvarsrett = "Opphør av ansvarsrett";
        public static readonly string PlanForUavhengigKontroll = "Plan for uavhengig kontroll";
        public static readonly string RapportFraMiljoekartlegging = "Rapport fra miljøkartlegging";
        public static readonly string RedegjoerelseAndreNaturMiljoeforhold = "Redegjørelse andre natur og miljøforhold";
        public static readonly string RedegjoerelseEstetikk = "Redegjørelse estetikk";
        public static readonly string RedegjoerelseUnntakTEK = "Redegjørelse for unntak fra TEK";
        public static readonly string RedegjoerelseForurensetGrunn = "Redegjørelse forurenset grunn";
        public static readonly string RedegjoerelseGrunnforhold = "Redegjørelse grunnforhold";
        public static readonly string RedegjoerelseSkredOgFlom = "Redegjørelse skred og flomfare";
        public static readonly string RekvisisjonAvOppmaalingsforretning = "Rekvisisjon av oppmålingsforretning";
        public static readonly string SamtykkeArbeidstilsynet = "Samtykke fra Arbeidstilsynet";
        public static readonly string SamtykkePlassering = "Samtykke til plassering nærmere enn 4 meter";
        public static readonly string Situasjonsplan = "Situasjonsplan";
        public static readonly string AnsvarsrettSelvbygger = "Søknad om ansvarsrett for selvbygger";
        public static readonly string ErklaeringSelvbygger = "Egenerklæring for selvbygger";
        public static readonly string SoknadOmAnsvarsrettTiltaksklasse1 = "Søknad om ansvarsrett for tiltaksklasse 1";
        public static readonly string TegningEksisterendeFasade = "Tegning eksisterende fasade";
        public static readonly string TegningEksisterendePlan = "Tegning eksisterende plan";
        public static readonly string TegningEksisterendeSnitt = "Tegning eksisterende snitt";
        public static readonly string TegningNyFasade = "Tegning ny fasade";
        public static readonly string TegningNyPlan = "Tegning ny plan";
        public static readonly string TegningNyttSnitt = "Tegning nytt snitt";
        public static readonly string TiltakshaverSignatur = "Tiltakshavers samtykke til byggeplaner";
        public static readonly string UnderlagUtnytting = "Underlag for beregning av utnytting";
        public static readonly string Utomhusplan = "Utomhusplan";
        public static readonly string UttalelseKulturminnemyndighet = "Uttalelse fra kulturminnemyndigheten (fylkeskommunen)";
        public static readonly string UttalelseVedtakAnnenOffentligMyndighet = "Uttalelse vedtak fra annen offentlig myndighet";
        public static readonly string DekryptertFoedselsnummerTiltakshaver = "Tiltakshavers fødselsnummer-UNTATT-OFFENTLIGHET";
        public static readonly string OpplysningerGittINabovarselDekrypteringsNoekkel = "Opplysninger gitt i nabovarsel-maskinlesbar_nøkkel-UNNTATT-OFFENTLIGHET";

        //Arbeidstilsynet
        public static readonly string Soknad = "Søknad";
        public static readonly string Arbeidsmiljoutvalg = "Arbeidsmiljoutvalg";
        public static readonly string ArbeidstakersRepresentant = "Arbeidstakersrepresentant";
        public static readonly string Bedriftshelsetjeneste = "Bedriftshelsetjeneste";
        public static readonly string Verneombud = "Verneombud";
        public static readonly string LeietakerArbeidsgiver = "Leietaker arbeidsgiver";
        public static readonly string BeskrivelseTypeArbeidProsess = "Beskrivelse type arbeidprosess";
        public static readonly string Arbeidsmiljofaktorer = "Arbeidsmiljøfaktorer";
        public static readonly string ArbeidsgiversMedvirkning = "Arbeidsgivers medvirkning";
        public static readonly string ForUtleiebygg = "For utleiebygg";
        public static readonly string BistandFraBHT = "Bistand fra bedriftshelsetjenesten";
        public static readonly string Ventilasjon = "Ventilasjon";
        public static readonly string AnsattesMedvirkning = "Ansattes medvirkning";

        // TODO - Gjennomgang av disse. Usortert - er de i bruk? - https://arkitektum.atlassian.net/wiki/spaces/DF2/pages/3311861769/Vedlegg+-+Spesifikasjon
        public static readonly string Situasjonskart = "Situasjonskart";
        public static readonly string Organisasjonsplan = "Organisasjonsplan";
        public static readonly string Revisjonserklaering = "Revisjonserklaering";
        public static readonly string SoknadUnntasOffentlighet = "Søknad unntas offentlighet";
    }
}