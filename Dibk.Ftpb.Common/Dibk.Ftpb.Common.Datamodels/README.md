# Dibk.Ftpb.Common.Datamodels #
Dibk.Ftpb.Common.Datamodels er et .Net Standard prosjekt for distribuering av XSDer og pregenererte C# klasser. 

## Utils ##
For å hente ut en spesifikk XSD kan man bruke verktøyet XsdResolver. Dersom fil-extension `.xsd` ikke er en del av filnavnet, legges denne på før det letes. 
```csharp
Stream xsdStream = XsdResolver.GetXsd("arbeidstilsynetsSamtykke2.xsd");
```

Standardoppførsel er at XsdResolveren leter etter XSDen i alle lastede bibliotek. Ønskes ikke dette, kan det overstyres med å sette parameteren `scanAllAssemblies` til `false`.
```csharp
Stream xsdStream = XsdResolver.GetXsd("arbeidstilsynetsSamtykke2.xsd", scanAllAssemblies: false);
```