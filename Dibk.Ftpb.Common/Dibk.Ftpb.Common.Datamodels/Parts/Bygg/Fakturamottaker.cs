﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Fakturamottaker
    {
        [XmlElement("organisasjonsnummer")]
        public string Organisasjonsnummer { get; set; }

        [XmlElement("bestillerReferanse")]
        public string Bestillerreferanse { get; set; }

        [XmlElement("fakturareferanser")]
        public string Fakturareferanser { get; set; }

        [XmlElement("navn")]
        public string Navn { get; set; }

        [XmlElement("prosjektnummer")]
        public string Prosjektnummer { get; set; }

        [XmlElement("ehfFaktura")]
        public bool? EhfFaktura { get; set; }

        [XmlElement("fakturaPapir")]
        public bool? FakturaPapir { get; set; }

        [XmlElement("adresse")]
        public EnkelAdresse Adresse { get; set; }
    }
}