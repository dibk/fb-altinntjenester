﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class Sjekklistekrav
    {
        [XmlElement("sjekklistepunktsvar")]
        public bool? Sjekklistepunktsvar { get; set; }

        [XmlElement("sjekklistepunkt")]
        public Kodeliste Sjekklistepunkt { get; set; }

        [XmlElement("dokumentasjon")]
        public string Dokumentasjon { get; set; }
    }
}