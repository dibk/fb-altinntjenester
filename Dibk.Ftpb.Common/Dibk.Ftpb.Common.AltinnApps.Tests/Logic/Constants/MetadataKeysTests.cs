using Dibk.Ftpb.Common.AltinnApps.Logic.Constants;
using Xunit;

namespace Dibk.Ftpb.Common.AltinnApps.Tests.Logic.Constants
{
    public class MetadataKeysTests
    {
        [Fact]
        public void ReferanseXML_ShouldHaveCorrectValue()
        {
            const string expectedValue = "ReferanseXML";
            const string actualValue = MetadataKeys.ReferanseXml;
            Assert.Equal(expectedValue, actualValue);
        }
    }
}