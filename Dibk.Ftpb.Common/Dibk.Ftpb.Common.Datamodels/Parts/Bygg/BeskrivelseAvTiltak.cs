﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class BeskrivelseAvTiltak
    {
        [XmlElement("bruk")]
        public Formaaltype Formaaltype { get; set; }

        [XmlElement("type")]
        public Kodeliste[] Tiltakstype { get; set; }

        [XmlElement("BRA")]
        public string BRA { get; set; }

        [XmlElement("foelgebrev")]
        public string Foelgebrev { get; set; }
    }
}