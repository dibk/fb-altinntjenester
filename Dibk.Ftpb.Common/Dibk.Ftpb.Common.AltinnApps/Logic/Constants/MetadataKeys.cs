﻿namespace Dibk.Ftpb.Common.AltinnApps.Logic.Constants
{
    /// <summary>
    /// Holds common constants
    /// </summary>
    public class MetadataKeys
    {
        /// <summary>
        /// Name of xml element in instance
        /// </summary>
        public const string ReferanseXml = "ReferanseXML";
    }
}
