﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    [XmlRoot("Avfallsplan")]
    public class SluttrapportForBygningsAvfall : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlArray("kommunensSaksnummer")]
        [XmlArrayItem("saksnummer")]
        public Saksnummer[] KommunensSaksnummer { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("tiltakshaver")]
        public Aktoer Tiltakshaver { get; set; }

        [XmlElement("overgangsordning")]
        public Overgangsordning Overgangsordning { get; set; }

        [XmlElement("metadata")]
        public MetadataSluttrapport Metadata { get; set; }

        [XmlElement("beskrivelseAvTiltak")]
        public BeskrivelseAvTiltak BeskrivelseAvTiltak { get; set; }

        [XmlElement("bruksarealAvfall")]
        public string BruksarealAvfall { get; set; }

        [XmlElement("plan")]
        public PlanForAvfall PlanForAvfall { get; set; }

        [XmlElement("sluttrapport")]
        public SluttrapportForAvfall SluttrapportForAvfall { get; set; }

        [XmlElement("erklaering")]
        public bool? Erklaering { get; set; }

        [XmlElement("avfallEtterRiving")]
        public bool? AvfallEtterRiving { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 7063<br />
        /// <b>DataFormatVersion:</b> 47177<br />
        /// <b>DataFormatProvider:</b> SERES
        /// </summary>
        public SluttrapportForBygningsAvfall()
        {
            DataFormatProvider = "SERES";
            DataFormatId = "7063";
            DataFormatVersion = "47177";
        }
    }
}
