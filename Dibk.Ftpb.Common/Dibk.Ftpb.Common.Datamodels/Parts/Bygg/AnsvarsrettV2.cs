﻿using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class AnsvarsrettV2
    {
        [XmlElement("funksjon")]
        public Kodeliste Funksjon { get; set; }

        [XmlElement("soeknadssystemetsReferanse")]
        public string SoeknadssystemetsReferanse { get; set; }

        [XmlElement("beskrivelseAvAnsvarsomraadet")]
        public string BeskrivelseAvAnsvarsomraadet { get; set; }

        [XmlElement("ansvarsrettErklaert")]
        public DateTime? AnsvarsrettErklaert { get; set; }
    }
}