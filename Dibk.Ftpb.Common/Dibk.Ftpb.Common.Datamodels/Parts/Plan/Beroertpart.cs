﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Plan
{
    public class Beroertpart
    {
        [XmlElement("partstype")]
        public Kodeliste Partstype { get; set; }

        [XmlElement("foedselsnummer")]
        public string Foedselsnummer { get; set; }

        [XmlElement("organisasjonsnummer")]
        public string Organisasjonsnummer { get; set; }

        [XmlElement("navn")]
        public string Navn { get; set; }

        [XmlElement("telefon")]
        public string Telefon { get; set; }

        [XmlElement("epost")]
        public string Epost { get; set; }

        [XmlElement("adresse")]
        public EnkelAdresse Adresse { get; set; }

        [XmlElement("beskrivelseForVarsel")]
        public string BeskrivelseForVarsel { get; set; }

        [XmlElement("systemReferanse")]
        public string Systemreferanse { get; set; }

        [XmlElement("erHoeringsmyndighet")]
        public bool? ErHoeringsmyndighet { get; set; }

        [XmlArray("gjelderEiendom")]
        [XmlArrayItem("gjeldereiendom")]
        public Eiendom[] GjelderEiendom { get; set; }
    }
}
