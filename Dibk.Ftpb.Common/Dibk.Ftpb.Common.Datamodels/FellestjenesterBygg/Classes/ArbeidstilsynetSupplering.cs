﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Atil;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    [XmlRoot("SuppleringArbeidstilsynet")]
    public class ArbeidstilsynetSupplering : DataTypeAttributesDatamodelBase
    {
        [XmlElement("tiltakshaver")]
        public Aktoer Tiltakshaver { get; set; }

        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlArray("ettersending")]
        [XmlArrayItem("ettersendinger")]
        public Ettersending[] Ettersending { get; set; }

        [XmlElement("foelgebrev")]
        public string Foelgebrev { get; set; }

        [XmlArray("mangelbesvarelse")]
        [XmlArrayItem("mangelbesvarelser")]
        public Mangelbesvarelse[] Mangelbesvarelse { get; set; }

        [XmlElement("arbeidstilsynetsSaksnummer")]
        public Saksnummer ArbeidstilsynetsSaksnummer { get; set; }

        [XmlElement("metadata")]
        public MetadataAtilSupplering Metadata { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 7086<br />
        /// <b>DataFormatVersion:</b> 47365<br />
        /// <b>DataFormatProvider:</b> SERES
        /// </summary>
        public ArbeidstilsynetSupplering()
        {
            DataFormatProvider = "SERES";
            DataFormatId = "7086";
            DataFormatVersion = "47365";
        }
    }
}
