﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class MetadataSluttrapport
    {
        [XmlElement("ftbId")]
        public string FtbId { get; set; }
        [XmlElement("prosjektnavn")]
        public string Prosjektnavn { get; set; }

        [XmlElement("sluttbrukersystemUrl")]
        public string SluttbrukersystemUrl { get; set; }

        [XmlElement("prosjektnr")]
        public string Prosjektnr { get; set; }

        [XmlElement("fraSluttbrukersystem")]
        public string FraSluttbrukersystem { get; set; }

        [XmlElement("versjonsnummerAvfallsplan")]
        public string VersjonsnummerAvfallsplan { get; set; }

    }
}
