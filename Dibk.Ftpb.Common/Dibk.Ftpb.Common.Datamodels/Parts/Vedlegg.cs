﻿using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts
{
    public class Vedlegg
    {
        [XmlElement("versjonsnummer")]
        public string Versjonsnummer { get; set; }

        [XmlElement("vedleggstype")]
        public Kodeliste Vedleggstype { get; set; }

        [XmlElement("versjonsdato")]
        public DateTime? Versjonsdato { get; set; }

        [XmlElement("filnavn")]
        public string Filnavn { get; set; }
    }
}