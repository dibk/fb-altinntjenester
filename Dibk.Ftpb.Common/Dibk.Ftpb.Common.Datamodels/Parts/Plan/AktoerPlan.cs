﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Plan
{
    public class AktoerPlan
    {
        [XmlElement("partstype")]
        public Kodeliste Partstype { get; set; }

        [XmlElement("foedselsnummer")]
        public string Foedselsnummer { get; set; }

        [XmlElement("organisasjonsnummer")]
        public string Organisasjonsnummer { get; set; }

        [XmlElement("navn")]
        public string Navn { get; set; }

        [XmlElement("epost")]
        public string Epost { get; set; }

        [XmlElement("adresse")]
        public EnkelAdresse Adresse { get; set; }

        [XmlElement("telefon")]
        public string Telefon { get; set; }
    }
}