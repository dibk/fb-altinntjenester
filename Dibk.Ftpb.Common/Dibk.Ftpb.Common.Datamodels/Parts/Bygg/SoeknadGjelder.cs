﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class SoeknadGjelder : SoeknadGjelderBase
    {
        [XmlElement("gjelderHeleTiltaket")]
        public bool? GjelderHeleTiltaket { get; set; }

        [XmlElement("delAvTiltaket")]
        public string DelAvTiltaket { get; set; }

        [XmlElement("delsoeknadsnummer")]
        public string Delsoeknadsnummer { get; set; }
    }
}