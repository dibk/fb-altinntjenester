﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "http://skjema.kxml.no/dibk/gjennomforingsplan/6.0")]
    [XmlRoot("Gjennomfoeringsplan", Namespace = "http://skjema.kxml.no/dibk/gjennomforingsplan/6.0")]
    public class GjennomfoeringsplanV7 : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested;

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer;

        [XmlElement("versjon")]
        public string Versjon;

        [XmlElement("signatur")]
        public Signatur Signatur;

        [XmlElement("ansvarligSoekerTiltaksklasse")]
        public Kodeliste AnsvarligSoekerTiltaksklasse;

        [XmlArray("gjennomfoeringsplan")]
        [XmlArrayItem("ansvarsomraade")]
        public AnsvarsomraadeGjennomfoeringsplan[] Gjennomfoeringsplan;

        [XmlElement("metadata")]
        public MetadataGjennomfoeringsplan Metadata;

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker;

        public GjennomfoeringsplanV7()
        {
            DataFormatProvider = "SERES";
            DataFormatId = "6146";
            DataFormatVersion = "44096";
        }
    }
}
