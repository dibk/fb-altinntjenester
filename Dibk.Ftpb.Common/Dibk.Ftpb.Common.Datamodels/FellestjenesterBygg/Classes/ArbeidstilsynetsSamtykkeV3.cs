﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Atil;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    [XmlType(Namespace = "https://skjema.ft.dibk.no/arbeidstilsynetssamtykke/v3")]
    [XmlRoot("ArbeidstilsynetsSamtykke", Namespace = "https://skjema.ft.dibk.no/arbeidstilsynetssamtykke/v3")]
    public class ArbeidstilsynetsSamtykkeV3 : DataTypeAttributesDatamodelBase
    {
        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] EiendomByggested { get; set; }

        [XmlElement("tiltakshaver")]
        public Aktoer Tiltakshaver { get; set; }

        [XmlElement("arbeidsplasser")]
        public Arbeidsplasser Arbeidsplasser { get; set; }

        [XmlElement("fakturamottaker")]
        public FakturamottakerV2 Fakturamottaker { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("betaling")]
        public Betaling Betaling { get; set; }

        [XmlArray("dispensasjoner")]
        [XmlArrayItem("dispensasjon")]
        public Dispensasjon[] Dispensasjoner { get; set; }

        [XmlArray("krav")]
        [XmlArrayItem("sjekklistekrav")]
        public Sjekklistekrav[] Sjekklistekrav { get; set; }

        [XmlElement("beskrivelseAvTiltak")]
        public BeskrivelseAvTiltakV2 BeskrivelseAvTiltak { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("arbeidstilsynetsSaksnummer")]
        public Saksnummer ArbeidstilsynetsSaksnummer { get; set; }

        [XmlElement("metadata")]
        public Metadata Metadata { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 12000<br />
        /// <b>DataFormatVersion:</b> 3<br />
        /// <b>DataFormatProvider:</b> ATIL
        /// </summary>
        public ArbeidstilsynetsSamtykkeV3()
        {
            DataFormatId = "12000";
            DataFormatVersion = "3";
            DataFormatProvider = "ATIL";
        }
    }
}
