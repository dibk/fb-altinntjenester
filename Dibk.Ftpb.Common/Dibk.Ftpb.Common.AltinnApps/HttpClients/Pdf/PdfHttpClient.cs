﻿using System.Text;
using Dibk.Ftpb.Common.AltinnApps.Models.AppSecrets;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;

namespace Dibk.Ftpb.Common.AltinnApps.HttpClients.Pdf
{
    public class PdfHttpClient : IPdfHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly PdfSettings _settings;
        private readonly IAppSecrets _secrets;
        private readonly ILogger<PdfHttpClient> _logger;

        public PdfHttpClient(
            HttpClient httpClient,
            IOptions<PdfSettings> options,
            IAppSecrets secrets,
            ILogger<PdfHttpClient> logger)
        {
            _httpClient = httpClient;
            _settings = options.Value;
            _secrets = secrets;
            _logger = logger;
        }

        public async Task<MemoryStream> GeneratePdf(string htmlString)
        {
            try
            {
                using var request = new HttpRequestMessage(HttpMethod.Post, _settings.GeneratorUrl)
                {
                    Content = CreateFormData(htmlString),
                };

                request.Headers.Add("X-API-KEY", _secrets.PdfGeneratorApiKey);

                using var response = await _httpClient.SendAsync(request);
                response.EnsureSuccessStatusCode();

                using var responseStream = await response.Content.ReadAsStreamAsync();
                var memoryStream = new MemoryStream();
                await responseStream.CopyToAsync(memoryStream);
                memoryStream.Position = 0;

                return memoryStream;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Kunne ikke generere PDF!");
                return null;
            }
        }

        private MultipartFormDataContent CreateFormData(string htmlString)
        {
            var formData = new MultipartFormDataContent
            {
                { new StringContent(htmlString), "htmlString" },
                { new StringContent(JObject.FromObject(_settings.Paper).ToString(), Encoding.UTF8, "application/json"), "options" }
            };

            formData.Headers.ContentType.MediaType = "multipart/form-data";

            return formData;
        }
    }
}