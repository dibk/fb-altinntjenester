﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public abstract class MetadataAltinn2Base
    {
        [XmlElement("ftbId")]
        public string FtbId { get; set; }

        [XmlElement("prosjektnavn")]
        public string Prosjektnavn { get; set; }

        [XmlElement("sluttbrukersystemUrl")]
        public string SluttbrukersystemUrl { get; set; }

        [XmlElement("hovedinnsendingsnummer")]
        public string Hovedinnsendingsnummer { get; set; }

        [XmlElement("klartForSigneringFraSluttbrukersystem")]
        public bool? KlartForSigneringFraSluttbrukersystem { get; set; }

        [XmlElement("fraSluttbrukersystem")]
        public string FraSluttbrukersystem { get; set; }
    }
}