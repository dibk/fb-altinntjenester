﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Formaaltype
    {
        [XmlElement("anleggstype")]
        public Kodeliste Anleggstype { get; set; }

        [XmlElement("naeringsgruppe")]
        public Kodeliste Naeringsgruppe { get; set; }

        [XmlElement("bygningstype")]
        public Kodeliste[] Bygningstype { get; set; }

        [XmlElement("tiltaksformaal")]
        public Kodeliste[] Tiltaksformaal { get; set; }

        [XmlElement("beskrivPlanlagtFormaal")]
        public string BeskrivPlanlagtFormaal { get; set; }
    }
}