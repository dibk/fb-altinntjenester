﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    [XmlType(Namespace = "https://skjema.ft.dibk.no/arbeidstilsynetssamtykke/v3")]
    [XmlRoot("ArbeidstilsynetsSamtykke", Namespace = "https://skjema.ft.dibk.no/arbeidstilsynetssamtykke/v3")]
    public class ArbeidstilsynetsSamtykke : ArbeidstilsynetsSamtykkeV3
    {
    }
}
