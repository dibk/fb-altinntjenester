﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "https://skjema.ft.dibk.no/igangsettingstillatelse/v4")]
    [XmlRoot("igangsettingstillatelse", Namespace = "https://skjema.ft.dibk.no/igangsettingstillatelse/v4")]
    public sealed class Igangsettingstillatelse : IgangsettingstillatelseV4
    {
    }
}