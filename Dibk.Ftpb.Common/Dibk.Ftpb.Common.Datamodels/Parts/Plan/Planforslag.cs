﻿using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Plan
{
    public class Planforslag : PlanforslagBase
    {
        [XmlElement("hjemmesidePlanforslag")]
        public string HjemmesidePlanforslag { get; set; }

        [XmlElement("kravKonsekvensUtredning")]
        public bool? KravKonsekvensutredning { get; set; }

        [XmlElement("planHensikt")]
        public string Planhensikt { get; set; }

        [XmlElement("fristForInnspill")]
        public DateTime? FristForInnspill { get; set; }

        [XmlElement("hjemmesidePlanprogram")]
        public string HjemmesidePlanprogram { get; set; }

        [XmlElement("begrunnelseKU")]
        public string BegrunnelseKU { get; set; }

        [XmlElement("saksgangOgMedvirkning")]
        public string SaksgangOgMedvirkning { get; set; }
    }
}