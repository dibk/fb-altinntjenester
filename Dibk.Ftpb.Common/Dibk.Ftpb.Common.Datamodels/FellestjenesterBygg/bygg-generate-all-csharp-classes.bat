echo "============================ GENERATE C# classes ============================"
echo "gjennomforingsplan6.xsd"
xsd.exe .\Xsd\gjennomforingsplan6.xsd /c /n:no.kxml.skjema.dibk.gjennomforingsplanV6
move /y gjennomforingsplan6.cs .\Classes\GjennomforingsplanV6.cs
REM
echo "igangsettingstillatelseV2.xsd"
xsd.exe .\Xsd\igangsettingstillatelseV2.xsd /c /n:no.kxml.skjema.dibk.igangsettingstillatelseV2
move /y igangsettingstillatelseV2.cs .\Classes\IgangsettingstillatelseV2.cs
REM
echo "midlertidigbrukstillatelseV2.xsd"
xsd.exe .\Xsd\midlertidigbrukstillatelseV2.xsd /c /n:no.kxml.skjema.dibk.midlertidigbrukstillatelseV2
move /y midlertidigbrukstillatelseV2.cs .\Classes\MidlertidigbrukstillatelseV2.cs
REM
echo "nabovarselV4.xsd"
xsd.exe .\Xsd\nabovarselV4.xsd /c /n:no.kxml.skjema.dibk.nabovarselV4
move /y nabovarselV4.cs .\Classes\NabovarselV4.cs
REM
echo "gjenpartnabovarselV2.xsd"
xsd.exe .\Xsd\gjenpartnabovarselV2.xsd /c /n:no.kxml.skjema.dibk.gjenpartnabovarselV2
move /y gjenpartnabovarselV2.cs .\Classes\GjenpartnabovarselV2.cs
REM
echo "rammesoknad3.xsd"
xsd.exe .\Xsd\rammesoknad3.xsd /c /n:no.kxml.skjema.dibk.rammesoknadV3
move /y rammesoknad3.cs .\Classes\RammesoknadV3.cs
REM
echo "etttrinnsoknad3.xsd"
xsd.exe .\Xsd\etttrinnsoknad3.xsd /c /n:no.kxml.skjema.dibk.etttrinnsoknadV3
move /y etttrinnsoknad3.cs .\Classes\EtttrinnsoknadV3.cs
REM
echo "tiltakutenansvarsrett3.xsd"
xsd.exe .\Xsd\tiltakutenansvarsrett3.xsd /c /n:no.kxml.skjema.dibk.tiltakutenansvarsrettV3
move /y tiltakutenansvarsrett3.cs .\Classes\TiltakutenansvarsrettV3.cs
REM
echo "ferdigattestV2.xsd"
xsd.exe .\Xsd\ferdigattestV2.xsd /c /n:no.kxml.skjema.dibk.ferdigattestV2
move /y ferdigattestV2.cs .\Classes\FerdigattestV2.cs
REM
echo "ansvarsrettDirekteOpprettet.xsd"
xsd.exe .\Xsd\ansvarsrettDirekteOpprettet.xsd /c /n:no.kxml.skjema.dibk.ansvarsrettDirekteOpprettet
move /y ansvarsrettDirekteOpprettet.cs .\Classes\AnsvarsrettDirekteOpprettet.cs
REM
echo "kontrollerklaeringDirekteOpprettet.xsd"
xsd.exe .\Xsd\kontrollerklaeringDirekteOpprettet.xsd /c /n:no.kxml.skjema.dibk.kontrollerklaeringDirekteOpprettet
move /y kontrollerklaeringDirekteOpprettet.cs .\Classes\KontrollerklaeringDirekteOpprettet.cs
REM
echo "samsvarserklaeringDirekteOpprettet.xsd"
xsd.exe .\Xsd\samsvarserklaeringDirekteOpprettet.xsd /c /n:no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet
move /y samsvarserklaeringDirekteOpprettet.cs .\Classes\SamsvarserklaeringDirekteOpprettet.cs
REM
echo "nabovarselsvarV3.xsd"
xsd.exe .\Xsd\nabovarselsvarV3.xsd /c /n:no.kxml.skjema.dibk.nabovarselsvarV3
move /y nabovarselsvarV3.cs .\Classes\SvarNabovarselV3.cs
REM
echo "MeldingstjenesteSERES.xsd"
xsd.exe .\Xsd\MeldingstjenesteSERES.xsd /c /n:no.kxml.skjema.dibk.meldingstjeneste
move /y MeldingstjenesteSERES.cs .\Classes\Meldingstjeneste.cs
REM
echo "arbeidstilsynetsSamtykke.xsd"
xsd.exe .\Xsd\arbeidstilsynetsSamtykke.xsd /c /n:no.kxml.skjema.dibk.arbeidstilsynetsSamtykke
move /y arbeidstilsynetsSamtykke.cs .\Classes\ArbeidstilsynetsSamtykke.cs
REM
echo "arbeidstilsynetsSamtykke2.xsd"
xsd.exe .\Xsd\arbeidstilsynetsSamtykke2.xsd /c /n:no.kxml.skjema.dibk.arbeidstilsynetsSamtykke2
move /y arbeidstilsynetsSamtykke2.cs .\Classes\ArbeidstilsynetsSamtykke2.cs
REM
echo "suppleringArbeidstilsynet.xsd"
xsd.exe .\Xsd\suppleringArbeidstilsynet.xsd /c /n:no.kxml.skjema.dibk.suppleringArbeidstilsynet
move /y suppleringArbeidstilsynet.cs .\Classes\SuppleringArbeidstilsynet.cs
REM
echo "matrikkelregistrering01.xsd"
xsd.exe .\Xsd\matrikkelregistrering01.xsd /c /n:no.kxml.skjema.dibk.matrikkelregistrering
move /y matrikkelregistrering01.cs .\Classes\Matrikkelregistrering.cs
REM
echo "vedlegg.xsd"
xsd.exe .\Xsd\vedlegg.xsd /c /n:no.kxml.skjema.dibk.vedlegg
move /y vedlegg.cs .\Classes\Vedlegg.cs
REM
echo "endringavtillatelse.xsd"
xsd.exe .\Xsd\endringavtillatelse.xsd /c /n:no.kxml.skjema.dibk.endringavtillatelse
move /y endringavtillatelse.cs .\Classes\Endringavtillatelse.cs
REM
echo "tiltakshaversSignatur.xsd"
xsd.exe .\Xsd\tiltakshaversSignatur.xsd /c /n:no.kxml.skjema.dibk.tiltakshaversSignatur
move /y tiltakshaversSignatur.cs .\Classes\TiltakshaversSignatur.cs
REM
echo "suppleringAvSoknad.xsd"
xsd.exe .\Xsd\suppleringAvSoknad.xsd /c /n:no.kxml.skjema.dibk.suppleringAvSoknad
move /y suppleringAvSoknad.cs .\Classes\SuppleringAvSoknad.cs
REM
echo "nabovarselPlan.xsd"
xsd.exe .\Xsd\nabovarselPlan.xsd /c /n:no.kxml.skjema.dibk.nabovarselPlan
move /y nabovarselPlan.cs .\Classes\NabovarselPlan.cs
REM
echo "nabovarselsvarPlan.xsd"
xsd.exe .\Xsd\nabovarselsvarPlan.xsd /c /n:no.kxml.skjema.dibk.nabovarselsvarPlan
move /y nabovarselsvarPlan.cs .\Classes\NabovarselsvarPlan.cs
REM
echo "MeldingstjenesteDAT.xsd"
xsd.exe .\Xsd\MeldingstjenesteDAT.xsd /c /n:no.kxml.skjema.dibk.meldingstjenesteDAT
move /y MeldingstjenesteDAT.cs .\Classes\MeldingstjenesteDAT.cs
REM
echo "AvfallsplanV2.xsd"
xsd.exe .\Xsd\AvfallsplanV2.xsd /c /n:no.kxml.skjema.dibk.avfallsplanV2
move /y AvfallsplanV2.cs .\Classes\AvfallsplanV2.cs
REM
echo "ansvarsrettAnsako.xsd"
xsd.exe .\Xsd\ansvarsrettAnsako.xsd /c /n:no.kxml.skjema.dibk.ansvarsrettAnsako
move /y ansvarsrettAnsako.cs .\Classes\AnsvarsrettAnsako.cs
REM
echo "kontrollerklaeringAnsako.xsd"
xsd.exe .\Xsd\kontrollerklaeringAnsako.xsd /c /n:no.kxml.skjema.dibk.kontrollerklaeringAnsako
move /y kontrollerklaeringAnsako.cs .\Classes\KontrollerklaeringAnsako.cs
REM
echo "samsvarserklaeringAnsako.xsd"
xsd.exe .\Xsd\samsvarserklaeringAnsako.xsd /c /n:no.kxml.skjema.dibk.samsvarserklaeringAnsako
move /y samsvarserklaeringAnsako.cs .\Classes\SamsvarserklaeringAnsako.cs

echo "============================ Finished generating classes ============================"
pause

