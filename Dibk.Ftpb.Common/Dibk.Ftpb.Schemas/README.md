# Dibk.Ftpb.Schemas
Prosjektet inneholder katalogen "xsd" som er kilden til innholdet på https://skjema.ft.dibk.no.
https://skjema.ft.dibk.no er en statisk nettside, knyttet til en storage account i Azure.

## CI/CD
Når det gjøres en push til master-branchen vil bitbucket-pipelines kjøre en jobb som synkroniserer innholdet i blob storaget i Azure med innholdet i "xsd"-katalogen.
Det vil i tillegg genereres index.html-filer for hver katalog i "xsd"-katalogen. Disse filene vil sørge for at den statiske nettsiden er navigerbar.

## Legge til nytt skjema, eller oppdatere eksisterende skjema
Man bør ikke publisere skjema i dette prosjektet før eksterne skal ta skjema i bruk. Dette for å unngå at eksterne bruker skjema som ikke er ferdig utviklet.
Dersom det likevel er nødvendig å publisere skjema i dette prosjektet før eksterne skal ta skjema i bruk, bør disse lagres under "xsd/TEST/".
Innholdet under "xsd/TEST/" vil ikke være synlig på https://skjema.ft.dibk.no.

### Legge til nytt skjema
Ved nytt skjema, eller ny versjon, må det under "xsd"-katalogen lages ny katalogstruktur som tilsvarer url-en som er oppgitt i skjemaets xmlns-attributt.
E.g.: Det er laget versjon 3 av planvarsel.xsd. Da vil skjemaet ha xmlns="https://skjema.ft.dibk.no/planvarsel/v3", og må ligge på lokasjonen "xsd/planvarsel/v3".
Siden lokasjonen "xsd/planvarsel" allerede eksisterer, må vi lage en ny katalog "v3" under "xsd/planvarsel", og deretter legge til filen "planvarselV3.xsd" i denne katalogen.
Til slutt døper vi om skjemaet fra "planvarselV3.xsd" til "planvarsel.xsd" (fjerner "versjons-tag" fra filnavn, altså).

### Oppdatere eksisterende skjema
Dersom man har fulgt oppfordringen med å ikke publisere skjema i dette prosjektet før eksterne skal ta skjema i bruk, bør oppdatering av eksisterende skjema helst ikke skje i dette prosjektet.
Dersom det er nødvendig å oppdatere et skjema som allerede er publisert, er det viktig å informere brukere om dette i god tid før master-branchen bygges.