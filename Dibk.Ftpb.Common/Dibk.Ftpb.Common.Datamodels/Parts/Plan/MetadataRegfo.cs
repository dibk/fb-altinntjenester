﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Plan
{
    public class MetadataRegfo : MetadataPlan
    {
        [XmlElement("prosjektnavn")]
        public string Prosjektnavn { get; set; }

        [XmlElement("prosjektnr")]
        public string Prosjektnr { get; set; }
    }
}