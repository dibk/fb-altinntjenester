using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts;

namespace Dibk.Ftpb.Common.Tests;

[TestClass]
public class XmlSerializerTests
{
    [TestMethod]
    public void SerializeXml_Eiendom()
    {
        var eiendom = new Eiendom
        {
            Adresse = null,
            Eiendomsidentifikasjon = null,
            Bolignummer = "1",
            Bygningsnummer = "123",
            Kommunenavn = "Kommunen min"
        };

        var xml = XmlSerializerUtil.Serialize(eiendom);

        Assert.IsFalse(xml.Contains("<adresse>"));
        Assert.IsFalse(xml.Contains("<eiendomsidentifikasjon>"));
        Assert.IsTrue(xml.Contains("<bolignummer>"));
        Assert.IsTrue(xml.Contains("<bygningsnummer>"));
        Assert.IsTrue(xml.Contains("<kommunenavn>"));
    }
}
