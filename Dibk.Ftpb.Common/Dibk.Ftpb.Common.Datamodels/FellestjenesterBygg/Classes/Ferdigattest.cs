﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels
{
    [XmlType(Namespace = "https://skjema.ft.dibk.no/ferdigattest/v4")]
    [XmlRoot("ferdigattest", Namespace = "https://skjema.ft.dibk.no/ferdigattest/v4")]
    public sealed class Ferdigattest : FerdigattestV4
    {
    }
}