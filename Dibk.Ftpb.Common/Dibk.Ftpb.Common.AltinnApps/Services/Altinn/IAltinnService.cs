﻿using Altinn.Platform.Storage.Interface.Models;
using Dibk.Ftpb.Common.AltinnApps.Services.Altinn.Models;

namespace Dibk.Ftpb.Common.AltinnApps.Services.Altinn
{
    public interface IAltinnService
    {
        Task<Instance> GetCurrentInstance();
        Task<T> DeserializeXML<T>(Instance instance, DataElement dataElement) where T : class;
        Task<T> DeserializeXML<T>(Instance instance, string formDataType) where T : class;
        Task<Stream> GetBinaryData(Instance instance, DataElement item);
        Task<Stream> GetBinaryData(Instance instance, string formDataType);
        Task<Stream> GetBinaryData(string formDataType);
        DataElement GetDataElement(Instance instance, string formDataType);
        List<DataElement> GetDataElements(Instance instance, string formDataType);
        List<Attachment> GetAttachments(Instance instance);
        Task<DataElement> InsertBinaryData(string datatype, string contentType, string filename, Stream stream);
        Task<DataElement> UpdateBinaryData(DataElement dataElement, Stream stream);
    }
}