﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Dibk.Ftpb.Common.Constants;

namespace Dibk.Ftpb.Common.Models
{
    /// <summary>
    /// Configuration class for dataType
    /// </summary>
    public class DataTypeConfig
    {
        public string DataType { get; set; }
        public string DisplayName { get; set; }
        public string Arkivlett1DokumentType { get; set; }
        public string Arkivlett2DokumentType { get; set; }
        public string Category { get; set; }

        public List<ConfigType> LimitedTo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="displayName"></param>
        /// <param name="arkivlett1DokumentType"></param>
        /// <param name="arkivlett2DokumentType"></param>
        /// <param name="vedleggskategori"></param>
        /// <param name="limitedTo"></param>
        public DataTypeConfig(string dataType, string displayName, string arkivlett1DokumentType, string arkivlett2DokumentType, Vedleggskategori? category = null, params ConfigType[] limitedTo) 
        {
            DataType = dataType;
            DisplayName = displayName;
            Arkivlett1DokumentType = arkivlett1DokumentType;
            Arkivlett2DokumentType = arkivlett2DokumentType;
            Category = category?.ToString();

            if (limitedTo != null && limitedTo.Length > 0)
                LimitedTo = limitedTo.ToList();
            else
                LimitedTo = new List<ConfigType>() { ConfigType.Undefined };
        }
    }

    public enum ConfigType
    {
        Undefined,
        MainForm,
        SubForm,
        Attachment
    }
}