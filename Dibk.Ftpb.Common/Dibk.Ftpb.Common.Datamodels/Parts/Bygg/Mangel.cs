﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Mangel
    {
        [XmlElement("mangelId")]
        public string MangelId { get; set; }

        [XmlElement("mangeltekst")]
        public Kodeliste Mangeltekst { get; set; }
    }
}