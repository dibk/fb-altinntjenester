﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Plan
{
    public class MetadataPlan
    {
        [XmlElement("ftbId")]
        public string FtbId { get; set; }

        [XmlElement("hovedinnsendingsnummer")]
        public string Hovedinnsendingsnummer { get; set; }

        [XmlElement("klartForSigneringFraSluttbrukersystem")]
        public bool? KlartForSigneringFraSluttbrukersystem { get; set; }

        [XmlElement("fraSluttbrukersystem")]
        public string FraSluttbrukersystem { get; set; }
    }
}