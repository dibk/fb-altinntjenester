namespace Dibk.Ftpb.Common.AltinnApps.Services.Altinn.Models;

public class Attachment
{
    public Attachment()
    {
    }

    public Attachment(string dataType, string filename)
    {
        DataType = dataType;
        Filename = filename;
    }

    public string DataType { get; set; }
    public string Filename { get; set; }
}