﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Varmesystem
    {
        [XmlArray("varmefordeling")]
        [XmlArrayItem("kode")]
        public Kodeliste[] Varmefordeling { get; set; }

        [XmlArray("energiforsyning")]
        [XmlArrayItem("kode")]
        public Kodeliste[] Energiforsyning { get; set; }

        [XmlElement("relevant")]
        public bool? ErRelevant { get; set; }
    }
}