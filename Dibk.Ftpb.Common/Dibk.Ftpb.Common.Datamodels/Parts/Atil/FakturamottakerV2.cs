﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Atil
{
    public class FakturamottakerV2 : FakturamottakerBase
    {
        [XmlElement("partstype")]
        public Kodeliste Partstype { get; set; }

        [XmlElement("foedselsnummer")]
        public string Foedselsnummer { get; set; }

        [XmlElement("bestillerreferanse")]
        public string Bestillerreferanse { get; set; }

        [XmlElement("prosjektnummer")]
        public string Prosjektnummer { get; set; }

        [XmlElement("navn")]
        public string Navn { get; set; }

        [XmlElement("adresse")]
        public EnkelAdresse Adresse { get; set; }

        [XmlElement("epost")]
        public string Epost { get; set; }
    }
}