﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Foretak : Aktoer
    {
        [XmlElement("harSentralGodkjenning")]
        public bool? HarSentralGodkjenning { get; set; }
    }
}