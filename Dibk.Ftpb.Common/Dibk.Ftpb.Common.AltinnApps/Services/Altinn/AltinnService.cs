using Altinn.App.Core.Internal.Data;
using Altinn.App.Core.Internal.Instances;
using Altinn.Platform.Storage.Interface.Models;
using Dibk.Ftpb.Common.AltinnApps.Services.Altinn.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using DiBK.RuleValidator.Extensions;

namespace Dibk.Ftpb.Common.AltinnApps.Services.Altinn;

public class AltinnService : IAltinnService
{
    private readonly IDataClient _dataService;
    private readonly IInstanceClient _instanceService;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ILogger<AltinnService> _logger;

    public AltinnService(
        IDataClient dataService,
        IInstanceClient instanceService,
        IHttpContextAccessor httpContextAccessor,
        ILogger<AltinnService> logger)
    {
        _dataService = dataService;
        _httpContextAccessor = httpContextAccessor;
        _instanceService = instanceService;
        _logger = logger;
    }

    public async Task<Instance> GetCurrentInstance()
    {
        var parameters = GetAppParameters();

        if (parameters == null)
            return null;

        return await _instanceService.GetInstance(
            parameters.App, parameters.Org, parameters.InstanceOwnerPartyId, parameters.InstanceGuid);
    }

    public async Task<T> DeserializeXML<T>(Instance instance, DataElement dataElement) where T : class
    {
        using var stream = await GetBinaryData(instance, dataElement);

        return XmlHelper.DeserializeXML<T>(stream);
    }

    public async Task<T> DeserializeXML<T>(Instance instance, string formDataType) where T : class
    {
        var dataElement = GetDataElement(instance, formDataType);

        if (dataElement == null)
            return null;

        return await DeserializeXML<T>(instance, dataElement);
    }

    public async Task<Stream> GetBinaryData(Instance instance, DataElement dataElement)
    {
        if (dataElement == null)
            return null;

        if (!int.TryParse(instance.InstanceOwner.PartyId, out var partyId))
            return null;

        if (!Guid.TryParse(dataElement.InstanceGuid, out var instanceGuid))
            return null;

        if (!Guid.TryParse(dataElement.Id, out var dataId))
            return null;

        return await _dataService.GetBinaryData(instance.Org, instance.AppId, partyId, instanceGuid, dataId);
    }

    public async Task<Stream> GetBinaryData(Instance instance, string formDataType)
    {
        var dataElement = GetDataElement(instance, formDataType);

        if (dataElement == null)
            return null;

        return await GetBinaryData(instance, dataElement);
    }

    public async Task<Stream> GetBinaryData(string formDataType)
    {
        var instance = await GetCurrentInstance();

        return await GetBinaryData(instance, formDataType);
    }

    public DataElement GetDataElement(Instance instance, string formDataType)
    {
        return instance.Data
            .Find(dataElement => dataElement.DataType == formDataType);
    }

    public List<DataElement> GetDataElements(Instance instance, string formDataType)
    {
        return instance.Data
            .FindAll(dataElement => dataElement.DataType == formDataType);
    }

    public List<Attachment> GetAttachments(Instance instance)
    {
        return instance.Data
            .Select(dataElement => new Attachment
            {
                DataType = dataElement.DataType,
                Filename = dataElement.Filename
            })
            .ToList();
    }

    private AppParameters GetAppParameters()
    {
        var routeValues = _httpContextAccessor.HttpContext.Request.RouteValues;

        if (!routeValues.TryGetValue("app", out var app))
            return null;

        if (!routeValues.TryGetValue("org", out var org))
            return null;

        if (!routeValues.TryGetValue("instanceOwnerPartyId", out var instanceOwnerPartyId))
            return null;

        if (!routeValues.TryGetValue("instanceGuid", out var instanceGuid))
            return null;

        return new AppParameters
        {
            App = app as string,
            Org = org as string,
            InstanceOwnerPartyId = int.Parse(instanceOwnerPartyId as string),
            InstanceGuid = Guid.Parse(instanceGuid as string)
        };
    }

    public async Task<DataElement> InsertBinaryData(string datatype, string contentType, string filename, Stream stream)
    {
        try
        {
            var instance = await GetCurrentInstance();
            var appParameters = GetAppParameters();
            var instanceId = appParameters.InstanceOwnerPartyId + "/" + appParameters.InstanceGuid;
            var dataElement = GetDataElement(instance, datatype);

            if (dataElement != null)
            {
                return await UpdateBinaryData(dataElement, stream);
            }

            return await _dataService.InsertBinaryData(instanceId, datatype, contentType, filename, stream);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, $"Kan ikke laste opp {filename} til Altinn!");
            return null;
        }
    }

    public async Task<DataElement> UpdateBinaryData(DataElement dataElement, Stream stream)
    {
        var appParameters = GetAppParameters();
        var httpContext = new DefaultHttpContext();
        httpContext.Request.ContentType = dataElement.ContentType;
        httpContext.Request.Headers.Add("Content-Disposition", $"attachment; filename={dataElement.Filename}");
        httpContext.Request.Body = stream;

        try
        {
            return await _dataService.UpdateBinaryData(
                appParameters.Org,
                appParameters.App,
                appParameters.InstanceOwnerPartyId,
                appParameters.InstanceGuid,
                Guid.Parse(dataElement.Id),
                httpContext.Request
            );
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, $"Kan ikke oppdatere {dataElement.Filename} i Altinn!");
            return null;
        }
    }
}