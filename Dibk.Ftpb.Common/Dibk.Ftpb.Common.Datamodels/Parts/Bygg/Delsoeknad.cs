﻿using System;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Delsoeknad
    {
        [XmlElement("delAvTiltaket")]
        public string DelAvTiltaket { get; set; }

        [XmlElement("tillatelsedato")]
        public DateTime? Tillatelsedato { get; set; }

        [XmlElement("kommentar")]
        public string Kommentar { get; set; }

        [XmlArray("type")]
        [XmlArrayItem("kode")]
        public Kodeliste[] Type { get; set; }

        [XmlElement("delsoeknadsnummer")]
        public string Delsoeknadsnummer { get; set; }
    }
}