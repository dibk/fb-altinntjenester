using System.Xml;
using System.Xml.Serialization;
using Altinn.App.Core.Features;
using Altinn.App.Core.Internal.Data;
using Altinn.App.Core.Models;
using Altinn.App.Core.Models.Validation;
using Altinn.Platform.Storage.Interface.Models;
using Dibk.Ftpb.Common.AltinnApps.Logic.Constants;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Validation.Client.Models.Request;
using Dibk.Ftpb.Validation.Client.Models.Response;
using Dibk.Ftpb.Validation.Client.Services;

namespace Dibk.Ftpb.Common.AltinnApps.Validator;
/// <summary>
/// Base class for creating a data element validator. 
/// </summary>
/// <param name="validationService"></param>
/// <param name="dataClient"></param>
/// <typeparam name="T"></typeparam>
public abstract class DataElementValidatorBase<T>(
    IValidationApiService validationService,
    IDataClient dataClient)
    : IDataElementValidator
{
    /// <summary>
    /// Må være samme som datatypen i applicationMetadata
    /// </summary>
    public abstract string DataType { get; }

    /// <summary>
    /// Fetch form data and send it to ValidationService. Validation report is saved on the
    /// instance in Altinn.
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="dataElement"></param>
    /// <param name="dataType"></param>
    /// <param name="language"></param>
    /// <returns></returns>
    public virtual async Task<List<ValidationIssue>> ValidateDataElement(
        Instance instance,
        DataElement dataElement,
        DataType dataType,
        string? language
    )
    {
        string formData;

        if (IsFormData(dataElement))
        {
            await using (var dataStream =
                         await ValidationHelper.GetDataAsStreamAsync(instance, dataElement, dataClient))
            {

                var tmpFormData = await GetFormDataAsStringAsync(dataStream);

                formData = RemoveAltinnRowIdAttribute(tmpFormData);

                // Sjekk om innholdet i dataStream lar seg deserialisere til den aktuelle typen T
                if (!ValidationHelper.DataStreamIsValidType<T>(dataStream))
                    return [ValidationIssues.GetKonverteringAvDatastroemIssue<T>(dataElement.Id)];
            }

            var validationRequest = await CreateValidationRequestAsync(formData, instance);

            if (ValidationRequestHasIssues(validationRequest, dataElement.Id, out var validationRequestIssues))
                return validationRequestIssues;

            var response = await validationService.ValidateReportAsync(validationRequest);

            // Håndtere at validering feiler
            if (response == null)
                return [ValidationIssues.GetFeilUnderValideringIssue(dataElement.Id, DataType)];

            var validationReportFileName = $"Valideringsrapport-{DataType}.xml";
            await SaveValidationResponse(response, instance, validationReportFileName, Guid.Parse(dataElement.Id));
            return ValidationHelper.MapValidationResult(response, dataElement.Id);
        }
        return [];
    }

    private static bool IsFormData(DataElement dataElement)
    {
        return ValidationHelper.ContentTypeIsXml(dataElement.ContentType) || ValidationHelper.ContentTypeIsGml(dataElement.ContentType);
    }

    private static async Task<string> GetFormDataAsStringAsync(Stream dataStream)
    {
        using var reader = new StreamReader(dataStream, leaveOpen: true);
        var formData = await reader.ReadToEndAsync();

        dataStream.Position = 0;

        return formData;
    }

    /// <summary>
    /// Lagrer eller oppdater valideringsrapporten
    /// </summary>
    /// <param name="response"></param>
    /// <param name="instance"></param>
    /// <param name="validationReportFileName"></param>
    /// <param name="validatedDataElementId"></param>
    /// <returns></returns>
    private async Task SaveValidationResponse(
        ValidationReportResponse response,
        Instance instance,
        string validationReportFileName,
        Guid validatedDataElementId
    )
    {
        var instanceOwnerPartyId = Convert.ToInt32(instance.InstanceOwner.PartyId);
        var instanceGuid = new Guid(instance.Id.Split("/")[1]);

        var metadataReferanseXml = new KeyValueEntry
        {
            Key = MetadataKeys.ReferanseXml,
            Value = validatedDataElementId.ToString()
        };

        var currentValidationReport = instance.Data.SingleOrDefault(d =>
            d.DataType.Equals(DataTypes.Valideringsrapport) &&
            d.Metadata != null &&
            d.Metadata.Any(m => m.Key == metadataReferanseXml.Key && m.Value == metadataReferanseXml.Value));

        try
        {
            using var streamResult = new MemoryStream();
            var serializer = new XmlSerializer(typeof(ValidationReportResponse));
            serializer.Serialize(streamResult, response);
            streamResult.Position = 0;

            currentValidationReport = currentValidationReport == null
                ? await dataClient.InsertBinaryData(
                    instance.Id,
                    DataTypes.Valideringsrapport,
                    ValidationHelper.ValidationReportContentType,
                    validationReportFileName,
                    streamResult,
                    "Task_1")
                : await dataClient.UpdateBinaryData(
                    new InstanceIdentifier(instanceOwnerPartyId, instanceGuid),
                    ValidationHelper.ValidationReportContentType,
                    validationReportFileName,
                    Guid.Parse(currentValidationReport.Id),
                    streamResult);

            currentValidationReport.Metadata = [metadataReferanseXml];

            await dataClient.Update(instance, currentValidationReport);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    /// <summary>
    /// Fjerner "altinnRowId"-attributten fra alle noder i XML-dokumentet.
    /// </summary>
    /// <param name="xmlContent"></param>
    private static string RemoveAltinnRowIdAttribute(string xmlContent)
    {
        var doc = new XmlDocument();
        doc.LoadXml(xmlContent);

        XmlElement? root = doc.DocumentElement;
        if (root != null)
            RemoveAltinnRowIdAttribute(root);

        return doc.InnerXml;
    }

    /// <summary>
    /// Rekursiv metode som fjerner "altinnRowId"-attributten fra alle noder
    /// </summary>
    /// <param name="node"></param>
    private static void RemoveAltinnRowIdAttribute(XmlNode node)
    {
        var attribute = node.Attributes?["altinnRowId"];
        if (node.Attributes != null && attribute != null)
        {
            node.Attributes.Remove(attribute);
        }

        // Iterate through child nodes and call the method recursively
        foreach (XmlNode childNode in node.ChildNodes)
        {
            RemoveAltinnRowIdAttribute(childNode);
        }
    }

    /// <summary>
    /// Lager en <see cref="ValidationRequest"/> som skal sendes til valideringstjenesten.
    /// </summary>
    /// <param name="data">Skjemadata for <typeparamref name="T"/> i streng-format</param>
    /// <param name="instance">Altinn3-instans</param>
    /// <returns></returns>
    protected abstract Task<ValidationRequest> CreateValidationRequestAsync(string data, Instance instance);

    /// <summary>
    /// Undersøker om <paramref name="validationRequest"/> har verdier som fører til valideringsfeil.
    /// </summary>
    /// <param name="validationRequest"></param>
    /// <param name="dataElementId"></param>
    /// <param name="validationIssues"></param>
    protected abstract bool ValidationRequestHasIssues(
        ValidationRequest validationRequest,
        string dataElementId,
        out List<ValidationIssue> validationIssues
    );
}