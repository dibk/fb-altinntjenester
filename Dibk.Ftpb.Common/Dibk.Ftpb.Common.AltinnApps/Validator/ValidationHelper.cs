﻿using System.Xml.Serialization;
using Altinn.App.Core.Internal.Data;
using Altinn.App.Core.Models.Validation;
using Altinn.Platform.Storage.Interface.Models;
using Dibk.Ftpb.Validation.Client.Models;
using Dibk.Ftpb.Validation.Client.Models.Response;

namespace Dibk.Ftpb.Common.AltinnApps.Validator;

/// <summary>
/// Helper methods for performing validation
/// </summary>
public class ValidationHelper
{
    /// <summary>
    /// Content type for the validation report
    /// </summary>
    public const string ValidationReportContentType = "application/xml";

    /// <summary>
    /// Maps the validation result from validationApiService to a list of altinn validationIssues
    /// </summary>
    /// <param name="result"></param>
    /// <param name="dataElementId"></param>
    /// <returns></returns>
    public static List<ValidationIssue> MapValidationResult(ValidationReportResponse result, string dataElementId)
    {
        var validationIssues = new List<ValidationIssue>();

        if (result?.Messages == null)
            return validationIssues;

        validationIssues.AddRange(from message in result.Messages
                                  let severity = message.Messagetype switch
                                  {
                                      ValidationResponseSeverityEnum.ERROR => ValidationIssueSeverity.Error,
                                      ValidationResponseSeverityEnum.WARNING => ValidationIssueSeverity.Warning,
                                      _ => ValidationIssueSeverity.Unspecified
                                  }
                                  select new ValidationIssue
                                  {
                                      Severity = severity,
                                      Code = message.Reference,
                                      DataElementId = dataElementId,
                                      Description = message.Message,
                                      Field = message.XpathField,
                                  });

        return validationIssues;
    }

    /// <summary>
    /// Fetch form data from the instance
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="dataElement"></param>
    /// <param name="dataClient"></param>
    /// <returns></returns>
    public static async Task<Stream> GetDataAsStreamAsync(Instance instance, DataElement dataElement, IDataClient dataClient)
    {
        var instanceGuid = new Guid(instance.Id.Split("/")[1]);

        return await dataClient.GetBinaryData(
            instance.Org,
            instance.AppId,
            Convert.ToInt32(instance.InstanceOwner.PartyId),
            instanceGuid,
            new Guid(dataElement.Id)
        );
    }

    /// <summary>
    /// Verify that stream can be deserialized to this data type.
    /// Note: Does not throw any exception for errors during deserialization process
    /// </summary>
    /// <param name="dataStream">the stream to deserialize</param>
    /// <typeparam name="T">the type to deserialize to</typeparam>
    /// <returns>true when successful deserialization, otherwise false</returns>
    public static bool DataStreamIsValidType<T>(Stream dataStream)
    {
        var deserialized = false;

        try
        {
            var serializer = new XmlSerializer(typeof(T));
            _ = (T)serializer.Deserialize(dataStream)!;
            deserialized = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine("Feil under konvertering av datastrøm: " + ex.Message);
        }
        finally
        {
            dataStream.Close();
        }

        return deserialized;
    }
    
    public static async Task<string> GetContentType(Stream stream)
    {
        long originalPosition = stream.Position;
        byte[] buffer = new byte[4];
        await stream.ReadAsync(buffer, 0, buffer.Length);
        stream.Position = originalPosition; // Reset stream position

        // Bestem content-type basert på de første byte
        string contentType = GetContentType(buffer);
        return contentType;
    }

    /// <summary>
    /// Determines the MIME content type based on the first few bytes of a file buffer.
    /// </summary>
    /// <param name="buffer">The byte array representing the file content.</param>
    /// <returns>
    /// A string representing the MIME content type. Returns one of the following:
    /// - "application/pdf" for PDF files.
    /// - "application/xml" for XML files.
    /// - "application/zip" for ZIP files.
    /// - "application/octet-stream" if the content type cannot be determined.
    /// </returns>
    private static string GetContentType(byte[] buffer)
    {
        if (buffer.Length < 4)
        {
            return "application/octet-stream";
        }

        if (buffer[0] == 0x25 && buffer[1] == 0x50 && buffer[2] == 0x44 && buffer[3] == 0x46)
        {
            return "application/pdf";
        }

        if (buffer[0] == 0x3C && buffer[1] == 0x3F && buffer[2] == 0x78 && buffer[3] == 0x6D)
        {
            return "application/xml";
        }

        if (buffer[0] == 0x50 && buffer[1] == 0x4B && buffer[2] == 0x03 && buffer[3] == 0x04)
        {
            return "application/zip";
        }

        return "application/octet-stream";
    }

    /// <summary>
    /// Checks if a given MIME content type represents an XML file.
    /// </summary>
    /// <param name="contentType">The MIME content type to check.</param>
    /// <returns>
    /// <c>true</c> if the content type is "application/xml" or "text/xml"; otherwise, <c>false</c>.
    /// </returns>
    public static bool ContentTypeIsXml(string contentType)
    {
        return contentType is "application/xml" or "text/xml";
    }

    /// <summary>
    /// Checks if a given MIME content type represents a GML file.
    /// </summary>
    /// <param name="contentType">The MIME content type to check.</param>
    /// <returns>
    /// <c>true</c> if the content type is "application/gml+xml" or "application/octet-stream"; otherwise, <c>false</c>.
    /// </returns>
    public static bool ContentTypeIsGml(string contentType)
    {
        return contentType is "application/gml+xml" or "application/octet-stream";
    }

    /// <summary>
    /// Validates whether a MIME content type matches a given file extension.
    /// </summary>
    /// <param name="contentType">The MIME content type to validate.</param>
    /// <param name="fileExtension">The file extension to validate against (without the leading dot).</param>
    /// <returns>
    /// <c>true</c> if the content type and file extension match based on the rules defined; otherwise, <c>false</c>.
    /// If no specific rules match, the method considers the content type and extension valid by default.
    /// </returns>
    public static bool IsContentTypeAndExtensionMatching(string contentType, string fileExtension)
    {
        // Normalize input
        contentType = contentType?.ToLowerInvariant().Trim();
        fileExtension = fileExtension?.ToLowerInvariant().TrimStart('.');

        // Validate for XML
        if (ContentTypeIsXml(contentType))
        {
            return fileExtension == "xml";
        }

        if (fileExtension == "xml")
        {
            return ContentTypeIsXml(contentType);
        }

        // Additional rules for other content types can be added here if necessary

        // If no rules match, assume the content type and extension are valid
        return true;
    }
}