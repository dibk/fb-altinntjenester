﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.Parts.Bygg
{
    public class Kontaktperson
    {
        [XmlElement("navn")]
        public string Navn { get; set; }

        [XmlElement("telefonnummer")]
        public string Telefonnummer { get; set; }

        [XmlElement("mobilnummer")]
        public string Mobilnummer { get; set; }

        [XmlElement("epost")]
        public string Epost { get; set; }
    }
}