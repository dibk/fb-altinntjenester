﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes
{
    [XmlRoot("Samsvarserklaering"), XmlType("Samsvarserklaering")]
    public class AnsakoSamsvarserklaering : DataTypeAttributesDatamodelBase
    {
        [XmlElement("prosjektnavn")]
        public string Prosjektnavn { get; set; }

        [XmlElement("prosjektnr")]
        public string Prosjektnr { get; set; }

        [XmlElement("fraSluttbrukersystem")]
        public string FraSluttbrukersystem { get; set; }

        [XmlElement("kommunensSaksnummer")]
        public Saksnummer KommunensSaksnummer { get; set; }

        [XmlElement("ansvarligSoeker")]
        public Aktoer AnsvarligSoeker { get; set; }

        [XmlElement("foretak")]
        public Aktoer Foretak { get; set; }

        [XmlArray("eiendomByggested")]
        [XmlArrayItem("eiendom")]
        public Eiendom[] eiendomByggested { get; set; }

        [XmlElement("ansvarsrett")]
        public AnsvarsrettV2 Ansvarsrett { get; set; }

        /// <summary>
        /// <b>DataFormatId:</b> 10001<br />
        /// <b>DataFormatVersion:</b> 1<br />
        /// <b>DataFormatProvider:</b> SERES
        /// </summary>
        public AnsakoSamsvarserklaering()
        {
            DataFormatProvider = "SERES";
            DataFormatId = "10001";
            DataFormatVersion = "1";
        }
    }
}
